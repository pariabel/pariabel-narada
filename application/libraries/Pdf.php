<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    function createPDF($html, $filename='', $download=TRUE, $paper='A4', $orientation='portrait'){
        
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle($filename);
        //$pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        $pdf->setFooterMargin(30);
        //$pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        $pdf->AddPage();
        
        $pdf->WriteHTML($html);
        
        $path=realpath(APPPATH . "./upload/".$filename.".pdf");
        $pdf->Output($filename.".pdf", 'D');
        $pdf->Output($path, 'F');
    }
}
?>