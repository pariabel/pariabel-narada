<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {
    private $payment_key;
    private $payment_type;
    private $payment_dp;
    private $payment_form;
    private $payment_total;
    
    public function __construct()
    {
        parent::__construct();


        $this->load->model('Level_model');
        $this->load->model('Level_class_model');

        $this->payment_key="210";
        $this->payment_type="VA";
        $this->payment_dp=1000000;
        $this->payment_form=500000;
        $this->payment_total=1500000;
    }
    public function index()
    {

        $this->load->view('registration/index');

    }

    public function form_register_internal()
    {

        $this->load->view('registration/form_register_internal');

    }

    public function register_eksternal()
    {

        $this->load->view('registration/register_eksternal');

    }

    public function form_register_eksternal()
    {

        $this->load->view('registration/form_register_eksternal');

    }

    public function detail_register()
    {

        $this->load->view('registration/detail_register');

    }

    public function register_success()
    {

        $this->load->view('registration/register_success');

    }

    public function payment()
    {
        $datas=[
            "invoice"       =>  "INV".date("Y").date("m")."-".$this->session->userdata("payment_key"),
            "parent_name"   =>  $this->session->userdata("parent_name"),
            "payment_code"  =>  $this->session->userdata("phone_mobile").$this->session->userdata("payment_key"),
            "dp"            =>  $this->session->userdata("fee_dp"),
            "form"          =>  $this->session->userdata("fee_form"),
            "total"         =>  $this->session->userdata("fee_total")
        ];  

        $this->load->view('registration/payment',$datas);

    }

    public function success()
    {

        $this->load->view('registration/success');

    }

    public function login()
    {

            $this->load->view('registration/login');

    }

    public function forgot_pass()
    {

        $this->load->view('registration/forgot_pass');

    }

    public function forgot_pass_success()
    {

            $this->load->view('registration/forgot_pass_success');

    }

    public function profile()
    {

        $this->load->view('registration/profile');

    }

    public function edit_profile()
    {

        $this->load->view('registration/edit_profile');

    }

    public function _getHeader(){
        return header('Content-Type: application/json');
    }

    public function search(){

        $this->_getHeader();
        $nis=$this->input->post('nis');

        $this->db->select('*')
            ->from('student')
            ->join('student_parents','student_parents.student_id=student.id')
            ->join(' parents','parents.id=student_parents.parents_id')
            ->where(['student.nis'=>$nis]);

        $res = $this->db->get()->result();

        echo json_encode($res);

    }
    public function getLevel(){

        $this->_getHeader();

        $res=$this->db->get('level')->result();
        echo json_encode($res);
    }
    public function getClass(){

        $this->_getHeader();

        $this->db->select('*')
            ->from('class')
            ->join('level_class', 'level_class.class_id = class.id')
            ->where(['level_class.level_id'=>$this->uri->segment(3)]);

        $res = [
            "kelas"     =>  $this->db->get()->result(),
            "formulir"  =>  $this->getLastRegNo($this->uri->segment(4))
        ];

        echo json_encode($res);
    }
    public function getLastRegNo($code="20221"){
        $this->_getHeader();
        $count=$this->getCountRefNo($code)==null?0:$this->getCountRefNo($code)[0]['count'];

        return $this->getNo($code,$count);

    }
    public function getCountRefNo(){
        $year=date("Y");
        $q="SELECT count(1) as count FROM registrations WHERE reg_no LIKE '%".$this->uri->segment(3)."%' AND year='".$year."' ";
        return $this->db->query($q)->result_array();

    }

    public function doRegister(){
        $datas = $this->input->post();

        $post=array(
            "nis"           =>  isset($datas['nis'])?$datas['nis']:0,
            "student_id"    =>  isset($datas['student_id'])?$datas['student_id']:0,
            "brother_name"  =>  isset($datas['brother_name'])?$datas['brother_name']:"",
            "dob_date"      =>  isset($datas['dob_date'])?$datas['dob_date']:"",
            "dob_place"     =>  isset($datas['dob_place'])?$datas['dob_place']:"",
            "email_parent"  =>  isset($datas['email_parent'])?$datas['email_parent']:"",
            "i_class"       =>  isset($datas['i_class'])?$datas['i_class']:0,
            "i_level"       =>  isset($datas['i_level'])?$datas['i_level']:0,
            "phone_home"    =>  isset($datas['phone_home'])?$datas['phone_home']:"00000",
            "parent_name"   =>  isset($datas['parent_name'])?$datas['parent_name']:"",
            "phone_home"    =>  isset($datas['phone_home'])?$datas['phone_home']:"00000",
            "phone_mobile"  =>  isset($datas['phone_mobile'])?$datas['phone_mobile']:"",
            "reg_no"        =>  isset($datas['reg_no'])?$datas['reg_no']:"",
            "reg_type"      =>  isset($datas['reg_type'])?$datas['reg_type']:"",
            "session"       =>  isset($datas['session'])?$datas['session']:"",
            "student_name"  =>  isset($datas['student_name'])?$datas['student_name']:"",
            "full_name"     =>  isset($datas['full_name'])?$datas['full_name']:"",
            "payment_key"   =>  $this->renderKey(),
            "fee_dp"        => (int)$this->payment_dp,
            "fee_form"      => (int)$this->payment_form,
            "fee_total"     => (int)$this->payment_total+(int)$this->renderKey(),
            "year_academic" =>  date('Y', strtotime('+1 year'))."/".date('Y', strtotime('+2 years')),
            "year"          =>  date("Y")
        );

        $this->db->insert("registrations",$post);

        $payments=[
            "invoice_no"    =>  "INV".date("Y").date("m")."-".$post['payment_key'],
            "payment_code"  =>  $post['phone_mobile'].$post['payment_key'],
            "payment_type"  =>  $this->payment_type,
            "item"          =>  $post['reg_type']=="internal"?"Registrasi / Daftar Ulang di Narada School":"Pendaftaran sekola di Narada School",
            "description"   =>  "",
            "amount"        =>  $post['fee_total'],
            "status"        =>  $post['reg_type']=="internal"?0:2

        ];

        $this->db->insert("payments",$payments);

        $this->session->set_userdata($post);
        
        $this->sendEmail($post['reg_type'],"kubilk56@gmail.com","Daftar Ulang di Narada School",$post);

        
        echo json_encode(1);
        
    }
    public function pdf(){
        
        $this->load->library('Pdf');
        
        $date=date("Y-m-d");
        $date = explode("-",$date);
        $dateS = $this->convertMonth($date[1]);
        $date=$date[2]." ".$dateS." ".$date[0];
        
        $datas=[
            "parent_name"   =>  $this->session->userdata('parent_name'),
            "date"          =>  $date
        ];
        
        $html = $this->load->view('registration/pdf', $datas, true);
        $filename="Pendaftaran Online Narada School". $datas['parent_name'];
        
        $this->pdf->createPDF($html,$filename, true);
        
        $this->db->where('reg_no', $this->session->userdata('reg_no'));
        $this->db->update("registrations",["pdf"=>$content]);
        
    }
    
    public function sendEmail($type="internal",$to="contact.andriana@gmail.com",$subject="Invoice",$datas=[]){

        $this->load->config('email');
        $this->load->library('email',$this->load->config('email'));

        $from = "it@naradaschool.sch.id";
        
        $this->email->set_newline("\r\n");
        $this->email->from($from,"Narada School Registration");
        $this->email->to($to);
        $this->email->subject($subject);

        switch($type){
            case    "internal":
                $this->email->message($this->load->view('registration/email_invoice',$datas,true));
                break;
            case    "eksternal":
                $this->email->message($this->load->view('registration/email_invitation',$datas,true));
                break;
        }
        

        if ($this->email->send()) {
            
        } else {
            show_error($this->email->print_debugger());
        }

    }
    public function renderKey(){
        $year=date("Y");
        $q="SELECT count(1) as count FROM registrations where year='".$year."' ";
        $count= $this->db->query($q)->result_array()[0]['count'];


        if((int)$count < 10){
            $digit=100;
        }else if((int)$count > 9 && (int)$count < 100){
            $digit=1;
        }else if((int)$count > 99 && (int)$count < 1000){
            $digit="";
        }

        $r=(string)$digit.(string)$count;

        return $r;
    }
    public function getNo($code="",$count=0){
        $digit="";
        if($count < 10){
            $digit="000";
        }else if($count >= 10 && $count <= 100){
            $digit="00";
        }else if($count >= 100 && $count <= 1000){
            $digit="0";
        }else if($count >= 1000 && $count <= 10000){
            $digit="";
        }

        $r=$count+1;

        return $code.$digit.$r;
    }
    public function convertMonth($m=null){
        
        $month="";
        switch($m){
            case    "01":
                $month="Januari";
                break;
            case    "02":
                $month="Februari";
                break;
            case    "03":
                $month="Maret";
                break;
            case    "04":
                $month="April";
                break;
            case    "05":
                $month="Mei";
                break;
            case    "06":
                $month="Juni";
                break;
            case    "07":
                $month="Juli";
                break;
            case    "08":
                $month="Agustus";
                break;
            case    "09":
                $month="September";
                break;
            case    "10":
                $month="Oktober";
                break;
            case    "11":
                $month="November";
                break;
            case    "12":
                $month="Desember";
                break;
        }
        return $month;
    }
    
    public function confirmJoin(){
        $reg_no = $this->input->get('code');
        $status = $this->input->get('status');
        
        $this->db->where('reg_no',$reg_no );
        $this->db->update("registrations",["status"=>$status]);
        
        echo json_encode([
            "result"    =>   true,
            "message"   =>  "Updated"
        ]);
        
    }
    
    public function confirmPayment(){
        
        $payment_code = $this->input->get('code');
        $status = $this->input->get('status');
        
        $this->db->where('payment_code', $payment_code);
        $this->db->update("payments",["status"=>$status]);
        
        echo json_encode([
            "result"    =>   true,
            "message"   =>  "Updated"
        ]);
    }
        
}
