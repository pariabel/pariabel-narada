<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('staff');

		$this->load->model('staff_model', 'Staff');
		$this->load->model('staff_role_model', 'StaffRole');
		$this->load->model('staff_user_model', 'StaffUser');
		$this->load->model('user_role_model', 'UserRole');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'staff')));

		$data = array();
		$data['page_title'] = 'Staffs';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('staff/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('staffs', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('nip', 'staff.name', 'role', 'is_active', 'created_at', 'updated_at');
		$this->search_columns = array('staff.name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {

			$params['select'] = 'staff.*, role.name AS role';
			$params['join'][] = array('staff_role', 'staff_role.staff_id = staff.id', 'LEFT');
			$params['join'][] = array('role', 'role.id = staff_role.role_id', 'LEFT');
			$staffs = $this->Staff->get($params);

			if ($staffs != FALSE) {
				foreach ($staffs as $staff) {
					if (in_array($staff->id, array(1, 2))) continue;

					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/staff/edit/' . $staff->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= $this->button_delete('/staff/delete/' . $staff->id, 'data-item="' . $staff->name . '"');
					}
					// $buttons .= $this->button_view('/staff/v/' . $staff->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

					$row = array(
						$staff->nip,
						$staff->name,
						$staff->role,
						$this->create_badge($badge_types[$staff->is_active], $staff->is_active),
						$staff->created_at,
						$staff->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Staff->count_filtered($params);
				$recordsTotal = $this->Staff->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('staffs', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'staff')));

		$data = array();
		$data['page_title'] = 'New Staff';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nip', 'NIP', 'required|is_unique[staff.nip]');
		$this->form_validation->set_rules('name', 'Staff name', 'required|is_unique[staff.name]');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Staff', 'Staff added successfully.'));
				redirect('staff/list');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Staff Failed', 'Failed create Staff. There was database failure.');
			}
		}

		$this->my_view('staff/add', $data);
	}

	private function create()
	{
		do {
			$role_id = $this->input->post('role_id');

			// Create user
			$user_id = $this->create_user();
			if ($user_id === FALSE) {
				$result = FALSE;
				break;
			}

			// Create user role
			$data = array('user_id' => $user_id, 'role_id' => $role_id);
			$result = $this->UserRole->replace($data);
			if ($result === FALSE) break;

			// Create staff
			$staff_id = $this->create_staff();
			if ($staff_id === FALSE) {
				$result = FALSE;
				break;
			}

			// Create staff tole
			$data = array('staff_id' => $staff_id, 'role_id' => $role_id);
			$result = $this->StaffRole->replace($data);
			if ($result === FALSE) break;

			// Create staff user
			$data = array('staff_id' => $staff_id, 'user_id' => $user_id);
			$result = $this->StaffUser->replace($data);
			if ($result === FALSE) break;

		} while (FALSE);

		return $result;
	}

	private function create_user()
	{
		$username = $this->input->post('username');
		$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('username', 'password', 'created_at', 'updated_at');

		$result = $this->User->insert($data);

		if ($result === TRUE) {
			$result = $this->db->insert_id();
			$this->log_history('user', 'create', json_encode($data));
		}

		return $result;
	}

	private function create_staff()
	{
		$name = $this->input->post('name');
		$nip = $this->input->post('nip');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('name', 'nip', 'created_at', 'updated_at');

		$result = $this->Staff->insert($data);

		if ($result === TRUE) {
			$result = $this->db->insert_id();
			$this->log_history('staff', 'create', json_encode($data));
		}

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('staffs', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'staff')));

		$data = array();
		$data['page_title'] = 'Edit Staff';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$staff = $this->Staff->get_by_id($id);
		$staff->role = $this->StaffRole->get_role_by_staff_id($id);
		if ($staff == FALSE) $this->go_to('staffs', $this->toastr('error', 'Staff', sprintf('Staff id %s not found', $id)));
		$user = $this->StaffUser->get_user_by_staff_id($id);
		$data['staff'] = $staff;
		$data['user'] = $user;

		$this->load->library('form_validation');
		if ($this->input->post()) {
			if ($this->input->post('nip') != $staff->nip) $this->form_validation->set_rules('nip', 'NIP.', 'required|is_unique[staff.nip]');
			if ($this->input->post('name') != $staff->name) $this->form_validation->set_rules('name', 'Staff name', 'required|is_unique[staff.name]');
			if ($this->input->post('username') != $user->username) $this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
			if ($this->input->post('password') != '') $this->form_validation->set_rules('password', 'Password', 'min_length[8]');
		}
		$this->form_validation->set_rules('is_active', 'Is Active', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Staff', 'Staff updated successfully.'));
				redirect('staff/list');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Staff Failed', 'Failed update Staff. There was database failure.');
			}
		}

		$this->my_view('staff/edit', $data);
	}

	private function update()
	{
		do {
			// Update user
			$result = $this->update_user();
			if ($result === FALSE) break;

			// update staff
			$result = $this->update_staff();
			if ($result === FALSE) break;

			// update role
			$staff_id = $id = $this->input->post('id');
			$user_id = $this->input->post('user_id');
			$role_id = $this->input->post('role_id');

			$data = array('user_id' => $user_id, 'role_id' => $role_id);
			$result = $this->UserRole->replace($data);
			if ($result === FALSE) break;

			$data = array('staff_id' => $staff_id, 'role_id' => $role_id);
			$result = $this->StaffRole->replace($data);
			if ($result === FALSE) break;

		} while (FALSE);

		return $result;
	}

	private function update_user()
	{
		$id = $this->input->post('user_id');
		$username = $this->input->post('username');
		$password = ($this->input->post('password') != '') ? password_hash($this->input->post('password'), PASSWORD_DEFAULT) : '';
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->User->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = ($password != '') ? compact('password', 'is_active', 'updated_at') : compact('is_active', 'updated_at');
		$where = compact('id');
		$result = $this->User->update($data, $where);

		if ($result == TRUE) $this->log_history('user', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	private function update_staff()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$nip = $this->input->post('nip');
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Staff->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = compact('name', 'nip', 'is_active', 'updated_at');
		$where = compact('id');
		$result = $this->Staff->update($data, $where);

		if ($result === TRUE) $this->log_history('staff', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('staffs', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'staff')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('staffs', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array(
			'code' => 200,
			'swal' => array(
				'title' => 'Deleted!',
				'html' => '',
				'type' => 'success'
			)
		);

		$error = array(
			'code' => 500,
			'swal' => array(
				'title' => 'Delete Staff Failed',
				'html' => 'Database failure',
				'type' => 'error'
			)
		);

		do {
			// Ge staff
			$staff = $this->Staff->get_by_id($id);
			if ($staff == FALSE)  {
				$response = $error;
				$response['code'] = 404;
				$response['swal']['html'] = sprintf('Staff id %s not found', $id);
				break;
			}

			// Get user
			$user = $this->StaffUser->get_user_by_staff_id($id);
			if ($user == FALSE)  {
				$response = $error;
				$response['code'] = 404;
				$response['swal']['html'] = sprintf('User account for staff id %s not found', $id);
				break;
			}

			// Delete user
			if ($this->User->delete(array('id' => $user->id)) == FALSE) {
				$response = $error;
				$response['code'] = 500;
				$response['swal']['html'] = sprintf('Delete user account id: %s failed', $user->id);
				break;
			}

			// Delete staff
			$where = array('id' => $id);
			if ($this->Staff->delete($where) == FALSE) {
				$response = $error;
				break;
			}

			$this->log_history('staff', 'delete', '', json_encode($staff), json_encode($where));

			$response = $success;
			$response['swal']['html'] = sprintf('Staff <strong>%s</strong> deleted.', $staff->name);

		} while (FALSE);

		$this->output_json($response);
	}
}
