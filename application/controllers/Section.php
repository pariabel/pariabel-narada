<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('section');

		$this->load->model('section_model', 'Section');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'section')));

		$data = array();
		$data['page_title'] = 'Sections';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('section/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('sections', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('id', 'section', 'created_at', 'updated_at');
		$this->search_columns = array('section');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$sections = $this->Section->get($params);

			if ($sections != FALSE) {
				foreach ($sections as $section) {
					$buttons = $this->button_edit('/section/edit/' . $section->id);
					$buttons .= $this->button_delete('/section/delete/' . $section->id, 'data-item="' . $section->section . '"');

					$row = array(
						$section->id,
						$section->section,
						$section->created_at,
						$section->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Section->count_filtered($params);
				$recordsTotal = $this->Section->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('section', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'section')));

		$data = array();
		$data['page_title'] = 'New Section';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('section', 'Section', 'required|is_unique[section.section]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Section', 'Section added successfully.'));
				redirect('sections');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Section Failed', 'Failed create section. There was database failure.');
			}
		}

		$this->my_view('section/add', $data);
	}

	private function create()
	{
		$section = $this->input->post('section');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('section', 'created_at', 'updated_at');

		$result = $this->Section->insert($data);

		if ($result === TRUE) $this->log_history('section', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('section', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'section')));

		$data = array();
		$data['page_title'] = 'Edit Section';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$section = $this->Section->get_by_id($id);
		if ($section == FALSE) $this->go_to('sections', $this->toastr('error', 'Section', sprintf('Section id %s not found', $id)));
		$data['section'] = $section;

		$this->load->library('form_validation');
		if ($this->input->post('section') && ($this->input->post('section') != $section->section)) $this->form_validation->set_rules('section', 'Section', 'required|is_unique[section.section]');
		$this->form_validation->set_rules('id', 'Id', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Section', 'Section updated successfully.'));
				redirect('sections');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Section Failed', 'Failed update section. There was database failure.');
			}
		}

		$this->my_view('section/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$section = $this->input->post('section');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Section->get_by_id($id);

		if ($old_data == FALSE) return FALSE;

		$data = compact('section', 'updated_at');
		$where = compact('id');

		$result = $this->Section->update($data, $where);

		if ($result == TRUE) $this->log_history('section', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('section', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'section')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('sections', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$section = $this->Section->get_by_id($id);
			if ($section == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Section Failed', sprintf('Section id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Section->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Section Failed', 'Database failure');
				break;
			}

			$this->log_history('section', 'delete', '', json_encode($section), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Section <strong>%s</strong> deleted.', $section->section));

		} while (FALSE);

		$this->output_json($response);
	}
}
