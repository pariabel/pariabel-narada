<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function generate_random_password($password_length = 8)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('dashboard', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$success = array(
			'code' => 200,
			'message' => 'Random password generated successfully.'
		);
		$error = array(
			'code' => 500,
			'message' => 'Failed generate random password.'
		);
		$password = parent::generate_random_password();

		if ($password != '') {
			$response = $success;
			$response['data'] = $password;
		} else {
			$response = $error;
		}
		$this->output_json($response);
	}
}
