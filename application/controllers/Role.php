<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('role');

		$this->load->model('role_model', 'Role');
		$this->load->model('role_module_model', 'RoleModule');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'role')));

		$data = array();
		$data['page_title'] = 'Roles';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('role/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('role', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('id', 'name');
		$this->search_columns = array('name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$roles = $this->Role->get($params);

			if ($roles != FALSE) {
				foreach ($roles as $role) {
					$disabled =  ($role->name == 'Administrator') ? ' disabled' : '';
					$buttons = $this->button_edit('/role/edit/' . $role->id) . '&nbsp;&nbsp;';
					$buttons .= '<a href="/role/permission/' . $role->id . '" class="mb-1 btn btn-sm btn-success' . $disabled . '"><i class="fas fa-sm fa-user-cog"></i> Permission</a>&nbsp;&nbsp;';
					$buttons .= $this->button_delete('/role/delete/' . $role->id, 'data-item="' . $role->name . '"');

					$row = array(
						$role->id,
						$role->name,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Role->count_filtered($params);
				$recordsTotal = $this->Role->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('role', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'role')));

		$data = array();
		$data['page_title'] = 'New Role';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required|is_unique[role.name]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Role', 'Role added successfully.'));
				redirect('roles');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Role Failed', 'Failed create role. There was database failure.');
			}
		}

		$this->my_view('role/add', $data);
	}

	private function create()
	{
		$name = $this->input->post('name');

		$data = compact('name');

		$result = $this->Role->insert($data);

		if ($result === TRUE) $this->log_history('role', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('role', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'role')));

		$data = array();
		$data['page_title'] = 'Edit Role';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$role = $this->Role->get_by_id($id);
		if ($role == FALSE) $this->go_to('role', $this->toastr('error', 'Role', sprintf('Role id %s not found', $id)));
		$data['role'] = $role;

		$this->load->library('form_validation');
		if ($this->input->post('name') && ($this->input->post('name') != $role->name)) $this->form_validation->set_rules('name', 'Name', 'required|is_unique[role.name]');
		$this->form_validation->set_rules('id', 'Id', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Role', 'Role updated successfully.'));
				redirect('roles');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Role Failed', 'Failed update role. There was database failure.');
			}
		}

		$this->my_view('role/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');

		$old_data = $this->Role->get_by_id($id);

		if ($old_data == FALSE) return FALSE;

		$data = compact('name');
		$where = compact('id');

		$result = $this->Role->update($data, $where);

		if ($result == TRUE) $this->log_history('role', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'role')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('role', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$role = $this->Role->get_by_id($id);
			if ($role == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Role Failed', sprintf('Role id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Role->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Role Failed', 'Database failure');
				break;
			}

			$this->log_history('role', 'delete', '', json_encode($role), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Role <strong>%s</strong> deleted.', $role->name));

		} while (FALSE);

		$this->output_json($response);
	}

	public function permission($role_id = 0)
	{
		if ( $this->sess_user->role->name != 'Administrator') $this->go_to('role', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'role permission')));

		$data = $role_modules = array();

		$role = $this->Role->get_by_id($role_id);
		if ($role == FALSE) $this->go_to('role', $this->toastr('error', 'Role', sprintf('Role id %s not found', $role_id)));

		$data['role'] = $role;
		$data['page_title'] = sprintf('%s - Role Permission', $role->name);
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		if ($this->input->post('role_id')) $role_id = $this->input->post('role_id');

		if ($this->input->post()) {
			if ($this->replace_permission() === TRUE) {
				$this->session->set_flashdata('toastr', array(
					'type' => 'success',
					'title' => 'Role Permission',
					'message' => 'Role permission updated successfully.'
				));
			} else {
				$this->session->set_flashdata('toastr', array(
					'type' => 'error',
					'title' => 'Role Permission',
					'message' => 'Update role permission failed.'
				));
			}
			redirect('role/permission/' . $role_id);
		}

		$role = $this->Role->get_by_id($role_id);
		if ($role == FALSE) $this->go_to('role', $this->toastr('error', 'Role', sprintf('Role id %s not found', $role_id)));
		$data['role'] = $role;

		// Administrator role module cannot editable
		// if ($role->name == 'Administrator') show_error('Role Administrator may not be edited', 404);

		$this->load->model('Module_model', 'Module');
		$data['modules'] = $this->Module->get();

		$rows = $this->RoleModule->get_by_role_id($role_id);
		if ($rows !== FALSE) {
			foreach ($rows as $row) {
				$role_modules[$row->module_id]['create'] = $row->create;
				$role_modules[$row->module_id]['read'] = $row->read;
				$role_modules[$row->module_id]['update'] = $row->update;
				$role_modules[$row->module_id]['delete'] = $row->delete;
			}
		}
		$data['role_modules'] = $role_modules;

		$this->my_view('role/permission', $data);
	}

	private function replace_permission()
	{
		$role_id = $this->input->post('role_id');
		$cb_create = $this->input->post('create[]');
		$cb_read = $this->input->post('read[]');
		$cb_update = $this->input->post('update[]');
		$cb_delete = $this->input->post('delete[]');

		$this->load->model('Module_model', 'Module');
		$modules = $this->Module->get();

		foreach ($modules as $module) {
			$data = array(
				'role_id' => $role_id,
				'module_id' => $module->id,
				'create' => (isset($cb_create[$module->id]) && $cb_create[$module->id] == '1') ? 'yes' : 'no',
				'read' => (isset($cb_read[$module->id]) && $cb_read[$module->id] == '1') ? 'yes' : 'no',
				'update' => (isset($cb_update[$module->id]) && $cb_update[$module->id] == '1') ? 'yes' : 'no',
				'delete' => (isset($cb_delete[$module->id]) && $cb_delete[$module->id] == '1') ? 'yes' : 'no',
				'modified_at' => date('Y-m-d H:i:s')
			);

			$result = $this->RoleModule->replace($data);
		}

		return $result;
	}
}
