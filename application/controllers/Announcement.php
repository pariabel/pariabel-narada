<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('announcement');

		$this->load->model('announcement_model', 'Announcement');
		$this->load->model('announcement_user_model', 'AnnouncementUser');
		$this->load->model('level_model', 'Level');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden' , sprintf('You don\'t have permission to access %s', 'announcement')));

		$data = array();
		$data['page_title'] = 'Announcements';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
				$this->my_view('announcement/super_user/index', $data);
			break;

			case 'Administrative Staff':
				$this->my_view('announcement/administrative_staff/index', $data);
			break;

			case 'Teacher':
				$this->my_view('announcement/teacher/index', $data);
			break;

			case 'Student':
				$this->my_view('announcement/student/index', $data);
			break;

			case 'Parents':
				$this->my_view('announcement/parents/index', $data);
			break;

			default:
				show_404();
		}
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('announcements', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
				$this->datatable_super_user();
			break;

			case 'Administrative Staff':
				$this->datatable_administrative_staff();
			break;

			case 'Teacher':
				$this->datatable_teacher();
			break;

			case 'Student':
				$this->datatable_student();
			break;

			case 'Parents':
				$this->datatable_parents();
			break;

			default:
				show_404();
		}
	}

	private function datatable_super_user()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Update announcement user (user notification)
			$this->update_notification_by_module('Announcement');

			$params['select'] = 'announcement.*, level.level';
			$params['join'][] = array('level', 'level.id = announcement.level_id', 'left');
			$announcements = $this->Announcement->get($params);

			if ($announcements != FALSE) {
				foreach ($announcements as $announcement) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/announcement/edit/' . $announcement->id) . '&nbsp;&nbsp;';
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= $this->button_delete('/announcement/delete/' . $announcement->id, 'data-item="' . $announcement->title . '"') . '&nbsp;&nbsp;';
					}
					$buttons .= $this->button_view('/announcement/v/' . $announcement->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

					$row = array(
						$announcement->level,
						$announcement->title,
						$announcement->created_at,
						$announcement->updated_at,
						// date('D, M d, Y', strtotime($announcement->created_at)),
						// date('D, M d, Y', strtotime($announcement->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Announcement->count_filtered($params);
				$recordsTotal = $this->Announcement->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_administrative_staff()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Update announcement user (user notification)
			$this->update_notification_by_module('Announcement');

			$params['select'] = 'announcement.*, level.level';
			$params['join'][] = array('level', 'level.id = announcement.level_id', 'left');
			$announcements = $this->Announcement->get($params);

			if ($announcements != FALSE) {
				foreach ($announcements as $announcement) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/announcement/edit/' . $announcement->id) . '&nbsp;&nbsp;';
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= $this->button_delete('/announcement/delete/' . $announcement->id, 'data-item="' . $announcement->title . '"') . '&nbsp;&nbsp;';
					}
					$buttons .= $this->button_view('/announcement/v/' . $announcement->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

					$row = array(
						$announcement->level,
						$announcement->title,
						$announcement->created_at,
						$announcement->updated_at,
						// date('D, M d, Y', strtotime($announcement->created_at)),
						// date('D, M d, Y', strtotime($announcement->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Announcement->count_filtered($params);
				$recordsTotal = $this->Announcement->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_teacher()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Update announcement user (user notification)
			$this->update_notification_by_module('Announcement');

			$params['select'] = 'announcement.*, level.level';
			$params['join'][] = array('level', 'level.id = announcement.level_id', 'left');
			$announcements = $this->Announcement->get($params);

			if ($announcements != FALSE) {
				foreach ($announcements as $announcement) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/announcement/edit/' . $announcement->id) . '&nbsp;&nbsp;';
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= $this->button_delete('/announcement/delete/' . $announcement->id, 'data-item="' . $announcement->title . '"') . '&nbsp;&nbsp;';
					}
					$buttons .= $this->button_view('/announcement/v/' . $announcement->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

					$row = array(
						$announcement->level,
						$announcement->title,
						date('D, M d, Y', strtotime($announcement->created_at)),
						date('D, M d, Y', strtotime($announcement->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Announcement->count_filtered($params);
				$recordsTotal = $this->Announcement->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_student()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('title', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Update announcement user (user notification)
			$this->update_notification_by_module('Announcement');

			$params['clauses']['where'] = array('level_id' => $this->sess_user->student_session->level_id);
			$announcements = $this->Announcement->get($params);

			if ($announcements != FALSE) {
				foreach ($announcements as $announcement) {
					$buttons = '';
					$buttons .= $this->button_view('/announcement/v/' . $announcement->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

					$row = array(
						$announcement->title,
						date('D, M d, Y', strtotime($announcement->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Announcement->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Announcement->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_parents()
	{
		$data = $level_ids = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Update announcement user (user notification)
			$this->update_notification_by_module('Announcement');

			// Get student level
			foreach ($this->sess_user->children as $student) {
				$student_session = $this->get_student_session($student->id);
				$level_ids[] = intval($student_session->level_id);
			}

			$params['select'] = 'announcement.*, level.level';
			$params['join'][] = array('level', 'level.id = announcement.level_id', 'left');
			$params['clauses']['where_in'] = array('level_id' => $level_ids);
			$announcements = $this->Announcement->get($params);

			if ($announcements != FALSE) {
				foreach ($announcements as $announcement) {
					$buttons = '';
					$buttons .= $this->button_view('/announcement/v/' . $announcement->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

					$row = array(
						$announcement->level,
						$announcement->title,
						date('D, M d, Y', strtotime($announcement->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Announcement->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Announcement->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('announcements', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'announcement')));

		$data = array();
		$data['page_title'] = 'New Announcement';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[announcement.title]');
		$this->form_validation->set_rules('body', 'Body', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Announcement', 'Announcement added successfully.'));
				redirect('announcements');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Announcement Failed', 'Failed create announcement. There was database failure.');
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('announcement/add', $data);
	}

	private function create()
	{
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$body = $this->input->post('body');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('level_id', 'title', 'body', 'created_at', 'updated_at');

		$result = $this->Announcement->insert($data);

		if ($result === TRUE) $this->log_history('announcement', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('announcements', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'announcement')));

		$data = array();
		$data['page_title'] = 'Edit Announcement';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$announcement = $this->Announcement->get_by_id($id);
		if ($announcement == FALSE) $this->go_to('announcements', $this->toastr('error', 'Announcement', sprintf('Announcement id %s not found', $id)));
		$data['announcement'] = $announcement;

		$this->load->library('form_validation');
		if ($this->input->post()) {
			if ($this->input->post('title') != $announcement->title) $this->form_validation->set_rules('title', 'Title', 'required|is_unique[announcement.title]');
		}
		$this->form_validation->set_rules('body', 'Body', 'required');
		$this->form_validation->set_rules('level_id', 'Level', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Announcement', 'Announcement updated successfully.'));
				redirect('announcements');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Announcement Failed', 'Failed update announcement. There was database failure.');
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('announcement/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$body = $this->input->post('body');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Announcement->get_by_id($id);

		if ($old_data == FALSE) return FALSE;

		$data = compact('level_id', 'title', 'body', 'updated_at');
		$where = compact('id');

		$result =  $this->Announcement->update($data, $where);

		if ($result == TRUE) $this->log_history('announcement', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('announcements', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'announcement')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('announcement', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {
			$announcement = $this->Announcement->get_by_id($id);
			if ($announcement == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Announcement Failed', sprintf('Announcement id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Announcement->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Announcement Failed', 'Database failure');
				break;
			}

			$this->log_history('announcement', 'delete', '', json_encode($announcement), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Announcement <strong>%s</strong> deleted.', $announcement->title));

		} while (FALSE);

		$this->output_json($response);
	}

	public function v($id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('announcement', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$announcement = $this->Announcement->get_by_id($id);
		$this->output_json($announcement);
	}
}
