<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		if ( ! $this->session->flashdata('password_changed')) $this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('profile');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'profile')));

		$data = array();
		$data['page_title'] = 'Profile';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|min_length[8]|callback_password_check');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|alpha_numeric|min_length[8]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->change_password()) {
				$toastr = $this->toastr('success', 'Change Password', 'Password changed successfully.');
				$this->session->set_flashdata('password_changed', 'success');
			} else {
				$toastr = $this->toastr('error', 'Change Password', 'Change password failed.');
			}
			$this->session->set_flashdata('toastr', $toastr);
			redirect('profile');
		}

		$data['tabs'] = $this->get_tabs();

		$this->my_view('profile/index', $data);
	}

	private function get_tabs()
	{
		$tabs = $data = array();
		$pages = array('profile', 'change password');
		$data['sess_user'] = $this->sess_user;

		foreach ($pages as $page) {
			switch ($page) {
				case 'profile':
					$id = 'tabProfile';
					$is_active = ($this->input->post() == NULL && $this->session->flashdata('toastr') == NULL) ? ' active' : '';
					$href = '#profile';
					$text = 'Profile';
					$content_id = 'profile';
					$content_body = $this->get_profile_body();
					break;

				case 'change password':
					$id = 'tabChangePassword';
					$is_active = ($this->input->post() != NULL OR $this->session->flashdata('toastr')) ? ' active' : '';
					$href = '#changePassword';
					$text = 'Change Password';
					$content_id = 'changePassword';
					$content_body = $this->load->view('profile/change_password', $data, TRUE);
					break;
			}
			$tabs[] = compact('id', 'is_active', 'href', 'text', 'content_id', 'content_body');
		}

		return $tabs;
	}

	private function change_password()
	{
		$new_password = password_hash($this->input->post('new_password'), PASSWORD_DEFAULT);

		$data = array('password' => $new_password, 'updated_at' => date('Y-m-d H:i:s'));
		$where = array('id' => $this->encryption->decrypt($this->session->userdata('sess_user_id')));
		return $this->User->update($data, $where);
	}

	public function password_check($current_password = '')
	{
		if ($current_password != '' && ! password_verify($current_password, $this->sess_user->password)) {
			$this->form_validation->set_message('password_check', 'The {field} field is not the same as session password.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	private function get_profile_body()
	{
		$data['sess_user'] = $this->sess_user;

		switch ($this->sess_user->role->name)
		{
			case 'Administrator':
			case 'Super User':
				return $this->load->view('profile/admin', $data, TRUE);
			break;

			case 'Administrative Staff':
			case 'Teacher':
				return $this->load->view('profile/staff', $data, TRUE);
			break;

			case 'Student':
				$this->load->model('student_session_model', 'StudentSession');
				$this->load->model('session_model', 'Session');
				$this->load->model('level_model', 'Level');
				$this->load->model('class_model', 'Class');
				$this->load->model('section_model', 'Section');

				// Student session
				$student_session = $this->StudentSession->get_by_student_id($this->sess_user->profile->id);

				if ($student_session !== FALSE) {
					$data['session'] = $this->Session->get_by_id($student_session->session_id);
					$data['level'] = $this->Level->get_by_id($student_session->level_id);
					$data['class'] = $this->Class->get_by_id($student_session->class_id);
					$data['section'] = $this->Section->get_by_id($student_session->section_id);
				}

				return $this->load->view('profile/student', $data, TRUE);
			break;

			case 'Parents':
				return $this->load->view('profile/parents', $data, TRUE);
			break;
		}
	}
}
