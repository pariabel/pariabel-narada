<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploaded_file extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('uploaded_file_model', 'UploadedFile');
	}

	public function read($file_id = 0, $file_name = '')
	{
		do {
			if ($file_id === 0 || $file_name == '') show_404();

			$uploaded_file = $this->UploadedFile->get_by_id($file_id);
			// $this->dd($uploaded_file);

			if ($uploaded_file == FALSE) show_404();

			if ($uploaded_file->orig_name != $file_name) show_404();

			if ( ! file_exists($uploaded_file->full_path)) show_404();

			$this->output
				->set_content_type($uploaded_file->file_type)
				->set_output(file_get_contents($uploaded_file->full_path));

		} while(FALSE);
	}

	public function download($file_id = 0, $file_name = '')
	{
		$this->load->helper('download');

		do {
			if ($file_id === 0 || $file_name == '') show_404();

			$uploaded_file = $this->UploadedFile->get_by_id($file_id);
			// $this->dd($uploaded_file);

			if ($uploaded_file == FALSE) show_404();

			if ($uploaded_file->orig_name != $file_name) show_404();

			if ( ! file_exists($uploaded_file->full_path)) show_404();

			force_download($file_name, file_get_contents($uploaded_file->full_path));

		} while(FALSE);
	}
}

