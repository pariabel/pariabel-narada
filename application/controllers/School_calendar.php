<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_calendar extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('school_calendar');

		$this->load->model('school_calendar_model', 'SchoolCalendar');
		$this->load->model('uploaded_file_model', 'UploadedFile');
		$this->load->model('level_model', 'Level');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'school calender')));

		$data = array();
		$data['page_title'] = 'School Calendar';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
				$this->my_view('school_calendar/super_user/index', $data);
			break;

			case 'Administrative Staff':
				$this->my_view('school_calendar/administrative_staff/index', $data);
			break;

			case 'Teacher':
				$this->my_view('school_calendar/teacher/index', $data);
			break;

			case 'Student':
				$this->my_view('school_calendar/student/index', $data);
			break;

			case 'Parents':
				$this->my_view('school_calendar/parents/index', $data);
			break;

			default:
				show_404();
		}
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('school_calendar', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
				$this->datatable_super_user();
			break;

			case 'Administrative Staff':
				$this->datatable_administrative_staff();
			break;

			case 'Teacher':
				$this->datatable_teacher();
			break;

			case 'Student':
				$this->datatable_student();
			break;

			case 'Parents':
				$this->datatable_parents();
			break;

			default:
				show_404();
		}
	}

	private function datatable_administrative_staff()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, school_calendar.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = school_calendar.file_id');
			$params['join'][] = array('level', 'level.id = school_calendar.level_id', 'left');
			$school_calendars = $this->SchoolCalendar->get($params);

			if ($school_calendars != FALSE) {
				foreach ($school_calendars as $school_calendar) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/school_calendar/edit/' . $school_calendar->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/school_calendar/delete/' . $school_calendar->id, 'data-item="' . $school_calendar->title . '"');
					}
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $school_calendar->file_id . '/' . $school_calendar->file, 'data-item="' . $school_calendar->file . '"');

					$row = array(
						$school_calendar->level,
						$school_calendar->title,
						$school_calendar->created_at,
						$school_calendar->updated_at,
						// date('D, M d, Y', strtotime($school_calendar->created_at)),
						// date('D, M d, Y', strtotime($school_calendar->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->SchoolCalendar->count_filtered($params);
				$recordsTotal = $this->SchoolCalendar->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_super_user()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, school_calendar.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = school_calendar.file_id');
			$params['join'][] = array('level', 'level.id = school_calendar.level_id', 'left');
			$school_calendars = $this->SchoolCalendar->get($params);

			if ($school_calendars != FALSE) {
				foreach ($school_calendars as $school_calendar) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/school_calendar/edit/' . $school_calendar->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/school_calendar/delete/' . $school_calendar->id, 'data-item="' . $school_calendar->title . '"');
					}
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $school_calendar->file_id . '/' . $school_calendar->file, 'data-item="' . $school_calendar->file . '"');

					$row = array(
						$school_calendar->level,
						$school_calendar->title,
						$school_calendar->created_at,
						$school_calendar->updated_at,
						// date('D, M d, Y', strtotime($school_calendar->created_at)),
						// date('D, M d, Y', strtotime($school_calendar->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->SchoolCalendar->count_filtered($params);
				$recordsTotal = $this->SchoolCalendar->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_teacher()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, school_calendar.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = school_calendar.file_id');
			$params['join'][] = array('level', 'level.id = school_calendar.level_id', 'left');
			$school_calendars = $this->SchoolCalendar->get($params);

			if ($school_calendars != FALSE) {
				foreach ($school_calendars as $school_calendar) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/school_calendar/edit/' . $school_calendar->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/school_calendar/delete/' . $school_calendar->id, 'data-item="' . $school_calendar->title . '"');
					}
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $school_calendar->file_id . '/' . $school_calendar->file, 'data-item="' . $school_calendar->file . '"');

					$row = array(
						$school_calendar->level,
						$school_calendar->title,
						date('D, M d, Y', strtotime($school_calendar->created_at)),
						date('D, M d, Y', strtotime($school_calendar->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->SchoolCalendar->count_filtered($params);
				$recordsTotal = $this->SchoolCalendar->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_student()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('title', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, school_calendar.*';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = school_calendar.file_id');
			$params['clauses']['where'] = array('level_id' => $this->sess_user->student_session->level_id);
			$school_calendars = $this->SchoolCalendar->get($params);

			if ($school_calendars != FALSE) {
				foreach ($school_calendars as $school_calendar) {
					$buttons = '';
					$buttons .= $this->button_download('/c/d/' . $school_calendar->file_id . '/' . $school_calendar->file, 'data-item="' . $school_calendar->file . '"');

					$row = array(
						$school_calendar->title,
						date('D, M d, Y', strtotime($school_calendar->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->SchoolCalendar->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->SchoolCalendar->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_parents()
	{
		$data = $level_ids = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Get student level
			foreach ($this->sess_user->children as $student) {
				$student_session = $this->get_student_session($student->id);
				$level_ids[] = intval($student_session->level_id);
			}

			$params['select'] = 'uploaded_file.orig_name AS `file`, school_calendar.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = school_calendar.file_id');
			$params['join'][] = array('level', 'level.id = school_calendar.level_id', 'left');
			$params['clauses']['where_in'] = array('level_id' => $level_ids);
			$school_calendars = $this->SchoolCalendar->get($params);

			if ($school_calendars != FALSE) {
				foreach ($school_calendars as $school_calendar) {
					$buttons = '';
					$buttons .= $this->button_download('/c/d/' . $school_calendar->file_id . '/' . $school_calendar->file, 'data-item="' . $school_calendar->file . '"');

					$row = array(
						$school_calendar->level,
						$school_calendar->title,
						date('D, M d, Y', strtotime($school_calendar->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->SchoolCalendar->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->SchoolCalendar->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('school_calendar', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'school calender')));

		$data = array();
		$data['page_title'] = 'New School Calendar';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[school_calendar.title]');
		if (isset($_FILES['file']['name'])) $this->form_validation->set_rules('file', 'File', 'callback_file_check');

		if ($this->form_validation->run() == TRUE) {
			$result = $this->create();
			if ($result['code'] == 'SUCCESS') {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New School Calendar', 'School Calendar added successfully.'));
				redirect('school_calendar');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create School Calendar Failed', $result['message']);
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('school_calendar/add', $data);
	}

	private function create()
	{
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		// Upload file
		$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');

		if ($upload_res['code'] == 'FAILED') {
			return array(
				'code' => 'FAILED',
				'message' => $upload_res['message']
			);
		}

		// Create uploaded file data
		$file_id = $this->create_uploaded_file(json_decode($upload_res['data'], TRUE));

		$data = compact('level_id', 'title', 'file_id', 'created_at', 'updated_at');
		if ($this->SchoolCalendar->insert($data) === FALSE) {
			$error = $this->db->error();
			return array(
				'code' => 'FAILED',
				'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'There was database failure.'
			);
		}

		$this->log_history('school_calendar', 'create', json_encode($data));

		return array(
			'code' => 'SUCCESS',
			'message' => 'Created successfully'
		);
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('school_calendar', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'school calender')));

		$data = array();
		$data['page_title'] = 'Edit School Calendar';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$school_calendar = $this->SchoolCalendar->get_by_id($id);
		if ($school_calendar == FALSE) $this->go_to('school_calendar', $this->toastr('error', 'School Calendar', sprintf('School Calendar id %s not found', $id)));
		$data['school_calendar'] = $school_calendar;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		if ($this->input->post('title') && ($this->input->post('title') != $school_calendar->title)) $this->form_validation->set_rules('title', 'Title', 'required|is_unique[school_calendar.title]');

		if ($this->form_validation->run() == TRUE) {
			$result = $this->update();
			if ($result['code'] == 'SUCCESS') {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit School Calendar', 'School Calendar updated successfully.'));

				redirect('school_calendar');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update School Calendar Failed', $result['message']);
			}
		}

		// Uploaded file
		$data['uploaded_file'] = $this->UploadedFile->get_by_id($school_calendar->file_id);

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('school_calendar/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$file_id = $this->input->post('file_id');
		$updated_at = date('Y-m-d H:i:s');

		if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
			$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');

			if ($upload_res['code'] == 'FAILED') {
				return array(
					'code' => 'FAILED',
					'message' => $upload_res['message']
				);
			} else {
				// Delete old uploaded file
				$uploaded_file = $this->UploadedFile->get_by_id($file_id);
				if ($uploaded_file !== FALSE) unlink($uploaded_file->full_path);

				// Update uploaded file data
				$this->update_uploaded_file($file_id, json_decode($upload_res['data'], TRUE));
			}
		}

		$old_data = $this->SchoolCalendar->get_by_id($id);
		$data = compact('level_id', 'title', 'updated_at');
		$where = compact('id');

		if ($this->SchoolCalendar->update($data, $where) == FALSE) {
			$error = $this->db->error();
			return array(
				'code' => 'FAILED',
				'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'There was database failure.'
			);
		}

		$this->log_history('school_calendar', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return array(
			'code' => 'SUCCESS',
			'message' => 'Updated successfully'
		);
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('school_calendar', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'school calender')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('school_calendar', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$school_calendar = $this->SchoolCalendar->get_by_id($id);
			if ($school_calendar == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Role Failed', sprintf('School Calendar id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->SchoolCalendar->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete School Calendar Failed', 'Database failure');
				break;
			}

			$this->delete_uploaded_file($school_calendar->file_id);

			$this->log_history('school_calendar', 'delete', '', json_encode($school_calendar), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('School Calendar <strong>%s</strong> deleted.', $school_calendar->title));

		} while (FALSE);

		$this->output_json($response);
	}

	public function file_check()
	{
		// $allowed_mime_type_arr = array(
		// 	'application/pdf'
		// );

		// return $this->uploaded_file_check($allowed_mime_type_arr, 'Please select only pdf file.');

		$allowed_mime_type_arr = array(
			'application/pdf',
			'application/msword',
			'application/vnd.ms-office',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/zip',
			'application/msword',
			'application/x-zip',
			'application/vnd.ms-excel',
			'application/msexcel',
			'application/x-msexcel',
			'application/x-ms-excel',
			'application/x-excel',
			'application/x-dos_ms_excel',
			'application/xls',
			'application/x-xls',
			'application/excel',
			'application/download',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		);

		return $this->uploaded_file_check($allowed_mime_type_arr, 'Please select only xls, xlsx, doc, docx, or pdf file.');
	}
}
