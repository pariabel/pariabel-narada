<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time_table extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('time_table');

		$this->load->model('time_table_model', 'TimeTable');
		$this->load->model('uploaded_file_model', 'UploadedFile');
		$this->load->model('level_model', 'Level');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'time table')));

		$data = array();
		$data['page_title'] = 'Time Table';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		switch ($this->sess_user->role->name) {
			case 'Administrator':
				$this->my_view('time_table/administrator/index', $data);
			break;

			case 'Super User':
				$this->my_view('time_table/super_user/index', $data);
			break;

			case 'Administrative Staff':
				$this->my_view('time_table/administrative_staff/index', $data);
			break;

			case 'Teacher':
				$this->my_view('time_table/teacher/index', $data);
			break;

			case 'Student':
				$this->my_view('time_table/student/index', $data);
			break;

			case 'Parents':
				$this->my_view('time_table/parents/index', $data);
			break;

			default:
				show_404();
		}
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('time_table', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
				$this->datatable_super_user();
			break;

			case 'Administrative Staff':
				$this->datatable_administrative_staff();
			break;

			case 'Teacher':
				$this->datatable_teacher();
			break;

			case 'Student':
				$this->datatable_student();
			break;

			case 'Parents':
				$this->datatable_parents();
			break;

			default:
				show_404();
		}
	}

	private function datatable_super_user()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, time_table.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = time_table.file_id');
			$params['join'][] = array('level', 'level.id = time_table.level_id', 'left');
			$time_tables = $this->TimeTable->get($params);

			if ($time_tables != FALSE) {
				foreach ($time_tables as $time_table) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/time_table/edit/' . $time_table->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/time_table/delete/' . $time_table->id, 'data-item="' . $time_table->title . '"');
					}
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $time_table->file_id . '/' . $time_table->file, 'data-item="' . $time_table->file . '"');

					$row = array(
						$time_table->level,
						$time_table->title,
						$time_table->created_at,
						$time_table->updated_at,
						// date('D, M d, Y', strtotime($time_table->created_at)),
						// date('D, M d, Y', strtotime($time_table->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->TimeTable->count_filtered($params);
				$recordsTotal = $this->TimeTable->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_administrative_staff()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, time_table.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = time_table.file_id');
			$params['join'][] = array('level', 'level.id = time_table.level_id', 'left');
			$time_tables = $this->TimeTable->get($params);

			if ($time_tables != FALSE) {
				foreach ($time_tables as $time_table) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/time_table/edit/' . $time_table->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/time_table/delete/' . $time_table->id, 'data-item="' . $time_table->title . '"');
					}
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $time_table->file_id . '/' . $time_table->file, 'data-item="' . $time_table->file . '"');

					$row = array(
						$time_table->level,
						$time_table->title,
						$time_table->created_at,
						$time_table->updated_at,
						// date('D, M d, Y', strtotime($time_table->created_at)),
						// date('D, M d, Y', strtotime($time_table->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->TimeTable->count_filtered($params);
				$recordsTotal = $this->TimeTable->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_teacher()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, time_table.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = time_table.file_id');
			$params['join'][] = array('level', 'level.id = time_table.level_id', 'left');
			$time_tables = $this->TimeTable->get($params);

			if ($time_tables != FALSE) {
				foreach ($time_tables as $time_table) {
					$buttons = '';

					if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
						$buttons .= $this->button_edit('/time_table/edit/' . $time_table->id);
					}
					if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
						$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/time_table/delete/' . $time_table->id, 'data-item="' . $time_table->title . '"');
					}
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $time_table->file_id . '/' . $time_table->file, 'data-item="' . $time_table->file . '"');

					$row = array(
						$time_table->level,
						$time_table->title,
						date('D, M d, Y', strtotime($time_table->created_at)),
						date('D, M d, Y', strtotime($time_table->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->TimeTable->count_filtered($params);
				$recordsTotal = $this->TimeTable->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_parents()
	{
		$data = $level_ids = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Get student level
			foreach ($this->sess_user->children as $student) {
				$student_session = $this->get_student_session($student->id);
				$level_ids[] = intval($student_session->level_id);
			}

			$params['select'] = 'uploaded_file.orig_name AS `file`, time_table.*, level.level';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = time_table.file_id');
			$params['join'][] = array('level', 'level.id = time_table.level_id', 'left');
			$params['clauses']['where_in'] = array('level_id' => $level_ids);
			$time_tables = $this->TimeTable->get($params);

			if ($time_tables != FALSE) {
				foreach ($time_tables as $time_table) {
					$buttons = '';
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $time_table->file_id . '/' . $time_table->file, 'data-item="' . $time_table->file . '"');

					$row = array(
						$time_table->level,
						$time_table->title,
						date('D, M d, Y', strtotime($time_table->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->TimeTable->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->TimeTable->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_student()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('title', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'uploaded_file.orig_name AS `file`, time_table.*';
			$params['join'][] = array('uploaded_file', 'uploaded_file.id = time_table.file_id');
			$params['clauses']['where'] = array('level_id' => $this->sess_user->student_session->level_id);
			$time_tables = $this->TimeTable->get($params);

			if ($time_tables != FALSE) {
				foreach ($time_tables as $time_table) {
					$buttons = '';
					$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $time_table->file_id . '/' . $time_table->file, 'data-item="' . $time_table->file . '"');

					$row = array(
						$time_table->title,
						date('D, M d, Y', strtotime($time_table->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->TimeTable->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->TimeTable->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('time_table', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'time table')));

		$data = array();
		$data['page_title'] = 'New Time Table';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[time_table.title]');
		if (isset($_FILES['file']['name'])) $this->form_validation->set_rules('file', 'File', 'callback_file_check');

		if ($this->form_validation->run() == TRUE) {
			$result = $this->create();
			if ($result['code'] == 'SUCCESS') {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Time Table', 'Time Table added successfully.'));
				redirect('time_table');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Time Table Failed', $result['message']);
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('time_table/add', $data);
	}

	private function create()
	{
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		// Upload file
		$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');

		if ($upload_res['code'] == 'FAILED') {
			return array(
				'code' => 'FAILED',
				'message' => $upload_res['message']
			);
		}

		// Create uploaded file data
		$file_id = $this->create_uploaded_file(json_decode($upload_res['data'], TRUE));

		$data = compact('level_id', 'title', 'file_id', 'created_at', 'updated_at');
		if ($this->TimeTable->insert($data) === FALSE) {
			$error = $this->db->error();
			return array(
				'code' => 'FAILED',
				'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'There was database failure.'
			);
		}

		$this->log_history('time_table', 'create', json_encode($data));

		return array(
			'code' => 'SUCCESS',
			'message' => 'Created successfully'
		);
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('time_table', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'time table')));

		$data = array();
		$data['page_title'] = 'Edit Time Table';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$time_table = $this->TimeTable->get_by_id($id);
		if ($time_table == FALSE) $this->go_to('time_table', $this->toastr('error', 'Time Table', sprintf('Time Table id %s not found', $id)));
		$data['time_table'] = $time_table;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		if ($this->input->post('title') && ($this->input->post('title') != $time_table->title)) $this->form_validation->set_rules('title', 'Title', 'required|is_unique[time_table.title]');

		if ($this->form_validation->run() == TRUE) {
			$result = $this->update();
			if ($result['code'] == 'SUCCESS') {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Time Table', 'Time Table updated successfully.'));
				redirect('time_table');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Failed update time table',$result['message']);
			}
		}

		// Uploaded file
		$data['uploaded_file'] = $this->UploadedFile->get_by_id($time_table->file_id);

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('time_table/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$file_id = $this->input->post('file_id');
		$updated_at = date('Y-m-d H:i:s');

		if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
			$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');

			if ($upload_res['code'] == 'FAILED') {
				return array(
					'code' => 'FAILED',
					'message' => $upload_res['message']
				);
			} else {
				// Delete old uploaded file
				$uploaded_file = $this->UploadedFile->get_by_id($file_id);
				if ($uploaded_file !== FALSE) unlink($uploaded_file->full_path);

				// Update uploaded file data
				$this->update_uploaded_file($file_id, json_decode($upload_res['data'], TRUE));
			}
		}

		$old_data = $this->TimeTable->get_by_id($id);
		$data = compact('title', 'updated_at');
		$where = compact('id');

		if ($this->TimeTable->update($data, $where) == FALSE) {
			$error = $this->db->error();
			return array(
				'code' => 'FAILED',
				'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'There was database failure.'
			);
		}

		$this->log_history('time_table', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return array(
			'code' => 'SUCCESS',
			'message' => 'Updated successfully'
		);
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('time_table', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'time table')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('time_table', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$time_table = $this->TimeTable->get_by_id($id);
			if ($time_table == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Time Table Failed', sprintf('Time Table id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->TimeTable->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Time Table Failed', 'Database failure');
				break;
			}

			$this->delete_uploaded_file($time_table->file_id);

			$this->log_history('time_table', 'delete', '', json_encode($time_table), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Time Table <strong>%s</strong> deleted.', $time_table->title));

		} while (FALSE);

		$this->output_json($response);
	}

	public function file_check()
	{
		// $allowed_mime_type_arr = array(
		// 	'application/pdf'
		// );

		// return $this->uploaded_file_check($allowed_mime_type_arr, 'Please select only pdf file.');

		$allowed_mime_type_arr = array(
			'application/pdf',
			'application/msword',
			'application/vnd.ms-office',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/zip',
			'application/msword',
			'application/x-zip',
			'application/vnd.ms-excel',
			'application/msexcel',
			'application/x-msexcel',
			'application/x-ms-excel',
			'application/x-excel',
			'application/x-dos_ms_excel',
			'application/xls',
			'application/x-xls',
			'application/excel',
			'application/download',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		);

		return $this->uploaded_file_check($allowed_mime_type_arr, 'Please select only xls, xlsx, doc, docx, or pdf file.');
	}
}
