<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assign_subjects extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('assign_subjects');

		$this->load->model('class_model', 'Class');
		$this->load->model('class_section_model', 'ClassSection');
		$this->load->model('class_subject_model', 'ClassSubject');
		$this->load->model('section_model', 'Section');
		$this->load->model('subject_model', 'Subject');
		$this->load->model('subject_teacher_model', 'SubjectTeacher');
		$this->load->model('staff_model', 'Staff');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to %s', 'assign subjects')));

		$data = array();
		$data['page_title'] = 'Assign Subjects';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		if ($this->input->post('search') == 'yes') {
			$class_id = $this->input->post('class_id');
			$section_id = $this->input->post('section_id');

			$this->session->set_userdata('class_id', $class_id);
			$this->session->set_userdata('section_id', $section_id);

			redirect('assign_subjects');

		} elseif ($this->input->post('update') == 'yes') {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Assign Subjects', 'Assign subjects updated successfully.'));
			} else {
				$this->session->set_flashdata('toastr', $this->toastr('error', 'Assign Subjects', 'Update assign subjects failed.'));
			}
			redirect('assign_subjects');
		}

		if ($this->session->userdata('class_id') && $this->session->userdata('section_id')) {
			$data['selected_class_id'] = $this->session->userdata('class_id');
			$data['selected_section_id'] = $this->session->userdata('section_id');

			$data['subjects'] = $this->ClassSubject->get_subject_by_class_id($this->session->userdata('class_id'));
			$data['sections'] = $this->ClassSection->get_section_by_class_id($this->session->userdata('class_id'));
			$data['teachers'] = $this->Staff->get_teachers();

			$data['subject_teacher'] = $this->get_subject_teacher($this->session->userdata('class_id'), $this->session->userdata('section_id'));
		}

		// Class options
		$data['classes'] = $this->get_class_options();

		$this->my_view('assign_subjects/index', $data);
	}

	private function get_class_options()
	{
		return $this->Class->get();
	}

	public function get_section_options($class_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assign_subjects', $this->toastr('error', 'Forbidden', 'No direct access allowed'));
		$this->output_json($this->ClassSection->get_section_by_class_id($class_id));
	}

	private function get_subject_teacher($class_id = 0, $section_id = 0)
	{
		$subject_teacher = $this->SubjectTeacher->get_by_class_section($class_id, $section_id);

		$data = array();
		foreach ( ($subject_teacher !== FALSE) ? $subject_teacher : array() as $st) {
			$data[$st->subject_id] = $st->teacher_id;
		}

		return $data;
	}

	private function update()
	{
		$class_id = $this->input->post('selected_class_id');
		$section_id = $this->input->post('selected_section_id');

		$teachers = $this->input->post('teacher');
		foreach ($teachers as $subject_id => $teacher_id) {
			if (intval($teacher_id) > 0) {
				$data = array(
					'class_id' => $class_id,
					'section_id' => $section_id,
					'subject_id' => $subject_id,
					'teacher_id' => $teacher_id,
				);
				$this->SubjectTeacher->replace($data);
			}
		}

		return TRUE;
	}
}
