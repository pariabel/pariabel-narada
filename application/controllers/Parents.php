<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('parents');

		$this->load->model('parents_model', 'Parents');
		$this->load->model('parents_user_model', 'ParentsUser');
		$this->load->model('user_role_model', 'UserRole');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'parents')));

		$data = array();
		$data['page_title'] = 'Parents';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('parents/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('parents/list', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('father', 'mother', 'is_active', 'created_at', 'updated_at');
		$this->search_columns = array('father', 'mother');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$parents = $this->Parents->get($params);

			if ($parents != FALSE) {
				foreach ($parents as $row) {
					$buttons = $this->button_edit('/parents/edit/'.$row->id);
					$buttons .= $this->button_delete('/parents/delete/'.$row->id, 'data-item="Parents"');

					$row = array(
						$row->father,
						$row->mother,
						$this->create_badge($badge_types[$row->is_active], $row->is_active),
						$row->created_at,
						$row->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Parents->count_filtered($params);
				$recordsTotal = $this->Parents->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('parents', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'parents')));

		$data = array();
		$data['page_title'] = 'New Parents';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('father', 'Father', 'required|is_unique[parents.father]');
		$this->form_validation->set_rules('father_email', 'Father Email', 'required|valid_email|callback_email_check');
		$this->form_validation->set_rules('mother', 'Mother', 'required|is_unique[parents.mother]');
		$this->form_validation->set_rules('mother_email', 'Mother Email', 'required|valid_email|callback_email_check');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create()) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Parents', 'Parents added successfully.'));
				redirect('parents/list');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Failed Create Parents', 'There was database failure.');
			}
		}

		$this->my_view('parents/add', $data);
	}

	private function create()
	{
		do {
			// ! Hardcode role id parents
			$role_id = 5;

			// Create user
			$user_id = $this->create_user();
			if ($user_id === FALSE) {
				$result = FALSE;
				break;
			}

			// Create user role
			$data = array('user_id' => $user_id, 'role_id' => $role_id);
			$result = $this->UserRole->replace($data);
			if ($result === FALSE) break;

			// Create parents
			$parents_id = $this->create_parents();
			if ($parents_id === FALSE) {
				$result = FALSE;
				break;
			}

			// Create parents user
			$data = array('parents_id' => $parents_id, 'user_id' => $user_id);
			$result = $this->ParentsUser->replace($data);
			if ($result === FALSE) break;

		} while (FALSE);

		return $result;
	}

	private function create_user()
	{
		$username = $this->input->post('username');
		$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('username', 'password', 'created_at', 'updated_at');

		$result = $this->User->insert($data);

		if ($result === TRUE) {
			$result = $this->db->insert_id();
			$this->log_history('user', 'create', json_encode($data));
		}

		return $result;
	}

	private function create_parents()
	{
		$father = $this->input->post('father');
		$mother = $this->input->post('mother');
		$father_email = $this->input->post('father_email');
		$mother_email = $this->input->post('mother_email');
		$address = $this->input->post('address');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('father', 'mother', 'father_email', 'mother_email', 'address', 'created_at', 'updated_at');

		$result = $this->Parents->insert($data);

		if ($result === TRUE) {
			$result = $this->db->insert_id();
			$this->log_history('parents', 'create', json_encode($data));
		}

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('parents', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'parents')));

		$data = array();
		$data['page_title'] = 'Edit Parents';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$parents = $this->Parents->get_by_id($id);
		if ($parents == FALSE) $this->go_to('parents/list', $this->toastr('error', 'Parents', sprintf('Parents id %s not found', $id)));
		$user = $this->ParentsUser->get_user_by_parents_id($id);
		$data['parents'] = $parents;
		$data['user'] = $user;

		$this->load->library('form_validation');
		if ($this->input->post()) {
			if ($this->input->post('father') != $parents->father) $this->form_validation->set_rules('father', 'Father', 'required|is_unique[parents.father]');
			if ($this->input->post('father_email') != $parents->father_email) $this->form_validation->set_rules('father_email', 'Father Email', 'required|valid_email|callback_email_check');
			if ($this->input->post('mother') != $parents->mother) $this->form_validation->set_rules('mother', 'Mother', 'required|is_unique[parents.mother]');
			if ($this->input->post('mother_email') != $parents->mother_email) $this->form_validation->set_rules('mother_email', 'Mother Email', 'required|valid_email|callback_email_check');
			if ($this->input->post('username') != $user->username) $this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
			if ($this->input->post('password') != '') $this->form_validation->set_rules('password', 'Password', 'min_length[8]');
		}
		$this->form_validation->set_rules('address', 'Address', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update()) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit User', 'Parents updated successfully.'));
				redirect('parents/list');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Failed update parents', 'There was database failure.');
			}
		}

		$this->my_view('parents/edit', $data);
	}

	private function update()
	{
		do {
			// Update user
			$result = $this->update_user();
			if ($result === FALSE) break;

			// update parents
			$result = $this->update_parents();
			if ($result === FALSE) break;

		} while (FALSE);

		return $result;
	}

	private function update_user()
	{
		$id = $this->input->post('user_id');
		$username = $this->input->post('username');
		$password = ($this->input->post('password') != '') ? password_hash($this->input->post('password'), PASSWORD_DEFAULT) : '';
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->User->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = ($password != '') ? compact('username', 'password', 'is_active', 'updated_at') : compact('username', 'is_active', 'updated_at');
		$where = compact('id');
		$result = $this->User->update($data, $where);

		if ($result == TRUE) $this->log_history('user', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	private function update_parents()
	{
		$id = $this->input->post('id');
		$father = $this->input->post('father');
		$mother = $this->input->post('mother');
		$father_email = $this->input->post('father_email');
		$mother_email = $this->input->post('mother_email');
		$address = $this->input->post('address');
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Parents->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = compact('father', 'mother', 'father_email', 'mother_email', 'address', 'is_active', 'updated_at');
		$where = compact('id');
		$result = $this->Parents->update($data, $where);

		if ($result === TRUE) $this->log_history('parents', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('teachers', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'parents')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('parents/list', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array(
			'code' => 200,
			'swal' => array(
				'title' => 'Deleted!',
				'html' => '',
				'type' => 'success'
			)
		);

		$error = array(
			'code' => 500,
			'swal' => array(
				'title' => 'Delete Parents Failed',
				'html' => 'Database failure',
				'type' => 'error'
			)
		);

		do {
			// Get parents
			$parents = $this->Parents->get_by_id($id);
			if ($parents == FALSE)  {
				$response = $error;
				$response['code'] = 404;
				$response['swal']['html'] = sprintf('Parents id %s not found', $id);
				break;
			}

			// Get user
			$user = $this->ParentsUser->get_user_by_parents_id($id);
			if ($user == FALSE)  {
				$response = $error;
				$response['code'] = 404;
				$response['swal']['html'] = sprintf('User account for parents id %s not found', $id);
				break;
			}

			// Delete user
			if ($this->User->delete(array('id' => $user->id)) == FALSE) {
				$response = $error;
				$response['code'] = 500;
				$response['swal']['html'] = sprintf('Delete user account id: %s failed', $user->id);
				break;
			}

			// Delete parents
			$where = array('id' => $id);
			if ($this->Parents->delete($where) == FALSE) {
				$response = $error;
				break;
			}

			$this->log_history('parents', 'delete', '', json_encode($parents), json_encode($where));

			$response = $success;
			$response['swal']['html'] = 'Parents deleted.';

		} while (FALSE);

		$this->output_json($response);
	}

	public function name_check($str = '')
	{
		if ($str !== '') {
			if ($this->Parents->count_by_name($str) > 0) {
				$this->form_validation->set_message('name_check', 'The {field} field must contain a unique value.');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function email_check($str = '')
	{
		if ($str !== '') {
			if ($this->Parents->count_by_email($str) > 0) {
				$this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}
}
