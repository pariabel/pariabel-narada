<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('student');

		$this->load->model('student_model', 'Student');
		$this->load->model('student_user_model', 'StudentUser');
		$this->load->model('student_session_model', 'StudentSession');
		$this->load->model('student_parents_model', 'StudentParents');
		$this->load->model('class_model', 'Class');
		$this->load->model('class_section_model', 'ClassSection');
		$this->load->model('level_class_model', 'LevelClass');
		$this->load->model('parents_model', 'Parents');
		$this->load->model('parents_user_model', 'ParentsUser');
		$this->load->model('user_role_model', 'UserRole');
		$this->load->model('session_model', 'SessionModel');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'student')));

		$data = array();
		$data['page_title'] = 'Students';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		if ($this->input->post('search') == 'yes') {
			$class_id = $this->input->post('class_id');
			$section_id = $this->input->post('section_id');

			$this->session->set_userdata('students_class_id', $class_id);
			$this->session->set_userdata('students_section_id', $section_id);

			redirect('student/list');
		}

		if ($this->session->userdata('students_class_id') && $this->session->userdata('students_section_id')) {
			$data['sections'] = $this->ClassSection->get_section_by_class_id($this->session->userdata('students_class_id'));
		}

		// Class options
		$data['classes'] = $this->get_class_options();

		$this->my_view('student/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('student/list', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$class_id = ($this->input->post('class_id')) ?$this->input->post('class_id') : 0;
		$section_id = ($this->input->post('section_id')) ? $this->input->post('section_id') : 0;

		$data = array();
		$recordsFiltered = $recordsTotal = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('nis', 'name', 'is_active', 'created_at', 'updated_at');
		$this->search_columns = array('name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'student.*';
			$params['join'] = array(array('student_session', 'student_session.student_id = student.id AND student_session.class_id = ' . $class_id . ' AND student_session.section_id = ' . $section_id));
			$students = $this->Student->get($params);

			if ($students != FALSE) {
				foreach ($students as $student) {
					$buttons = $this->button_edit('/student/edit/'.$student->id);
					$buttons .= $this->button_delete('/student/delete/'.$student->id, 'data-item="'. $student->name . '"');

					$row = array(
						$student->nis,
						$student->name,
						$this->create_badge($badge_types[$student->is_active], $student->is_active),
						$student->created_at,
						$student->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Student->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Student->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('student/list', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'student')));

		$data = array();
		$data['page_title'] = 'New Student';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nis', 'NIS.', 'required|is_unique[student.nis]');
		$this->form_validation->set_rules('name', 'Student name', 'required|is_unique[student.name]');
		$this->form_validation->set_rules('class_id', 'Class', 'required');
		$this->form_validation->set_rules('section_id', 'Section', 'required');
		$this->form_validation->set_rules('student_username', 'Username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('student_password', 'Password', 'required|min_length[8]');
		$this->form_validation->set_rules('parents_info', 'Parents Information', 'required');
		if ($this->input->post() && $this->input->post('parents_info') == 'existing') {
			$this->form_validation->set_rules('father', 'Father', 'required');
			$this->form_validation->set_rules('father_email', 'Father Email', 'required');
			$this->form_validation->set_rules('mother', 'Mother', 'required');
			$this->form_validation->set_rules('mother_email', 'Mother Email', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');
		} else {
			$this->form_validation->set_rules('father', 'Father', 'required|is_unique[parents.father]');
			$this->form_validation->set_rules('father_email', 'Father Email', 'required|valid_email|is_unique[parents.father_email]');
			$this->form_validation->set_rules('mother', 'Mother', 'required|is_unique[parents.mother]');
			$this->form_validation->set_rules('mother_email', 'Mother Email', 'required|valid_email|is_unique[parents.mother_email]');
			$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('parents_username', 'Username', 'required|is_unique[user.username]');
			$this->form_validation->set_rules('parents_password', 'Password', 'required|min_length[8]');
		}

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Student', 'Student added successfully.'));
				$this->session->set_userdata('students_class_id', $this->input->post('class_id'));
				$this->session->set_userdata('students_section_id', $this->input->post('section_id'));
				redirect('student/list');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Student Failed', 'Failed create student. There was database failure.');
			}
		}

		// Class options
		$data['classes'] = $this->get_class_options();

		// Section options
		if ($this->input->post('class_id') && $this->input->post('class_id') > 0) $data['sections'] = $this->ClassSection->get_section_by_class_id($this->input->post('class_id'));

		$this->my_view('student/add', $data);
	}

	private function create()
	{
		do {
			// * STUDENT STARTS
			// ! Hardcode role id student
			$student_role_id = 4;

			// Create user
			$user_id = $this->create_user('student');
			if ($user_id === FALSE) {
				$result = FALSE;
				break;
			}

			// Create user role
			$data = array('user_id' => $user_id, 'role_id' => $student_role_id);
			$result = $this->UserRole->replace($data);
			if ($result === FALSE) break;

			// Create student
			$student_id = $this->create_student();
			if ($student_id === FALSE) {
				$result = FALSE;
				break;
			}

			// Create student user
			$data = array('student_id' => $student_id, 'user_id' => $user_id);
			$result = $this->StudentUser->replace($data);
			if ($result === FALSE) break;

			// Create student session
			$result =  $this->create_student_session($student_id);
			if ($result === FALSE) break;

			// * STUDENT ENDS

			$parents_info = $this->input->post('parents_info');

			// * PARENTS START
			if ($parents_info == 'new') {
				// ! Hardcode role id parents
				$role_id = 5;

				// Create user
				$user_id = $this->create_user('parents');
				if ($user_id === FALSE) {
					$result = FALSE;
					break;
				}

				// Create user role
				$data = array('user_id' => $user_id, 'role_id' => $role_id);
				$result = $this->UserRole->replace($data);
				if ($result === FALSE) break;

				// Create parents
				$parents_id = $this->create_parents();
				if ($parents_id === FALSE) {
					$result = FALSE;
					break;
				}

				// Create parents user
				$data = array('parents_id' => $parents_id, 'user_id' => $user_id);
				$result = $this->ParentsUser->replace($data);
				if ($result === FALSE) break;
			} elseif ($parents_info == 'existing') {
				$parents_id = $this->input->post('parents_id');

				if ($parents_id === 0) {
					$result = FALSE;
					break;
				}
			}
			// * PARENTS END

			$data = array('student_id' => $student_id, 'parents_id' => $parents_id);
			$this->StudentParents->replace($data);

		} while (FALSE);

		return $result;
	}

	private function create_user($role = '')
	{
		$result = FALSE;
		$username = $password = '';

		switch ($role) {
			case 'student':
				$username = $this->input->post('student_username');
				$password = password_hash($this->input->post('student_password'), PASSWORD_DEFAULT);
			break;

			case 'parents':
				$username = $this->input->post('parents_username');
				$password = password_hash($this->input->post('parents_password'), PASSWORD_DEFAULT);
			break;
		}

		if ($username != '' && $password != '') {
			$created_at = date('Y-m-d H:i:s');
			$updated_at = date('Y-m-d H:i:s');

			$data = compact('username', 'password', 'created_at', 'updated_at');

			$result = $this->User->insert($data);

			if ($result === TRUE) {
				$result = $this->db->insert_id();
				$this->log_history('user', 'create', json_encode($data));
			}
		}

		return $result;
	}

	private function create_student()
	{
		$nis = $this->input->post('nis');
		$name = $this->input->post('name');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('nis', 'name', 'created_at', 'updated_at');

		$result = $this->Student->insert($data);

		if ($result === TRUE) {
			$result = $this->db->insert_id();
			$this->log_history('student', 'create', json_encode($data));
		}

		return $result;
	}

	private function create_student_session($student_id = 0)
	{
		if ($student_id === 0) return FALSE;

		$class_id = $this->input->post('class_id');
		$section_id = $this->input->post('section_id');

		$active_session = $this->SessionModel->get_active_session();
		$level = $this->LevelClass->get_level_by_class_id($class_id);

		$session_id = $active_session->id;
		$level_id = $level->id;

		$data = compact('student_id', 'session_id', 'level_id', 'class_id', 'section_id');
		return $this->StudentSession->replace($data);
	}

	private function create_parents()
	{
		$father = $this->input->post('father');
		$mother = $this->input->post('mother');
		$father_email = $this->input->post('father_email');
		$mother_email = $this->input->post('mother_email');
		$address = $this->input->post('address');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('father', 'mother', 'father_email', 'mother_email', 'address', 'created_at', 'updated_at');

		$result = $this->Parents->insert($data);

		if ($result === TRUE) {
			$result = $this->db->insert_id();
			$this->log_history('parents', 'create', json_encode($data));
		}

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('student/list', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'student')));

		$data = array();
		$data['page_title'] = 'Edit Student';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$student = $this->Student->get_by_id($id);
		if ($student == FALSE) $this->go_to('student/list', $this->toastr('error', 'Student', sprintf('Student id %s not found', $id)));
		$student_session = $this->StudentSession->get_by_student_id($id);
		$student_user = $this->StudentUser->get_user_by_student_id($id);
		$parents = $this->StudentParents->get_parents_by_student_id($id);
		$parents_user = $this->ParentsUser->get_user_by_parents_id($parents->id);
		$data['student'] = $student;
		$data['student_session'] = $student_session;
		$data['student_user'] = $student_user;
		$data['parents'] = $parents;
		$data['parents_user'] = $parents_user;

		$this->load->library('form_validation');
		if ($this->input->post()) {
			if ($this->input->post('nis') != $student->nis) $this->form_validation->set_rules('nis', 'NIS.', 'required|is_unique[student.nis]');
			if ($this->input->post('name') != $student->name) $this->form_validation->set_rules('name', 'Student name', 'required|is_unique[student.name]');
			if ($this->input->post('student_username') != $student_user->username) $this->form_validation->set_rules('student_username', 'Username', 'required|is_unique[user.username]');
			if ($this->input->post('student_password') != '') $this->form_validation->set_rules('student_password', 'Password', 'min_length[8]');

			if ($this->input->post('no_changes') == 'yes') {
				if ($this->input->post('father') != $parents->father) $this->form_validation->set_rules('father', 'Father', 'required|is_unique[parents.father]');
				if ($this->input->post('father_email') != $parents->father_email) $this->form_validation->set_rules('father_email', 'Father Email', 'required|valid_email|is_unique[parents.father_email]');
				if ($this->input->post('mother') != $parents->mother) $this->form_validation->set_rules('mother', 'Mother', 'required|is_unique[parents.mother]');
				if ($this->input->post('mother_email') != $parents->mother_email) $this->form_validation->set_rules('mother_email', 'Mother Email', 'required|valid_email|is_unique[parents.mother_email]');
				if ($this->input->post('parents_username') != $parents_user->username) $this->form_validation->set_rules('parents_username', 'Username', 'required|is_unique[user.username]');
				if ($this->input->post('parents_password') != '') $this->form_validation->set_rules('parents_password', 'Password', 'min_length[8]');
			} else {
				if ($this->input->post('father') != $parents->father) $this->form_validation->set_rules('father', 'Father', 'required');
				if ($this->input->post('father_email') != $parents->father_email) $this->form_validation->set_rules('father_email', 'Father Email', 'required');
				if ($this->input->post('mother') != $parents->mother) $this->form_validation->set_rules('mother', 'Mother', 'required');
				if ($this->input->post('mother_email') != $parents->mother_email) $this->form_validation->set_rules('mother_email', 'Mother Email', 'required');
				if ($this->input->post('parents_username') != $parents_user->username) $this->form_validation->set_rules('parents_username', 'Username', 'required');
			}
		}
		$this->form_validation->set_rules('class_id', 'Class', 'required');
		$this->form_validation->set_rules('section_id', 'Section', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('no_changes', 'No Changes', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Student', 'Student updated successfully.'));
				redirect('student/list');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Student Failed', 'Failed update student. There was database failure.');
			}
		}

		// Class section options
		$data['classes'] = $this->get_class_options();
		$data['sections'] = $this->ClassSection->get_section_by_class_id($student_session->class_id);

		$this->my_view('student/edit', $data);
	}

	private function update()
	{
		do {
			// * STUDENT STARTS
			// Update user
			$result = $this->update_user('student');
			if ($result === FALSE) break;

			// Update student session
			$result = $this->update_student_session();
			if ($result === FALSE) break;

			// Update student
			$result = $this->update_student();
			if ($result === FALSE) break;
			// * STUDENT ENDS

			$no_changes = $this->input->post('no_changes');

			// * PARENTS STARTS
			if ($no_changes == 'yes') {
				// Update user
				$result = $this->update_user('parents');
				if ($result === FALSE) break;

				$result = $this->update_parents();
				if ($result === FALSE) break;
			}
			// * PARENTS END

			$data = array('student_id' => $this->input->post('id'), 'parents_id' => $this->input->post('parents_id'));
			$this->StudentParents->replace($data);

		} while (FALSE);

		return $result;
	}

	private function update_user($role = '')
	{
		$result = FALSE;
		$username = $password = '';

		switch ($role) {
			case 'student':
				$id = $this->input->post('student_user_id');
				$username = $this->input->post('student_username');
				$password = password_hash($this->input->post('student_password'), PASSWORD_DEFAULT);
			break;

			case 'parents':
				$id = $this->input->post('parents_user_id');
				$username = $this->input->post('parents_username');
				$password = password_hash($this->input->post('parents_password'), PASSWORD_DEFAULT);
			break;
		}


		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		if ($username != '' && $password != '') {
			$old_data = $this->User->get_by_id($id);
			if ($old_data == FALSE) return FALSE;

			$data = ($password != '') ? compact('username', 'password', 'is_active', 'updated_at') : compact('username', 'is_active', 'updated_at');
			$where = compact('id');
			$result = $this->User->update($data, $where);

			if ($result == TRUE) $this->log_history('user', 'update', json_encode($data), json_encode($old_data), json_encode($where));
		}

		return $result;
	}

	private function update_student()
	{
		$id = $this->input->post('id');
		$nis = $this->input->post('nis');
		$name = $this->input->post('name');
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Student->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = compact('nis', 'name', 'is_active', 'updated_at');
		$where = compact('id');
		$result = $this->Student->update($data, $where);

		if ($result === TRUE) $this->log_history('student', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	private function update_student_session()
	{
		$student_id = $this->input->post('id');
		$class_id = $this->input->post('class_id');
		$section_id = $this->input->post('section_id');

		$active_session = $this->SessionModel->get_active_session();
		$level = $this->LevelClass->get_level_by_class_id($class_id);

		$session_id = $active_session->id;
		$level_id = $level->id;

		$data = compact('student_id', 'session_id', 'level_id', 'class_id', 'section_id');
		return $this->StudentSession->replace($data);
	}

	private function update_parents()
	{
		$id = $this->input->post('parents_id');
		$father = $this->input->post('father');
		$mother = $this->input->post('mother');
		$father_email = $this->input->post('father_email');
		$mother_email = $this->input->post('mother_email');
		$address = $this->input->post('address');
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Parents->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = compact('father', 'mother', 'father_email', 'mother_email', 'address', 'is_active', 'updated_at');
		$where = compact('id');
		$result = $this->Parents->update($data, $where);

		if ($result === TRUE) $this->log_history('parents', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('teachers', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'student')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('student/list', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200, 'swal' => $this->swal('success', 'Deleted!', ''));

		$error = array('code' => 500, 'swal' => $this->swal('error', 'Delete Parents Failed', 'Database failure'));

		do {
			// Get student
			$student = $this->Student->get_by_id($id);
			if ($student == FALSE)  {
				$response = $error;
				$response['code'] = 404;
				$response['swal']['html'] = sprintf('Student id %s not found', $id);
				break;
			}

			// Get user
			$user = $this->StudentUser->get_user_by_student_id($id);
			if ($user == FALSE)  {
				$response = $error;
				$response['code'] = 404;
				$response['swal']['html'] = sprintf('User account for student id %s not found', $id);
				break;
			}

			// Delete user
			if ($this->User->delete(array('id' => $user->id)) == FALSE) {
				$response = $error;
				$response['code'] = 500;
				$response['swal']['html'] = sprintf('Delete user account id: %s failed', $user->id);
				break;
			}

			// Delete student
			$where = array('id' => $id);
			if ($this->Student->delete($where) == FALSE) {
				$response = $error;
				break;
			}

			$this->log_history('student', 'delete', '', json_encode($student), json_encode($where));

			$response = $success;
			$response['swal']['html'] = 'Student <strong>' . $student->name. '</strong> deleted.';

		} while (FALSE);

		$this->output_json($response);
	}

	public function parents()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('student/list', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$q = ($this->input->post('q')) ?$this->input->post('q') : '';

		$data = array();
		$recordsFiltered = $recordsTotal = 0;

		// Set datatables order and search columns
		$this->order_columns = array('father', 'mother', 'address');
		$this->search_columns = array();

		$params = $this->datatable_params();
		$params['clauses'] = array('where' => array('is_active' => 'yes'), 'like' => array('father' => $q), 'or_like' => array('mother' => $q));
		$parents = $this->Parents->get($params);

		if ($parents != FALSE) {
			foreach ($parents as $p) {
				$row = array(
					$p->father,
					$p->mother,
					$p->address,
					sprintf('<button type="button" class="btn btn-sm btn-primary do-copy" data-parentsid="%s" data-father="%s" data-fatheremail="%s" data-mother="%s" data-motheremail="%s" data-address="%s"><i class="far fa-sm fa-copy"></i> Copy</button>', $p->id, $p->father, $p->father_email, $p->mother, $p->mother_email, $p->address)
				);
				$data[] = $row;
			}

			$recordsFiltered = $this->Parents->count_filtered($params);
			$recordsTotal = $this->Parents->count_filtered($params);
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function get_class_options()
	{
		return $this->Class->get();
	}

	public function get_section_options($class_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('student/list', $this->toastr('error', 'Forbidden', 'No direct access allowed'));
		$this->output_json($this->ClassSection->get_section_by_class_id($class_id));
	}

	public function name_check($str = '')
	{
		if ($str !== '') {
			if ($this->Parents->count_by_name($str) > 0) {
				$this->form_validation->set_message('name_check', 'The {field} field must contain a unique value.');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function email_check($str = '')
	{
		if ($str !== '') {
			if ($this->Parents->count_by_email($str) > 0) {
				$this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}
}
