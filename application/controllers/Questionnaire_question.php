<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire_question extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('questionnaire_question');

		$this->load->model('questionnaire_model', 'Questionnaire');
		$this->load->model('questionnaire_question_model', 'Question');
		$this->load->model('qquestion_option_model', 'QuestionOption');
	}

	public function index($questionnaire_id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'questionnaire')));

		if ( ! in_array($this->sess_user->role->name, array('Administrator', 'Super User', 'Administrative Staff'))) show_404();

		$questionnaire = $this->Questionnaire->get_by_id($questionnaire_id);
		if ($questionnaire == FALSE) $this->go_to('dashboard', $this->toastr('error', 'Oops!', sprintf('Questionnaire id %s not found', $questionnaire_id)));
		if ($questionnaire->created_by != $this->sess_user->id) $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access questionnaire id:%s', $questionnaire_id)));

		$this->session->set_userdata('questionnaire', $questionnaire);

		$data = array();
		$data['page_title'] = 'Questions';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
		$data['questionnaire'] = $questionnaire;

		$this->my_view('questionnaire_question/index', $data);
	}

	public function datatable($questionnaire_id = 0)
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('id', 'question', 'answer_type', 'created_at', 'updated_at');
		$this->search_columns = array('question');

		$params = $this->datatable_params();

		$questionnaire = $this->Questionnaire->get_by_id($questionnaire_id);
		if ($questionnaire->created_by == $this->sess_user->id) {
			$params['clauses']['where'] = array('questionnaire_id' => $questionnaire_id);
			$questions = $this->Question->get($params);

			if ($questions != FALSE) {
				foreach ($questions as $question) {
					$buttons = $this->button_edit('/questionnaire_question/edit/' . $question->id);
					$buttons .= $this->button_delete('/questionnaire_question/delete/' . $question->id, 'data-item="' . $question->question . '"');

					$row = array(
						$question->id,
						$question->question,
						$question->option_type,
						$question->created_at,
						$question->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Question->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Question->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! $this->session->userdata('questionnaire')) redirect('questionnaire');
		$questionnaire = $this->session->userdata('questionnaire');

		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'questionnaire')));

		if ($questionnaire->created_by != $this->sess_user->id) $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access questionnaire id:%s', $questionnaire->id)));

		$data = array();
		$data['page_title'] = 'New Question';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('question', 'Question', 'required');
		if ($this->input->post()) {
			foreach ($this->input->post('option') as $k => $v) {
				$this->form_validation->set_rules('option[' . $k . ']', 'Options', 'required');
			}
		}

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Question', 'Question added successfully.'));
				redirect('questionnaire/question/' . $questionnaire->id);
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Question Failed', 'Failed create question. There was database failure.');
			}
		}

		$data['questionnaire'] = $questionnaire;

		$this->my_view('questionnaire_question/add', $data);
	}

	private function create()
	{
		$questionnaire_id = $this->input->post('questionnaire_id');
		$question = $this->input->post('question');
		$option_type = $this->input->post('option_type');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('questionnaire_id', 'question', 'option_type', 'created_at', 'updated_at');

		$result = $this->Question->insert($data);

		if ($result == TRUE) {
			$qquestion_id = $this->db->insert_id();
			$this->log_history('qquestion_qq', 'create', json_encode($data));
			$result = $this->create_options($qquestion_id);
		}

		return $result;
	}

	private function create_options($qquestion_id = 0)
	{
		if ($qquestion_id === 0) return FALSE;

		$options = $this->input->post('option');

		foreach ($options as $option) {
			$data = array('qquestion_id' => $qquestion_id, 'option' => $option);

			if ($this->QuestionOption->insert($data) == FALSE) return FALSE;
		}

		return TRUE;
	}

	public function edit($id = 0)
	{
		if ( ! $this->session->userdata('questionnaire')) redirect('questionnaire');
		$questionnaire = $this->session->userdata('questionnaire');

		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'questionnaire')));

		if ($questionnaire->created_by != $this->sess_user->id) $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access questionnaire id:%s', $questionnaire->id)));

		$data = array();
		$data['page_title'] = 'Edit Question';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('question', 'Question', 'required');
		if ($this->input->post()) {
			foreach ($this->input->post('option') as $k => $v) {
				$this->form_validation->set_rules('option[' . $k . ']', 'Options', 'required');
			}
			foreach (($this->input->post('new_option')) ? $this->input->post('new_option') : array() as $k => $v) {
				$this->form_validation->set_rules('new_option[' . $k . ']', 'Options', 'required');
			}
		}

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Question', 'Question updated successfully.'));
				redirect('questionnaire/question/' . $questionnaire->id);
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Question Failed', 'Failed update question. There was database failure.');
			}
		}

		$data['questionnaire'] = $questionnaire;

		if ($this->input->post('id')) $id = $this->input->post('id');
		$question = $this->Question->get_by_id($id);
		if ($question == FALSE) $this->go_to('questionnaire/question/' . $questionnaire->id, $this->toastr('error', 'Questionnaire Question', sprintf('Question id %s not found', $id)));
		$data['question'] = $question;

		// Get options
		$options = $this->QuestionOption->get_by_question_id($question->id);
		$data['options'] = $options;
		// $this->dd($options);

		$this->my_view('questionnaire_question/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$questionnaire_id = $this->input->post('questionnaire_id');
		$question = $this->input->post('question');
		$option_type = $this->input->post('option_type');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Question->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = compact('questionnaire_id', 'question', 'option_type', 'updated_at');
		$where = compact('id');

		$result = $this->Question->update($data, $where);

		if ($result == TRUE) $this->log_history('qquestion_qq', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		$result = $this->update_options($id);

		return $result;
	}

	private function update_options($qquestion_id = 0)
	{
		if ($qquestion_id === 0) return FALSE;

		$old_options = $this->QuestionOption->get_by_question_id($qquestion_id);

		$options = $this->input->post('option');
		$new_options = ($this->input->post('new_option')) ? $this->input->post('new_option') : array();

		foreach ($old_options as $option) {
			$data = array('option' => $options[$option->id]);
			$where = array('id' => $option->id);
			if (isset($options[$option->id])) {
				if ($this->QuestionOption->update($data, $where) == FALSE) return FALSE;
			} else {
				if ($this->QuestionOption->delete($where) == FALSE) return FALSE;
			}
		}

		foreach ($new_options as $new_option) {
			$data = array('qquestion_id' => $qquestion_id, 'option' => $new_option);
			if ($this->QuestionOption->insert($data) == FALSE) return FALSE;
		}

		return TRUE;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'questionnaire question')));

		$questionnaire = $this->session->userdata('questionnaire');

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('questionnaire/question/' . $questionnaire->id, $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {
			$question = $this->Question->get_by_id($id);
			if ($question == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Question Failed', sprintf('Question id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Question->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Question Failed', 'Database failure');
				break;
			}

			$this->log_history('qquestion_qq', 'delete', '', json_encode($question), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Question <strong>%s</strong> deleted.', $question->question));

		} while (FALSE);

		$this->output_json($response);
	}
}
