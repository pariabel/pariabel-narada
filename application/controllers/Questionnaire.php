<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('questionnaire');

		$this->load->model('questionnaire_model', 'Questionnaire');
		$this->load->model('questionnaire_user_model', 'QuestionnaireUser');
		$this->load->model('questionnaire_question_model', 'Question');
		$this->load->model('qquestion_option_model', 'QuestionOption');
		$this->load->model('qqoption_user_model', 'OptionUser');
		$this->load->model('level_model', 'Level');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'questionnaire')));

		$data = array();
		$data['page_title'] = 'Questionnaire';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
		$data['swal'] = ($this->session->flashdata('swal')) ? $this->session->flashdata('swal') : '';

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
			case 'Administrative Staff':
				$this->my_view('questionnaire/administrative_staff/index', $data);
			break;

			case 'Teacher':
				$this->my_view('questionnaire/teacher/index', $data);
			break;

			case 'Student':
				$this->my_view('questionnaire/student/index', $data);
			break;

			case 'Parents':
				$this->my_view('questionnaire/parents/index', $data);
			break;

			default:
				show_404();
		}
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		switch ($this->sess_user->role->name) {
			case 'Administrator':
			case 'Super User':
			case 'Administrative Staff':
				$this->datatable_admininistrative_staff();
			break;

			case 'Teacher':
				$this->datatable_teacher();
			break;

			case 'Student':
				$this->datatable_student();
			break;

			case 'Parents':
				$this->datatable_parents();
			break;

			default:
				show_404();
		}
	}

	private function datatable_admininistrative_staff()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'respondent', NULL, 'is_active', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'questionnaire_q.*, level.level';
			$params['join'][] = array('level', 'level.id = questionnaire_q.level_id', 'left');
			$questionnaires = $this->Questionnaire->get($params);

			if ($questionnaires != FALSE) {
				foreach ($questionnaires as $questionnaire) {
					$buttons = '';
					if ($questionnaire->created_by == $this->sess_user->id) {
						$buttons .= $this->button_edit('/questionnaire/edit/' . $questionnaire->id);
						$buttons .= $this->button_delete('/questionnaire/delete/' . $questionnaire->id, 'data-item="' . $questionnaire->title . '"');
						$buttons .= '<a href="/questionnaire/question/' . $questionnaire->id . '" class="btn btn-sm btn-warning mb-1 mr-1"><i class="fas fa-sm fa-question-circle"></i> Questions</a>';
						$buttons .= $this->button_view('/questionnaire/v/' . $questionnaire->id);
						$buttons .= '<a href="/questionnaire/result/' . $questionnaire->id . '" class="btn btn-sm btn-success mb-1 mr-1"><i class="fas fa-sm fa-chart-bar"></i> Result</a>';
					}
					$created_by = '<a class="btn btn-sm btn-info" href="/questionnaire/staff/' . $questionnaire->created_by . '" data-toggle="modal" data-target="#modalStaff"><i class="fas fa-sm fa-user-tie"></i> Staff</a>';

					$row = array(
						$questionnaire->level,
						$questionnaire->title,
						$questionnaire->respondent,
						$created_by,
						$this->create_badge($badge_types[$questionnaire->is_active], $questionnaire->is_active),
						$questionnaire->created_at,
						$questionnaire->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Questionnaire->count_filtered($params);
				$recordsTotal = $this->Questionnaire->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_teacher()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('level', 'title', 'respondent', 'created_at', 'updated_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = 'questionnaire_q.*, level.level';
			$params['join'][] = array('level', 'level.id = questionnaire_q.level_id', 'left');
			$questionnaires = $this->Questionnaire->get($params);

			if ($questionnaires != FALSE) {
				foreach ($questionnaires as $questionnaire) {
					$buttons = $this->button_view('/questionnaire/v/' . $questionnaire->id) . '&nbsp;&nbsp;';

					$row = array(
						$questionnaire->level,
						$questionnaire->title,
						$questionnaire->respondent,
						$this->create_badge($badge_types[$questionnaire->is_active], $questionnaire->is_active),
						date('D, M d, Y', strtotime($questionnaire->created_at)),
						date('D, M d, Y', strtotime($questionnaire->updated_at)),
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Questionnaire->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Questionnaire->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_student()
	{
		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('title', 'updated_at', 'responded_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['select'] = "questionnaire_q.*, IFNULL(questionnaire_user.created_at, '') AS responded_at";
			$params['join'][] = array('questionnaire_user', 'questionnaire_user.questionnaire_id = questionnaire_q.id AND questionnaire_user.user_id = ' . $this->sess_user->id, 'left');
			$params['clauses']['where'] = array('respondent' => 'Student', 'is_active' => 'yes', 'level_id' => $this->sess_user->student_session->level_id);
			$questionnaires = $this->Questionnaire->get($params);

			if ($questionnaires != FALSE) {
				foreach ($questionnaires as $questionnaire) {
					$buttons = '';
					if ($questionnaire->responded_at === '') {
						$buttons .= '<a href="/questionnaire/respond/' . $questionnaire->id . '" class="btn btn-sm btn-primary mb-1"><i class="fas fa-sm fa-tasks"></i> Respond</a>&nbsp;&nbsp;';
					}

					$row = array(
						$questionnaire->title,
						date('D, M d, Y', strtotime($questionnaire->updated_at)),
						($questionnaire->responded_at !== '') ? date('D, M d, Y', strtotime($questionnaire->responded_at)) : '-',
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Questionnaire->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Questionnaire->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_parents()
	{
		$data = $level_ids = array();
		$recordsTotal = $recordsFiltered = 0;
		$buttons = '';

		// Set datatables order and search columns
		$this->order_columns = array('title', 'updated_at', 'responded_at');
		$this->search_columns = array('title');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			// Get student level
			foreach ($this->sess_user->children as $student) {
				$student_session = $this->get_student_session($student->id);
				$level_ids[] = intval($student_session->level_id);
			}

			$params['select'] = "questionnaire_q.*, IFNULL(questionnaire_user.created_at, '') AS responded_at";
			$params['join'][] = array('questionnaire_user', 'questionnaire_user.questionnaire_id = questionnaire_q.id AND questionnaire_user.user_id = ' . $this->sess_user->id, 'left');
			$params['clauses']['where'] = array('respondent' => 'Parents', 'is_active' => 'yes');
			$params['clauses']['where_in'] = array('level_id' => $level_ids);
			$questionnaires = $this->Questionnaire->get($params);

			if ($questionnaires != FALSE) {
				foreach ($questionnaires as $questionnaire) {
					$buttons = '';
					if ($questionnaire->responded_at === '') {
						$buttons .= '<a href="/questionnaire/respond/' . $questionnaire->id . '" class="btn btn-sm btn-primary mb-1"><i class="fas fa-sm fa-tasks"></i> Respond</a>&nbsp;&nbsp;';
					}

					$row = array(
						$questionnaire->title,
						date('D, M d, Y', strtotime($questionnaire->updated_at)),
						($questionnaire->responded_at !== '') ? date('D, M d, Y', strtotime($questionnaire->responded_at)) : '-',
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Questionnaire->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Questionnaire->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'questionnaire')));

		$data = array();
		$data['page_title'] = 'New Questionnaire';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[questionnaire_q.title]');
		$this->form_validation->set_rules('respondent', 'Respondent', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Questionnaire', 'Questionnaire added successfully.'));
				redirect('questionnaire');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Questionnaire Failed', 'Failed create questionnaire. There was database failure.');
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('questionnaire/add', $data);
	}

	private function create()
	{
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$respondent = $this->input->post('respondent');
		$is_active = $this->input->post('is_active');
		$created_by = $this->sess_user->id;
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('level_id', 'title', 'description', 'respondent', 'is_active', 'created_by', 'created_at', 'updated_at');

		$result = $this->Questionnaire->insert($data);

		if ($result === TRUE) $this->log_history('questionnaire_q', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'questionnaire')));

		$data = array();
		$data['page_title'] = 'Edit Questionnaire';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$questionnaire = $this->Questionnaire->get_by_id($id);
		if ($questionnaire == FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('Questionnaire id %s not found', $id)));
		if ($questionnaire->created_by != $this->sess_user->id) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s id: %s', 'questionnaire', $id)));
		$data['questionnaire'] = $questionnaire;

		$this->load->library('form_validation');
		if ($this->input->post('title') && ($this->input->post('title') != $questionnaire->title)) $this->form_validation->set_rules('title', 'Title', 'required|is_unique[questionnaire_q.title]');
		$this->form_validation->set_rules('respondent', 'Respondent', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Questionnaire', 'Questionnaire updated successfully.'));
				redirect('questionnaire');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Questionnaire Failed', 'Failed update questionnaire. There was database failure.');
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		$this->my_view('questionnaire/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$level_id = $this->input->post('level_id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$respondent = $this->input->post('respondent');
		$is_active = $this->input->post('is_active');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Questionnaire->get_by_id($id);
		if ($old_data == FALSE) return FALSE;

		$data = compact('level_id', 'title', 'description', 'respondent', 'is_active', 'updated_at');
		$where = compact('id');

		$result =  $this->Questionnaire->update($data, $where);

		if ($result == TRUE) $this->log_history('questionnaire', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'questionnaire')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$questionnaire = $this->Questionnaire->get_by_id($id);
			if ($questionnaire == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Questionnaire Failed', sprintf('Questionnaire id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Questionnaire->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Questionnaire Failed', 'Database failure');
				break;
			}

			$this->log_history('questionnaire', 'delete', '', json_encode($questionnaire), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Questionnaire <strong>%s</strong> deleted.', $questionnaire->title));

		} while (FALSE);

		$this->output_json($response);
	}

	public function preview($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'questionnaire')));

		$questionnaire = $this->Questionnaire->get_by_id($id);
		if ($questionnaire == FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('Questionnaire id %s not found', $id)));
		if ($questionnaire->created_by != $this->sess_user->id) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access questionnaire id:%s', $id)));

		$data = $params = array();
		$data['page_title'] = 'Questionnaire';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
		$data['questionnaire'] = $questionnaire;

		// Get questions
		$params['clauses']['where'] = array('questionnaire_id' => $id);
		$questions = $this->Question->get($params);
		foreach ( ($questions !== FALSE) ? $questions : array() as $k => $question) {
			$questions[$k]->options = $this->QuestionOption->get_by_question_id($question->id);
		}
		$data['questions'] = $questions;

		$this->my_view('questionnaire/preview', $data);
	}

	public function result($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'questionnaire')));

		$questionnaire = $this->Questionnaire->get_by_id($id);
		if ($questionnaire == FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('Questionnaire id %s not found', $id)));
		if ($questionnaire->created_by != $this->sess_user->id) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access questionnaire id:%s', $id)));

		$data = $params = array();
		$data['page_title'] = 'Questionnaire Result';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
		$data['questionnaire'] = $questionnaire;

		// Get questions
		$params['clauses']['where'] = array('questionnaire_id' => $id);
		$questions = $this->Question->get($params);
		foreach ( ($questions !== FALSE) ? $questions : array() as $k => $question) {
			$questions[$k]->options = $this->QuestionOption->get_by_question_id($question->id);
			$questions[$k]->total_count = 0;
			foreach ($questions[$k]->options as $i => $option) {
				$j = ($i % 10);
				$questions[$k]->options[$i] = $option;
				$questions[$k]->options[$i]->bg_class = $this->config->item('bg_class')[$j];
				$questions[$k]->options[$i]->bg_color = $this->config->item('bg_color')[$j];
				$questions[$k]->options[$i]->user_count = $this->OptionUser->count_by_option_id($option->id);
				$questions[$k]->total_count += $questions[$k]->options[$i]->user_count;
			}
		}
		// $this->dd($questions);
		$data['questions'] = $questions;

		$this->my_view('questionnaire/result', $data);
	}

	public function respond($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'questionnaire')));

		if ( ! in_array($this->sess_user->role->name, array('Student', 'Parents'))) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to respond %s', 'questionnaire')));

		$data = $params = array();
		$data['page_title'] = 'Questionnaire';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$questionnaire = $this->Questionnaire->get_by_id($id);
		if ($questionnaire == FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', sprintf('Questionnaire id %s not found', $id)));
		$data['questionnaire'] = $questionnaire;

		// Check if user already responded
		if ($this->QuestionnaireUser->has_responded($questionnaire->id, $this->sess_user->id)) {
			$swal = array('icon' => 'error', 'title' => 'Questionnaire', 'text' => 'You have responded the questionnaire.');
			$this->session->set_flashdata('swal', $swal);
			redirect('questionnaire');
		}

		// Get questions
		$params['clauses']['where'] = array('questionnaire_id' => $id);
		$questions = $this->Question->get($params);
		foreach ( ($questions !== FALSE) ? $questions : array() as $k => $question) {
			$questions[$k]->options = $this->QuestionOption->get_by_question_id($question->id);
		}
		$data['questions'] = $questions;

		$this->load->library('form_validation');

		if ($this->input->post()) {
			foreach ($questions as $question) {
				$this->form_validation->set_rules(
					'option_question[' . $question->id . ']',
					'Question',
					'required',
					array('required' => $this->get_required_message($question->option_type))
				);
			}
		}

		if ($this->form_validation->run() == TRUE) {
			$result = $this->responding($questions);
			if ($result['code'] == '200') {
				$swal = array('icon' => 'success', 'title' => 'Questionnaire', 'text' => 'Thank You for responding the questionnaire.');
			} else {
				$swal = array('icon' => 'error', 'title' => 'Questionnaire', 'text' => 'Failed submit the questionnaire. Please try again later.');
			}
			$this->session->set_flashdata('swal', $swal);
			redirect('questionnaire');
		}

		$this->my_view('questionnaire/respond', $data);
	}

	private function responding($questions = array())
	{
		$success = array(
			'code' => 200,
			'message' => 'Success'
		);
		$error = array(
			'code' => 500,
			'message' => 'Error'
		);

		do {
			$questionnaire_id = $this->input->post('questionnaire_id');
			$created_at = date('Y-m-d H:i:s');

			$data = array(
				'questionnaire_id' => $questionnaire_id,
				'user_id' => $this->sess_user->id,
				'created_at' => $created_at
			);
			if ($this->QuestionnaireUser->insert($data) == FALSE) {
				$response = $error;
				break;
			}

			$option_question = $this->input->post('option_question');

			foreach ($questions as $question) {
				switch ($question->option_type) {
					case 'radio':
						$data = array(
							'option_id' => $option_question[$question->id],
							'user_id' => $this->sess_user->id,
							'created_at' => $created_at
						);
						$this->OptionUser->insert($data);
					break;

					case 'checkbox':
						foreach ($option_question[$question->id] as $option_id) {
							$data = array(
								'option_id' => $option_id,
								'user_id' => $this->sess_user->id,
								'created_at' => $created_at
							);
							$this->OptionUser->insert($data);
						}
					break;
				}
			}
		} while (FALSE);

		$response = $success;
		return $response;
	}

	private function get_required_message($option_type = '')
	{
		$msg = '';
		switch ($option_type) {
			case 'radio':
				$msg = 'Please choose one.';
			break;

			case 'checkbox':
				$msg = 'Please choose one or more.';
			break;

			default:
				$msg = 'The field is required.';
		}

		return $msg;
	}

	public function staff($user_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('questionnaire', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();

		do {
			if (! in_array($this->sess_user->role->name, array('Administrator', 'Super User', 'Administrative Staff'))) {
				$response = array('code' => 403, 'message' => 'You don\'t have permission to access staff.');
				break;
			}

			// Get user account
			$user = $this->User->get_by_id($user_id);
			if ($user == FALSE) {
				$response = array('code' => 404, 'message' => 'User not found.');
				break;
			}

			// Get user profile
			$this->load->model('staff_user_model', 'StaffUser');
			$staff = $this->StaffUser->get_staff_by_user_id($user_id);
			if ($staff == FALSE) {
				$response = array('code' => 404, 'message' => 'Staff not found.');
				break;
			}

			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => array(
					'nip' => $staff->nip,
					'name' => $staff->name,
					'username' => $user->username,
					'register_at' => date('F d, Y H:i:s', strtotime($user->created_at))
				)
			);

		} while (FALSE);

		$this->output_json($response);
	}
}
