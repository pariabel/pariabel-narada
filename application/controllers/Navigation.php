<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigation extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('navigation');

		$this->load->model('module_navigation_model', 'Navigation');
		$this->load->model('module_model', 'Module');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'navigation')));

		$data = array();
		$data['page_title'] = 'Navigation';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('navigation/index', $data);
	}

	public function datatable($parent_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('navigation', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		if ($parent_id === 0) {
			$this->datatable_parents();
		} else {
			$this->datatable_childs($parent_id);
		}
	}

	private function datatable_parents()
	{
		$data = array();
		$recordsFiltered = $recordsTotal = 0;

		// Set datatables order and search columns
		$this->order_columns = array('seq_num', 'name', 'icon', 'uri', NULL, 'updated_at');
		$this->search_columns = array('name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['clauses']['where'] = array('parent_id' => 0);
			$navigations = $this->Navigation->get($params);

			if ($navigations != FALSE) {
				foreach ($navigations as $navigation) {
					$buttons = $this->button_edit('/navigation/edit/' . $navigation->id);
					$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/navigation/delete/' . $navigation->id, 'data-item="' . $navigation->name . '"');

					$childs_count = $this->Navigation->count_childs($navigation->id);

					$btn_name = '<a class="btn btn-sm btn-block btn-info';
					$btn_name .= ($childs_count > 0) ? '' : ' disabled';
					$btn_name .= '" href="navigation/childs/' . $navigation->id . '">' . $navigation->name . '</a>';

					$row = array(
						'<input type="text" class="form-control" name="seq_num[' . $navigation->id . ']" value="' . $navigation->seq_num . '">',
						$btn_name,
						$navigation->icon,
						$navigation->uri,
						$childs_count,
						$navigation->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Navigation->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Navigation->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_childs($parent_id = 0)
	{
		$data = array();
		$recordsFiltered = $recordsTotal = 0;

		// Set datatables order and search columns
		$this->order_columns = array('seq_num', 'name', 'icon', 'uri', 'updated_at');
		$this->search_columns = array('name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$params['clauses']['where'] = array('parent_id' => $parent_id);
			$navigations = $this->Navigation->get($params);

			if ($navigations != FALSE) {
				foreach ($navigations as $navigation) {
					$buttons = $this->button_edit('/navigation/edit/' . $navigation->id);
					$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/navigation/delete/' . $navigation->id, 'data-item="' . $navigation->name . '"');

					$row = array(
						'<input type="text" class="form-control" name="seq_num[' . $navigation->id . ']" value="' . $navigation->seq_num . '">',
						$navigation->name,
						$navigation->icon,
						$navigation->uri,
						$navigation->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Navigation->count_filtered($params);
				if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
				if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
				$recordsTotal = $this->Navigation->count_filtered($params);
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('navigation', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'navigation')));

		$data = array();
		$data['page_title'] = 'New Navigation';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required|is_unique[module_navigation.name]');
		$this->form_validation->set_rules('uri', 'URI', 'required|is_unique[module_navigation.uri]');
		$this->form_validation->set_rules('module_id', 'Module', 'required');
		$this->form_validation->set_rules('action', 'Action', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Navigation', 'Navigation added successfully.'));
				redirect('navigation');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Navigation Failed', 'Failed create navigation. There was database failure.');
			}
		}

		// Parent navigation options
		$data['parent_navs'] = $this->get_parent_navigations();

		// Module options
		$data['modules'] = $this->Module->get();

		$this->my_view('navigation/add', $data);
	}

	private function create()
	{
		$parent_id = $this->input->post('parent_id');
		$name = $this->input->post('name');
		$icon = $this->input->post('icon');
		$uri = $this->input->post('uri');
		$module_id = $this->input->post('module_id');
		$action = $this->input->post('action');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('parent_id', 'name', 'icon', 'uri', 'module_id', 'action', 'created_at', 'updated_at');

		$result = $this->Navigation->insert($data);

		if ($result === TRUE) $this->log_history('module_navigation', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('navigation', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'navigation')));

		$data = array();
		$data['page_title'] = 'Edit Navigation';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$navigation = $this->Navigation->get_by_id($id);
		if ($navigation == FALSE) $this->go_to('navigation', $this->toastr('error', 'Navigation', sprintf('Navigation id %s not found', $id)));
		$data['navigation'] = $navigation;

		$this->load->library('form_validation');
		if ($this->input->post()) {
			if ($this->input->post('name') != $navigation->name) $this->form_validation->set_rules('name', 'Name', 'required|is_unique[module_navigation.name]');
			if ($this->input->post('uri') != $navigation->uri) $this->form_validation->set_rules('uri', 'URI', 'required|is_unique[module_navigation.uri]');
		}
		$this->form_validation->set_rules('module_id', 'Module', 'required');
		$this->form_validation->set_rules('action', 'Action', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Navigation', 'Navigation updated successfully.'));
				redirect('navigation');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Navigation Failed', 'Failed update navigation. There was database failure.');
			}
		}

		// Parent navigation options
		$data['parent_navs'] = $this->get_parent_navigations();

		// Module options
		$data['modules'] = $this->Module->get();

		$this->my_view('navigation/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$parent_id = $this->input->post('parent_id');
		$name = $this->input->post('name');
		$icon = $this->input->post('icon');
		$uri = $this->input->post('uri');
		$module_id = $this->input->post('module_id');
		$action = $this->input->post('action');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Navigation->get_by_id($id);

		if ($old_data == FALSE) return FALSE;

		$data = compact('parent_id', 'name', 'icon', 'uri', 'module_id', 'action', 'updated_at');
		$where = compact('id');

		return $this->Navigation->update($data, $where);
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('navigation', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'navigation')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('navigation', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {
			$navigation = $this->Navigation->get_by_id($id);
			if ($navigation == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Navigation Failed', sprintf('Navigation id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Navigation->delete($where) == FALSE) {
				$response = $error;
				break;
			}

			$this->log_history('module_navigation', 'delete', '', json_encode($navigation), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Navigation <strong>%s</strong> deleted.', $navigation->name));

		} while (FALSE);

		$this->output_json($response);
	}

	private function get_parent_navigations()
	{
		$params = array(
			'clauses' => array(
				'where' => array(
					'parent_id' => 0
				)
			)
		);

		$navs = $this->Navigation->get($params);

		return $navs;
	}

	public function update_sequence()
	{
		$seq_num = $this->input->post('seq_num');

		foreach ($seq_num as $id => $seq_num) {
			$result = $this->Navigation->update(array('seq_num' => $seq_num), array('id' => $id));
			if ($result == FALSE) break;
		}

		if ($result == TRUE) {
			$response = $this->response_success('Sequence updated successfully.');
		} else {
			$response = $this->response_error('Failed update sequence.');
		}

		$this->output_json($response);
	}

	public function childs($parent_id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'navigation')));

		$parent = $this->Navigation->get_by_id($parent_id);
		if ($parent == FALSE) $this->go_to('navigation', $this->toastr('error', '', sprintf('Navigation id: %s not found', $parent_id)));

		$data = array();
		$data['page_title'] = 'Navigation';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
		$data['parent'] = $parent;

		$this->my_view('navigation/childs', $data);
	}
}
