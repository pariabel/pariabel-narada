<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('user');

		$this->load->model('role_model', 'Role');
		$this->load->model('user_role_model', 'UserRole');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'user')));

		$data = array();
		$data['page_title'] = 'Users';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('user/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('users', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('id', 'username', 'is_active', 'created_at', 'updated_at');
		$this->search_columns = array('username');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$users = $this->User->get($params);

			if ($users != FALSE) {
				foreach ($users as $user) {
					$buttons = $this->button_edit('/user/edit/'.$user->id);
					$buttons .= '&nbsp;&nbsp;'.$this->button_delete('/user/delete/'.$user->id, 'data-item="'.$user->username.'"');

					$row = array(
						$user->id,
						$user->username,
						$this->create_badge($badge_types[$user->is_active], $user->is_active),
						$user->created_at,
						$user->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->User->count_filtered($params);
				$recordsTotal = $this->User->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('users', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'user')));

		$data = array();
		$data['page_title'] = 'New User';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[7]');
		$this->form_validation->set_rules('role_id', 'Role', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create()) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New User', 'User added successfully.'));
				redirect('users');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create User Failed', 'Failed create User. There was database failure.');
			}
		}

		// Role options
		$data['roles'] = $this->Role->get();

		$this->my_view('user/add', $data);
	}

	private function create()
	{
		do {
			$username = $this->input->post('username');
			$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$created_at = date('Y-m-d H:i:s');
			$updated_at = date('Y-m-d H:i:s');

			$data = compact('username', 'password', 'created_at', 'updated_at');

			$result = $this->User->insert($data);

			if ($result == FALSE) break;

			$this->log_history('user', 'create', json_encode($data));

			$result = $this->replace_user_role();

		} while (FALSE);

		return $result;
	}

	private function replace_user_role()
	{
		$username = $this->input->post('username');
		$role_id = $this->input->post('role_id');

		$user = $this->User->row(array('clauses' => array('where' => array('username' => $username))));

		if ($user == FALSE) return FALSE;

		return $this->UserRole->replace(array('user_id' => $user->id, 'role_id' => $role_id));
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('users', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'user')));

		$data = array();
		$data['page_title'] = 'Edit User';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$user = $this->User->get_by_id($id);
		if ($user == FALSE) $this->go_to('users', $this->toastr('error', 'User', sprintf('User id %s not found', $id)));

		$this->load->library('form_validation');
		if ($this->input->post('username') && ($this->input->post('username') != $user->username)) $this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'min_length[8]');
		$this->form_validation->set_rules('role_id', 'Role', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update()) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit User', 'User updated successfully.'));
				redirect('users');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update User Failed', 'Failed update user. There was database failure.');
			}
		}

		// Get user role
		$user->role = $this->UserRole->row(array('clauses' => array('where' => array('user_id' => $user->id))));
		$data['user'] = $user;

		// Role options
		$data['roles'] = $this->Role->get();

		$this->my_view('user/edit', $data);
	}

	private function update()
	{
		do {
			$id = $this->input->post('id');
			$password = ($this->input->post('password') != '') ? password_hash($this->input->post('password'), PASSWORD_DEFAULT) : '';
			$is_active = $this->input->post('is_active');
			$updated_at = date('Y-m-d H:i:s');

			$old_data = $this->User->get_by_id($id);

			if ($old_data == FALSE) {
				$result = FALSE;
				break;
			}

			$data = ($password != '') ? compact('password', 'is_active', 'updated_at') : compact('is_active', 'updated_at');
			$where = compact('id');

			$result = $this->User->update($data, $where);
			if ($result == FALSE) break;

			$this->log_history('user', 'update', json_encode($data), json_encode($old_data), json_encode($where));

			$result = $this->replace_user_role();

		} while (FALSE);

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('users', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'user')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('users', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$user = $this->User->get_by_id($id);
			if ($user == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal']=  $this->swal('error', 'Delete User Failed', sprintf('User id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->User->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete User Failed', 'Database failure');
				break;
			}

			$this->log_history('user', 'delete', '', json_encode($user), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('User <strong>%s</strong> deleted.', $user->username));

		} while (FALSE);

		$this->output_json($response);
	}
}
