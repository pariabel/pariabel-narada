<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_children extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('my_children');

		$this->load->model('student_model', 'Student');
		$this->load->model('student_parents_model', 'StudentParents');
		$this->load->model('student_session_model', 'StudentSession');
		$this->load->model('session_model', 'Session');
		$this->load->model('level_model', 'Level');
		$this->load->model('class_model', 'Class');
		$this->load->model('section_model', 'Section');
	}

	public function index()
	{
		show_404();
	}

	public function student($student_id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'my children')));

		$data = array();
		$data['page_title'] = 'My Children';

		$student = $this->Student->get_by_id($student_id);
		$student_parents = $this->StudentParents->get_parents_id_by_student_id($student_id);

		if ($student_parents->parents_id != $this->sess_user->profile->id) $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access student id: %s', $student_id)));

		// Student session
		$student_session = $this->StudentSession->get_by_student_id($student_id);

		if ($student_session !== FALSE) {
			$student->session = $this->Session->get_by_id($student_session->session_id);
			$student->level = $this->Level->get_by_id($student_session->level_id);
			$student->class = $this->Class->get_by_id($student_session->class_id);
			$student->section = $this->Section->get_by_id($student_session->section_id);
		}

		$data['student'] = $student;
		$data['sess_user'] = $this->sess_user;

		$this->my_view('my_children/student', $data);
	}
}
