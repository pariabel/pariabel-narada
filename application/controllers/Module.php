<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('module');

		$this->load->model('module_model', 'Module');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'module')));

		$data = array();
		$data['page_title'] = 'Modules';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('module/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('modules', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('id', 'name', 'controller', 'created_at', 'updated_at');
		$this->search_columns = array('name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$modules = $this->Module->get($params);

			if ($modules != FALSE) {
				foreach ($modules as $module) {
					$buttons = $this->button_edit('/module/edit/' . $module->id);
					$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/module/delete/' . $module->id, 'data-item="' . $module->name . '"');

					$row = [
						$module->id,
						$module->name,
						$module->controller,
						$module->created_at,
						$module->updated_at,
						$buttons
					];
					$data[] = $row;
				}

				$recordsFiltered = $this->Module->count_filtered($params);
				$recordsTotal = $this->Module->count_all();
			}
		}

		$response = [
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		];

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('modules', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'module')));

		$data = array();
		$data['page_title'] = 'New Module';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required|is_unique[module.name]');
		$this->form_validation->set_rules('controller', 'Controller', 'required|is_unique[module.name]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Module', 'Module added successfully.'));
				redirect('modules');
			} else {
				$data['alert'] = 'Failed create module. There was database failure.';
			}
		}

		$this->my_view('module/add', $data);
	}

	private function create()
	{
		$name = $this->input->post('name');
		$controller = $this->input->post('controller');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('name', 'controller', 'created_at', 'updated_at');

		$result =  $this->Module->insert($data);

		if ($result === TRUE) $this->log_history('module', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('modules', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'module')));

		$data = array();
		$data['page_title'] = 'Edit Module';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$module = $this->Module->get_by_id($id);
		if ($module == FALSE) $this->go_to('modules', $this->toastr('error', 'Module', sprintf('Module id %s not found', $id)));
		$data['module'] = $module;

		$this->load->library('form_validation');
		if ($this->input->post()) {
			if ($this->input->post('name') != $module->name) $this->form_validation->set_rules('name', 'Name', 'required|is_unique[module.name]');
			if ($this->input->post('controller') != $module->controller) $this->form_validation->set_rules('controller', 'Controller', 'required|is_unique[module.name]');
		}
		$this->form_validation->set_rules('is_active', 'Is Active', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Module', 'Module updated successfully.'));
				redirect('modules');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Module Failed', 'Failed update module. There was database failure.');
			}
		}

		$this->my_view('module/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$is_active = $this->input->post('is_active');
		$controller = $this->input->post('controller');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Module->get_by_id($id);

		$data = compact('name', 'controller', 'is_active', 'updated_at');
		$where = compact('id');

		$result = $this->Module->update($data, $where);

		if ($result == TRUE) $this->log_history('module', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('modules', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'module')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('modules', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {
			$module = $this->Module->get_by_id($id);
			if ($module == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Module Failed', sprintf('Module id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Module->delete($where) == FALSE) {
				$response = $error;
				break;
			}

			$this->log_history('module', 'delete', '', json_encode($module), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Module <strong>%s</strong> deleted.', $module->name));

		} while (FALSE);

		$this->output_json($response);
	}
}
