<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		if ( ! $this->session->flashdata('password_changed')) $this->check_session();

		$this->sess_user = $this->get_sess_user();
	}

	public function index()
	{
		$data = array();
		$data['page_title'] = 'Dashboard';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
                
		switch ($this->sess_user->role->name) {
			case 'Administrator':
				$data['content'] = $this->administrator();
			break;

			case 'Super User':
				$data['content'] = $this->super_user();
			break;

			case 'Administrative Staff':
			case 'Teacher':
				$this->form_change_password();
				$data['content'] = $this->staff();
			break;

			case 'Student':
				$this->form_change_password();
				$data['content'] = $this->student();
			break;

			case 'Parents':
				$data['content'] = $this->parents();
			break;

			default:
				show_404();
		}

		$this->my_view('dashboard/index', $data);
	}

	// ADMINISTRATOR
	private function administrator()
	{
		$content = '';
		return $content;
	}

	// SUPER USER
	private function super_user()
	{
		$content = $this->stat_box();
		return $content;
	}

	private function stat_box()
	{
		$data = $stat_box = array();
		$stat_box[] = $this->stat_box_data('class');
		$stat_box[] = $this->stat_box_data('staff');
		$stat_box[] = $this->stat_box_data('student');
		$stat_box[] = $this->stat_box_data('announcement');
		$data['stat_boxs'] = $stat_box;
		return $this->load->view('widgets/stat_box', $data, TRUE);
	}

	// TEACHER
	private function staff()
	{
		$content = $this->staff_profile();
		return $content;
	}

	private function staff_profile()
	{
		$data['tabs'] = $this->user_profile_tabs();
		return $this->load->view('widgets/user_profile', $data, TRUE);
	}


	// STUDENT
	private function student()
	{
		$content = $this->student_profile();
		return $content;
	}

	private function student_profile()
	{
		$data['tabs'] = $this->user_profile_tabs();
		return $this->load->view('widgets/user_profile', $data, TRUE);
	}


	// PARENTS
	private function parents()
	{
		$content = $this->my_children();
		return $content;
	}

	private function my_children()
	{
		$this->load->model('student_model', 'Student');
		$this->load->model('student_session_model', 'StudentSession');
		$this->load->model('session_model', 'Session');
		$this->load->model('level_model', 'Level');
		$this->load->model('class_model', 'Class');
		$this->load->model('section_model', 'Section');

		$data = $children = array();

		foreach ($this->sess_user->children as $student) {
			// Student session
			$student_session = $this->StudentSession->get_by_student_id($student->id);

			if ($student_session !== FALSE) {
				$student->session = $this->Session->get_by_id($student_session->session_id);
				$student->level = $this->Level->get_by_id($student_session->level_id);
				$student->class = $this->Class->get_by_id($student_session->class_id);
				$student->section = $this->Section->get_by_id($student_session->section_id);

				$children[] = $student;
			}
		}

		$data['children'] = $children;
		return $this->load->view('widgets/my_children', $data, TRUE);
	}


	// Common functions
	private function form_change_password()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|min_length[8]|callback_password_check');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|alpha_numeric|min_length[8]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->change_password()) {
				$toastr = array(
					'type' => 'success',
					'title' => 'Change Password',
					'message' => 'Password changed successfully.'
				);
				$this->session->set_flashdata('password_changed', 'success');
			} else {
				$toastr = array(
					'type' => 'error',
					'title' => 'Change Password',
					'message' => 'Change password failed.'
				);
			}
			$this->session->set_flashdata('toastr', $toastr);
			redirect('dashboard');
		}
	}

	private function user_profile_tabs()
	{
		$tabs = $data = array();
		$pages = array('profile', 'change password');
		$data['sess_user'] = $this->sess_user;

		foreach ($pages as $page) {
			switch ($page) {
				case 'profile':
					$id = 'tabProfile';
					$is_active = ($this->input->post() == NULL && $this->session->flashdata('toastr') == NULL) ? ' active' : '';
					$href = "#profile";
					$text = 'Profile';
					$content_id = 'profile';
					$content_body = $this->get_profile_body();
					break;

				case 'change password':
					$id = 'tabChangePassword';
					$is_active = ($this->input->post() != NULL OR $this->session->flashdata('toastr')) ? ' active' : '';
					$href = "#changePassword";
					$text = 'Change Password';
					$content_id = 'changePassword';
					$content_body = $this->load->view('profile/change_password', $data, TRUE);
					break;
			}
			$tabs[] = compact('id', 'is_active', 'href', 'text', 'content_id', 'content_body');
		}

		return $tabs;
	}

	private function get_profile_body()
	{
		$data['sess_user'] = $this->sess_user;

		switch ($this->sess_user->role->name)
		{
			case 'Administrator':
			case 'Super User':
				return $this->load->view('profile/admin', $data, TRUE);
			break;

			case 'Teacher':
			case 'Administrative Staff':
				return $this->load->view('profile/staff', $data, TRUE);
			break;

			case 'Student':
				$this->load->model('student_session_model', 'StudentSession');
				$this->load->model('session_model', 'Session');
				$this->load->model('level_model', 'Level');
				$this->load->model('class_model', 'Class');
				$this->load->model('section_model', 'Section');

				// Student session
				$student_session = $this->StudentSession->get_by_student_id($this->sess_user->profile->id);

				if ($student_session !== FALSE) {
					$data['session'] = $this->Session->get_by_id($student_session->session_id);
					$data['level'] = $this->Level->get_by_id($student_session->level_id);
					$data['class'] = $this->Class->get_by_id($student_session->class_id);
					$data['section'] = $this->Section->get_by_id($student_session->section_id);
				}

				return $this->load->view('profile/student', $data, TRUE);
			break;

			case 'Parents':
				return $this->load->view('profile/parents', $data, TRUE);
			break;
		}
	}

	private function change_password()
	{
		$new_password = password_hash($this->input->post('new_password'), PASSWORD_DEFAULT);

		$data = array('password' => $new_password, 'updated_at' => date('Y-m-d H:i:s'));
		$where = array('id' => $this->encryption->decrypt($this->session->userdata('sess_user_id')));
		return $this->User->update($data, $where);
	}

	public function password_check($current_password = '')
	{
		if ($current_password != '' && ! password_verify($current_password, $this->sess_user->password)) {
			$this->form_validation->set_message('password_check', 'The {field} field is not the same as session password.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	private function stat_box_data($module = '')
	{
		$data = array();
		switch ($module) {
			case 'class':
				$this->load->model('class_model', 'Class');
				$data = array(
					'bg' => 'bg-info',
					'count' => $this->Class->count_all(),
					'title' => 'Class',
					'icon' => 'fas fa-lg fa-school',
					'url' => '/classes'
				);
			break;

			case 'staff':
				$this->load->model('staff_model', 'Staff');
				$params = array(
					'is_active' => 'yes',
					'id >' => 2
				);
				$data = array(
					'bg' => 'bg-success',
					'count' => $this->Staff->count_filtered($params),
					'title' => 'Staffs',
					'icon' => 'fas fa-lg fa-user-tie',
					'url' => '/staff/list'
				);
			break;

			case 'student':
				$this->load->model('student_model', 'Student');
				$params = array(
					'is_active' => 'yes'
				);
				$data = array(
					'bg' => 'bg-warning',
					'count' => $this->Student->count_filtered($params),
					'title' => 'Students',
					'icon' => 'fas fa-lg fa-user-graduate',
					'url' => '/student/list'
				);
			break;

			case 'announcement':
				$this->load->model('announcement_model', 'Announcement');
				$params = array(
					'is_active' => 'yes'
				);
				$data = array(
					'bg' => 'bg-maroon',
					'count' => $this->Announcement->count_filtered($params),
					'title' => 'Announcement',
					'icon' => 'fas fa-lg fas fa-bullhorn',
					'url' => '/announcement'
				);
			break;
		}

		return $data;
	}
}
