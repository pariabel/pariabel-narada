<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_history extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('log_history');

		$this->load->model('log_history_model', 'LogHistory');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'log history')));

		$data = array();
		$data['page_title'] = 'Log History';

		$this->my_view('log_history/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('log_history', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('id', 'affected_table', 'operation', 'new_data', 'created_at');
		$this->search_columns = array('affected_table', 'operation');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$logs = $this->LogHistory->get($params);

			if ($logs != FALSE) {
				foreach ($logs as $log) {

					$btn_user = '<a class="btn btn-sm btn-info" href="/log_history/user/' . $log->user_id . '" data-toggle="modal" data-target="#modalUser"><i class="fas fa-sm fa-user"></i> User</a>';

					$row = array(
						$log->id,
						$log->created_at,
						$log->affected_table,
						$log->operation,
						$btn_user,
						$log->new_data,
						$log->old_data,
						$log->where_clause
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->LogHistory->count_filtered($params);
				$recordsTotal = $this->LogHistory->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function user($user_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('log_history', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();

		do {
			if (! in_array($this->sess_user->role->name, array('Administrator', 'Super User'))) {
				$response = array('code' => 403, 'message' => 'You don\'t have permission to access log history.');
				break;
			}

			// Get user account
			$user = $this->User->get_by_id($user_id);
			if ($user == FALSE) {
				$response = array('code' => 404, 'message' => 'User not found.');
				break;
			}

			$response = array(
				'code' => 200,
				'message' => 'Success',
				'data' => array(
					'role' => '',
					'name' => '',
					'username' => $user->username,
					'register_at' => date('F d, Y H:i:s', strtotime($user->created_at))
				)
			);

			// Get user role
			$this->load->model('user_role_model', 'UserRole');
			$user->role = $this->UserRole->get_role_by_user_id($user->id);
			$response['data']['role'] = $user->role->name;

			// Get user profile
			switch ($user->role->name) {
				case 'Administrator':
				case 'Super User':
				case 'Teacher':
					$this->load->model('staff_user_model', 'StaffUser');
					$staff = $this->StaffUser->get_staff_by_user_id($user_id);
					if ($staff == FALSE) {
						$response = array('code' => 404, 'message' => 'Staff not found.');
						break;
					}
					$response['data']['name'] = $staff->name;
				break;

				case 'Student':
					$this->load->model('student_user_model', 'StudentUser');
					$student = $this->StudentUser->get_student_by_user_id($user_id);
					if ($student == FALSE) {
						$response = array('code' => 404, 'message' => 'Student not found.');
						break;
					}
					$response['data']['name'] = $student->name;
				break;

				case 'Parents':
					$this->load->model('parents_user_model', 'ParentsUser');
					$parents = $this->ParentsUser->get_parents_by_user_id($user_id);
					if ($parents == FALSE) {
						$response = array('code' => 404, 'message' => 'Parents not found.');
						break;
					}
					$response['data']['name'] = $parents->father.' &amp; '.$parents->mother;
				break;
			}

		} while (FALSE);

		$this->output_json($response);
	}
}
