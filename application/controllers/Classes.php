<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('classes');

		$this->load->model('class_model', 'Class');
		$this->load->model('class_section_model', 'ClassSection');
		$this->load->model('class_teacher_model', 'ClassTeacher');
		$this->load->model('class_subject_model', 'ClassSubject');
		$this->load->model('section_model', 'Section');
		$this->load->model('level_model', 'Level');
		$this->load->model('level_class_model', 'LevelClass');
		$this->load->model('subject_model', 'Subject');
	}

	public function index($level_id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'class')));

		$data = array();

		$data['page_title'] = 'Class';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';
		$data['classes'] = $this->datatable($level_id);
		$data['levels'] = $this->Level->get();

		if ($level_id > 0) {
			$level = $this->Level->get_by_id($level_id);
			if ($level === FALSE) $this->go_to('classes', $this->toastr('error', 'Class', sprintf('Level id %s not found', $level_id)));
			$data['level'] = $level;
		}

		$this->my_view('class/index', $data);
	}

	private function datatable($level_id = 0)
	{
		$data = array();

		if ($level_id > 0) {
			$classes = $this->LevelClass->get_class_by_level_id($level_id);
		} else {
			$classes = $this->Class->get();
		}

		if ($classes != FALSE) {
			foreach ($classes as $class) {
				$buttons = $this->button_edit('/classes/edit/' . $class->id);
				$buttons .= $this->button_delete('/classes/delete/' . $class->id, 'data-item="' . $class->class . '"');
				$buttons .= '<a class="btn btn-sm btn-info mb-1" href="/classes/subjects/' . $class->id . '"><i class="fas fa-sm fa-book"></i> Subjects</a>';

				$class->sections = $this->ClassSection->get_section_by_class_id($class->id);
				$class->buttons = $buttons;
				$data[] = $class;
			}
		}

		return $data;
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('classes', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'class')));

		$data = array();
		$data['page_title'] = 'New Class';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		$this->form_validation->set_rules('class', 'Class', 'required|is_unique[class.class]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Class', 'Class added successfully.'));
				redirect('classes');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Class Failed', 'Failed create class. There was database failure.');
			}
		}

		// Level options
		$data['levels'] = $this->Level->get();

		// Section options
		$data['sections'] = $this->Section->get();

		$this->my_view('class/add', $data);
	}

	private function create()
	{
		do {
			$level_id = $this->input->post('level_id');
			$class = $this->input->post('class');
			$cb_section = $this->input->post('section');
			$created_at = date('Y-m-d H:i:s');
			$updated_at = date('Y-m-d H:i:s');

			$data = compact('class', 'created_at', 'updated_at');

			$result = $this->Class->insert($data);

			if ($result === FALSE) break;

			$class_id = $this->db->insert_id();

			$this->log_history('class', 'create', json_encode($data));

			// level class
			$data = compact('level_id', 'class_id');
			$result = $this->LevelClass->insert($data);
			if ($result === FALSE) break;

			// class section
			$sections = $this->Section->get();

			foreach ($sections as $section) {
				$data = array(
					'class_id' => $class_id,
					'section_id' => $section->id,
					'is_active' => (isset($cb_section[$section->id]) && $cb_section[$section->id] == '1') ? 'yes' : 'no',
				);

				$result = $this->ClassSection->replace($data);
				if ($result === FALSE) break;
			}

		} while (FALSE);

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('classes', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'class')));

		$data = array();
		$data['page_title'] = 'Edit Class';

		if ($this->input->post('id')) $id = $this->input->post('id');
		$class = $this->Class->get_by_id($id);
		if ($class === FALSE) $this->go_to('classes', $this->toastr('error', 'Class', sprintf('Class id %s not found', $id)));

		$this->load->library('form_validation');
		$this->form_validation->set_rules('level_id', 'Level', 'required');
		if ($this->input->post() && ($this->input->post('class') != $class->class)) $this->form_validation->set_rules('class', 'Class', 'required|is_unique[class.class]');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Class', 'Class updated successfully.'));
				redirect('classes');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Class Failed', 'Failed update class. There was database failure.');
			}
		}

		// Get level
		$class->level = $this->LevelClass->get_level_by_class_id($id);

		// Get section
		$sections = $this->ClassSection->get_section_by_class_id($id);
		foreach ($sections as $section) {
			$class->sections[$section->id] = $section->section;
		}
		$data['class'] = $class;

		// Level options
		$data['levels'] = $this->Level->get();

		// Section options
		$data['sections'] = $this->Section->get();

		$this->my_view('class/edit', $data);
	}

	private function update()
	{
		do {
			$id = $this->input->post('id');
			$level_id = $this->input->post('level_id');
			$class = $this->input->post('class');
			$cb_section = $this->input->post('section');
			$updated_at = date('Y-m-d H:i:s');

			$old_data = $this->Class->get_by_id($id);

			if ($old_data == FALSE) return FALSE;

			$data = compact('class', 'updated_at');
			$where = compact('id');

			$result = $this->Class->update($data, $where);

			if ($result === FALSE) break;

			$this->log_history('class', 'update', json_encode($data), json_encode($old_data), json_encode($where));

			// level class
			$data = compact('level_id');
			$where = array('class_id' => $id);
			$result = $this->LevelClass->update($data, $where);
			if ($result === FALSE) break;

			// class section
			$sections = $this->Section->get();

			foreach ($sections as $section) {
				$data = array(
					'class_id' => $id,
					'section_id' => $section->id,
					'is_active' => (isset($cb_section[$section->id]) && $cb_section[$section->id] == '1') ? 'yes' : 'no',
				);

				$result = $this->ClassSection->replace($data);
				if ($result === FALSE) break;
			}

		} while (FALSE);

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('classes', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'class')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('classes', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$class = $this->Class->get_by_id($id);

		$response = array();
		$success = array('code' => 200, 'swal' => $this->swal('success', 'Deleted!', sprintf('Class <strong>%s</strong> deleted.', $class->class)));

		$error = array('code' => 500, 'swal' => $this->swal('error', 'Delete Class Failed', 'Database failure'));

		do {

			if ($class == FALSE) {
				$response = array('code' => 404, 'swal' => $this->swal('error', 'Delete Class Failed', sprintf('Class id %s not found', $id)));
				break;
			}

			// ! Delete level class
			if ( ! $this->LevelClass->delete(array('class_id' => $id))) {
				$response = $error;
				break;
			}

			// ! Delete class section
			if ( ! $this->ClassSection->delete(array('class_id' => $id))) {
				$response = $error;
				break;
			}

			// ! Delete class teacher
			if ( ! $this->ClassTeacher->delete(array('class_id' => $id))) {
				$response = $error;
				break;
			}

			// ! Delete class subject
			if ( ! $this->ClassSubject->delete(array('class_id' => $id))) {
				$response = $error;
				break;
			}

			// Finally delete class
			$where = array('id' => $id);
			if ( ! $this->Class->delete($where)) {
				$response = $error;
				break;
			}

			$this->log_history('class', 'delete', '', json_encode($class), json_encode($where));

			$response = $success;

		} while (FALSE);

		$this->output_json($response);
	}

	public function subjects($class_id = 0)
	{
		if ( ! in_array($this->sess_user->role->name, array('Administrator', 'Super User'))) $this->go_to('classes', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to update %s', 'class subjects')));

		$data = array();

		if ($this->input->post('class_id')) $class_id = $this->input->post('class_id');

		$class = $this->Class->get_by_id($class_id);
		if ($class === FALSE) $this->go_to('classes', $this->toastr('error', 'Class', sprintf('Class id %s not found', $class_id)));

		if ($this->input->post()) {
			$toastr_title = sprintf('Class %s Subjects', $class->class);
			if ($this->replace_class_subjects() === TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', $toastr_title, 'Subjects updated successfully.'));
			} else {
				$this->session->set_flashdata('toastr', $this->toastr('error', $toastr_title, 'Update subjects failed.'));
			}
			redirect('classes/subjects/' . $class_id);
		}

		$data['page_title'] = sprintf('Class %s Subjects', $class->class);
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		// Get subjects
		$params['order_by'] = 'name';
		$data['subjects'] = $this->Subject->get($params);

		// Get class subjects
		$class->subjects = $this->get_class_subjects($class_id);

		$data['class'] = $class;

		$this->my_view('class/subjects', $data);
	}

	private function get_class_subjects($class_id = 0)
	{
		$class_subjects = $this->ClassSubject->get_subject_by_class_id($class_id);
		$data = array();
		if ($class_subjects !== FALSE) {
			foreach ($class_subjects as $subject) {
				$data[$subject->id]['is_active'] = 'yes';
			}
		}

		return $data;
	}

	private function replace_class_subjects()
	{
		$class_id = $this->input->post('class_id');
		$cb_subject = $this->input->post('subject[]');

		$subjects = $this->Subject->get();

		foreach ($subjects as $subject) {
			$data = array(
				'class_id' => $class_id,
				'subject_id' => $subject->id,
				'is_active' => (isset($cb_subject[$subject->id]) && $cb_subject[$subject->id] == '1') ? 'yes' : 'no'
			);

			$result = $this->ClassSubject->replace($data);
		}

		return $result;
	}
}
