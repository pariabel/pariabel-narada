<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('assignment');

		$this->load->model('assignment_model', 'Assignment');
		$this->load->model('assignment_student_model', 'AssignmentStudent');
		$this->load->model('assignment_selected_model', 'AssignmentSelected');
		$this->load->model('subject_model', 'Subject');
		$this->load->model('subject_teacher_model', 'SubjectTeacher');
		$this->load->model('staff_model', 'Staff');
		$this->load->model('uploaded_file_model', 'UploadedFile');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'assignment')));

		$data = array();
		$data['page_title'] = 'Assignments';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		switch ($this->sess_user->role->name) {
			case 'Teacher':
				$this->my_view('assignment/teacher/index', $data);
			break;

			case 'Student':
				$this->load->library('form_validation');
				if (isset($_FILES['file']['name'])) $this->form_validation->set_rules('file', 'File', 'callback_file_check');
				$this->my_view('assignment/student/index', $data);
			break;

			default:
				show_404();
		}
	}

	public function datatable($student_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		switch ($this->sess_user->role->name) {
			case 'Teacher':
				$this->datatable_teacher();
			break;

			case 'Student':
				$this->datatable_student();
			break;

			case 'Parents':
				$this->datatable_parents($student_id);
			break;

			default:
				show_404();
		}
	}

	private function datatable_teacher()
	{
		$data = array();
		$recordsFiltered = $recordsTotal = 0;

		// Set datatables order and search columns
		$this->order_columns = array('class', 'section', 'subject', 'created_date', 'submit_date');
		$this->search_columns = array('subject');

		$params = $this->datatable_params();
		$params['select'] = 'assignment.*, class.class, section.section, subject.name AS `subject`, uploaded_file.orig_name AS `file`';
		$params['join'][] = array('class', 'class.id = assignment.class_id');
		$params['join'][] = array('section', 'section.id = assignment.section_id');
		$params['join'][] = array('subject', 'subject.id = assignment.subject_id');
		$params['join'][] = array('uploaded_file', 'uploaded_file.id = assignment.file_id');
		$params['clauses']['where'] = array('assignment.teacher_id' => $this->sess_user->profile->id);
		$assignments = $this->Assignment->get($params);

		if ($assignments != FALSE) {
			foreach ($assignments as $assignment) {
				$buttons = '';

				if ($this->sess_user->modules[$this->module->id]['update'] == 'yes') {
					$buttons .= $this->button_edit('/assignment/edit/' .	$assignment->id);
				}
				if ($this->sess_user->modules[$this->module->id]['delete'] == 'yes') {
					$buttons .= '&nbsp;&nbsp;' . $this->button_delete('/assignment/delete/' .	$assignment->id, 'data-item="' .	$assignment->subject . '"');
				}
				$buttons .= '&nbsp;&nbsp;' . $this->button_download('/c/d/' . $assignment->file_id . '/' . $assignment->file, 'data-item="' . $assignment->file . '"');

				$row = array(
					$assignment->class,
					$assignment->section,
					$assignment->subject,
					date('D, M d, Y', strtotime($assignment->created_date)),
					date('D, M d, Y', strtotime($assignment->submit_date)),
					'<a href="/assignment/students_submission/' . $assignment->id . '" class="btn btn-sm btn-info mb-1" data-toggle="modal" data-target="#modalStudentsSubmission"><i class="fas fa-user-graduate"></i> Students</a>',
					$buttons
				);
				$data[] = $row;
			}

			$recordsFiltered = $this->Assignment->count_filtered($params);
			if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
			if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
			$recordsTotal = $this->Assignment->count_filtered($params);
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	private function datatable_student()
	{
		$data = array();
		$recordsFiltered = $recordsTotal = 0;
		$badge_types = $this->config->item('badge_types');

		// Set datatables order and search columns
		$this->order_columns = array('subject', 'teacher', 'assignment.created_date', 'submit_date', 'submitted');
		$this->search_columns = array('subject');

		$student_session = $this->get_student_session($this->sess_user->profile->id);

		// All
		$params = $this->datatable_params();
		$params['select'] = 'assignment.*, subject.name AS `subject`, staff.name AS `teacher`, uploaded_file.orig_name AS `file`, IFNULL(assignment_student.id, 0) AS submitted';
		$params['join'][] = array('staff', 'staff.id = assignment.teacher_id');
		$params['join'][] = array('subject', 'subject.id = assignment.subject_id');
		$params['join'][] = array('uploaded_file', 'uploaded_file.id = assignment.file_id');
		$params['join'][] = array('assignment_student', 'assignment_student.assignment_id = assignment.id AND student_id = '. $this->sess_user->profile->id, 'left');
		$params['clauses']['where'] = array(
			'assignment.class_id' => $student_session->class_id,
			'assignment.section_id' => $student_session->section_id,
			'assignment.is_selected' => 'no'
		);
		$assignments = $this->Assignment->get($params);

		if ($assignments != FALSE) {
			foreach ($assignments as $assignment) {
				$buttons = '';
				$buttons .= $this->button_download('/c/d/' . $assignment->file_id . '/' . $assignment->file, 'data-item="' . $assignment->file . '"') . '&nbsp;&nbsp;';
				$buttons .= $this->button_upload('/assignment/v/' . $assignment->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

				$submitted = ($assignment->submitted > 0) ? 'yes' : 'no';

				$row = array(
					$assignment->subject,
					$assignment->teacher,
					date('D, M d, Y', strtotime($assignment->created_date)),
					date('D, M d, Y', strtotime($assignment->submit_date)),
					$this->create_badge($badge_types[$submitted], $submitted),
					$buttons
				);
				$data[] = $row;
			}

			$recordsFiltered = $this->Assignment->count_filtered($params);
			if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
			if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
			$recordsTotal = $this->Assignment->count_filtered($params);
		}

		// Selected
		unset($params);
		$params = $this->datatable_params();
		$params['select'] = 'assignment.*, subject.name AS `subject`, staff.name AS `teacher`, uploaded_file.orig_name AS `file`, IFNULL(assignment_student.id, 0) AS submitted';
		$params['join'][] = array('staff', 'staff.id = assignment.teacher_id');
		$params['join'][] = array('subject', 'subject.id = assignment.subject_id');
		$params['join'][] = array('uploaded_file', 'uploaded_file.id = assignment.file_id');
		$params['join'][] = array('assignment_student', 'assignment_student.assignment_id = assignment.id AND student_id = '. $this->sess_user->profile->id, 'left');
		$params['join'][] = array('assignment_selected', 'assignment_selected.assignment_id = assignment.id AND assignment_selected.student_id = '. $this->sess_user->profile->id . " AND assignment_selected.is_selected = 'yes'");
		$params['clauses']['where'] = array(
			'assignment.class_id' => $student_session->class_id,
			'assignment.section_id' => $student_session->section_id,
			'assignment.is_selected' => 'yes'
		);
		$assignments = $this->Assignment->get($params);

		if ($assignments != FALSE) {
			foreach ($assignments as $assignment) {
				$buttons = '';
				$buttons .= $this->button_download('/c/d/' . $assignment->file_id . '/' . $assignment->file, 'data-item="' . $assignment->file . '"') . '&nbsp;&nbsp;';
				$buttons .= $this->button_upload('/assignment/v/' . $assignment->id, 'data-toggle="modal" data-target="#modalDetail"') . '&nbsp;&nbsp;';

				$submitted = ($assignment->submitted > 0) ? 'yes' : 'no';

				$row = array(
					$assignment->subject,
					$assignment->teacher,
					date('D, M d, Y', strtotime($assignment->created_date)),
					date('D, M d, Y', strtotime($assignment->submit_date)),
					$this->create_badge($badge_types[$submitted], $submitted),
					$buttons
				);
				$data[] = $row;
			}

			$recordsFiltered += $this->Assignment->count_filtered($params);
			if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
			if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
			$recordsTotal += $this->Assignment->count_filtered($params);
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function student($student_id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'assignment')));

		$data = array();
		$data['page_title'] = 'Assignments';
		$data['student_id'] = $student_id;

		$this->my_view('assignment/parents/student', $data);
	}

	private function datatable_parents($student_id = 0)
	{
		$data = array();
		$recordsFiltered = $recordsTotal = 0;

		// Set datatables order and search columns
		$this->order_columns = array('subject', 'teacher', 'created_date', 'submit_date');
		$this->search_columns = array('subject');

		$this->load->model('student_model', 'Student');
		$this->load->model('student_parents_model', 'StudentParents');
		$student = $this->Student->get_by_id($student_id);
		$student_parents = $this->StudentParents->get_parents_id_by_student_id($student_id);
		$student->parents_id = $student_parents->parents_id;

		if ($student->parents_id == $this->sess_user->profile->id) {
			$student_session = $this->get_student_session($student->id);

			// All
			$params = $this->datatable_params();
			$params['select'] = 'assignment.*, subject.name AS `subject`, staff.name AS `teacher`, IFNULL(assignment_student.file_id, "") AS submission_file';
			$params['join'][] = array('staff', 'staff.id = assignment.teacher_id');
			$params['join'][] = array('subject', 'subject.id = assignment.subject_id');
			$params['join'][] = array('assignment_student', 'assignment_student.assignment_id = assignment.id', 'left');
			$params['clauses']['where'] = array(
				'assignment.class_id' => $student_session->class_id,
				'assignment.section_id' => $student_session->section_id,
				'assignment.is_selected' => 'no'
			);
			$assignments = $this->Assignment->get($params);

			if ($assignments != FALSE) {
				foreach ($assignments as $assignment) {
					$buttons = '-';

					if ($assignment->submission_file != '') {
						$uploaded_file = $this->UploadedFile->get_by_id($assignment->submission_file);
						if ($uploaded_file != FALSE) $buttons = $this->button_download('/c/d/' . $uploaded_file->id . '/' . $uploaded_file->orig_name, 'data-item="' . $uploaded_file->orig_name . '"') . '&nbsp;&nbsp;';
					}

					$row = array(
						$assignment->subject,
						$assignment->teacher,
						date('D, M d, Y', strtotime($assignment->created_date)),
						date('D, M d, Y', strtotime($assignment->submit_date)),
						$buttons
					);
					$data[] = $row;
				}
			}

			$recordsFiltered = $this->Assignment->count_filtered($params);
			if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
			if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
			$recordsTotal = $this->Assignment->count_filtered($params);

			// Selected
			unset($params);
			$params = $this->datatable_params();
			$params['select'] = 'assignment.*, subject.name AS `subject`, staff.name AS `teacher`, IFNULL(assignment_student.file_id, "") AS submission_file';
			$params['join'][] = array('staff', 'staff.id = assignment.teacher_id');
			$params['join'][] = array('subject', 'subject.id = assignment.subject_id');
			$params['join'][] = array('assignment_student', 'assignment_student.assignment_id = assignment.id', 'left');
			$params['join'][] = array('assignment_selected', 'assignment_selected.assignment_id = assignment.id AND assignment_selected.student_id = ' . $student_id . " AND assignment_selected.is_selected = 'yes'");
			$params['clauses']['where'] = array(
				'assignment.class_id' => $student_session->class_id,
				'assignment.section_id' => $student_session->section_id,
				'assignment.is_selected' => 'yes'
			);
			$assignments = $this->Assignment->get($params);

			if ($assignments != FALSE) {
				foreach ($assignments as $assignment) {
					$buttons = '-';

					if ($assignment->submission_file != '') {
						$uploaded_file = $this->UploadedFile->get_by_id($assignment->submission_file);
						if ($uploaded_file != FALSE) $buttons = $this->button_download('/c/d/' . $uploaded_file->id . '/' . $uploaded_file->orig_name, 'data-item="' . $uploaded_file->orig_name . '"') . '&nbsp;&nbsp;';
					}

					$row = array(
						$assignment->subject,
						$assignment->teacher,
						date('D, M d, Y', strtotime($assignment->created_date)),
						date('D, M d, Y', strtotime($assignment->submit_date)),
						$buttons
					);
					$data[] = $row;
				}
			}

			$recordsFiltered += $this->Assignment->count_filtered($params);
			if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
			if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
			$recordsTotal += $this->Assignment->count_filtered($params);
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'assignment')));

		$data = array();
		$data['page_title'] = 'New Assignment';
		$data['alert'] = NULL;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('class_id', 'Class', 'required');
		$this->form_validation->set_rules('section_id', 'Section', 'required');
		$this->form_validation->set_rules('subject_id', 'Subject', 'required');
		$this->form_validation->set_rules('created_date', 'Created Date', 'required');
		$this->form_validation->set_rules('submit_date', 'Submit Date', 'required');
		if (isset($_FILES['file']['name'])) $this->form_validation->set_rules('file', 'File', 'callback_file_check');
		if ($this->input->post() && $this->input->post('is_selected') == 'yes') {
			$this->form_validation->set_rules('student_id[]', 'Select Students', 'required');
		}

		if ($this->form_validation->run() == TRUE) {
			$result = $this->create();
			if ($result['code'] == 'SUCCESS') {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Assignment', 'Assignment added successfully.'));
				redirect('assignment');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Assignment Failed', $result['message']);
			}
		}

		// Class options
		$data['classes'] = $this->get_class_options();

		$this->my_view('assignment/teacher/add', $data);
	}

	private function create()
	{
		$class_id = $this->input->post('class_id');
		$section_id = $this->input->post('section_id');
		$subject_id = $this->input->post('subject_id');
		$description = $this->input->post('description');
		$created_date = $this->input->post('created_date');
		$submit_date = $this->input->post('submit_date');
		$is_selected = $this->input->post('is_selected');
		$teacher_id = $this->sess_user->profile->id;
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		// Upload file
		$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');

		if ($upload_res['code'] == 'FAILED') {
			return array(
				'code' => 'FAILED',
				'message' => $upload_res['message']
			);
		}

		// Create uploaded file data
		$file_id = $this->create_uploaded_file(json_decode($upload_res['data'], TRUE));

		$data = compact('class_id', 'section_id', 'subject_id', 'description', 'created_date', 'submit_date', 'is_selected', 'file_id', 'teacher_id', 'created_at', 'updated_at');
		if ($this->Assignment->insert($data) === FALSE) {
			$error = $this->db->error();
			return array(
				'code' => 'FAILED',
				'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'There was database failure.'
			);
		}
		$assignment_id = $this->db->insert_id();

		$this->log_history('assignment', 'create', json_encode($data));

		// Assignment selected
		if ($is_selected == 'yes') $this->replace_assignment_selected($assignment_id);

		return array(
			'code' => 'SUCCESS',
			'message' => 'Created successfully'
		);
	}

	private function replace_assignment_selected($assignment_id = 0)
	{
		if ($assignment_id === 0) return FALSE;

		$sstudent_id = $this->input->post('student_id');

		$this->AssignmentSelected->update(array('is_selected' => 'no'), array('assignment_id' => $assignment_id));

		foreach ($sstudent_id as $student_id) {
			$data = array('assignment_id' => $assignment_id, 'student_id' => $student_id, 'is_selected' => 'yes');
			$this->AssignmentSelected->replace($data);
		}

		return TRUE;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'assignment')));

		$data = array();
		$data['page_title'] = 'Edit Assignment';
		$data['alert'] = NULL;

		if ($this->input->post('id')) $id = $this->input->post('id');
		$assignment = $this->Assignment->get_by_id($id);
		if ($assignment == FALSE) $this->go_to('assignments', $this->toastr('error', 'Assignment', sprintf('Assignment id %s not found', $id)));

		if ($assignment->teacher_id != $this->sess_user->profile->id) $this->go_to('assignment', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit the %s', 'assignment')));

		$this->load->library('form_validation');
		$this->form_validation->set_rules('class_id', 'Class', 'required');
		$this->form_validation->set_rules('section_id', 'Section', 'required');
		$this->form_validation->set_rules('subject_id', 'Subject', 'required');
		$this->form_validation->set_rules('created_date', 'Created Date', 'required');
		$this->form_validation->set_rules('submit_date', 'Submit Date', 'required');
		if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') $this->form_validation->set_rules('file', 'File', 'callback_file_check');
		if ($this->input->post() && $this->input->post('is_selected') == 'yes') {
			$this->form_validation->set_rules('student_id[]', 'Select Students', 'required');
		}

		if ($this->form_validation->run() == TRUE) {
			$result = $this->update();
			if ($result['code'] == 'SUCCESS') {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Assignment', 'Assignment updated successfully.'));
				redirect('assignment');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Assignment Failed', $result['message']);
			}
		}

		// Uploaded file
		$data['uploaded_file'] = $this->UploadedFile->get_by_id($assignment->file_id);

		// Class options
		$data['classes'] = $this->get_class_options();

		// Assignment selected
		if ($assignment->is_selected == 'yes') {
			$params['select'] = 'student_id';
			$params['clauses'] = array('where' => array('assignment_id' => $assignment->id, 'is_selected' => 'yes'));
			$assignment->selected_students = $this->AssignmentSelected->get($params);
		}

		$data['assignment'] = $assignment;

		$this->my_view('assignment/teacher/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$class_id = $this->input->post('class_id');
		$section_id = $this->input->post('section_id');
		$subject_id = $this->input->post('subject_id');
		$description = $this->input->post('description');
		$created_date = $this->input->post('created_date');
		$submit_date = $this->input->post('submit_date');
		$is_selected = $this->input->post('is_selected');
		$file_id = $this->input->post('file_id');
		$updated_at = date('Y-m-d H:i:s');

		if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
			$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');

			if ($upload_res['code'] == 'FAILED') {
				return array(
					'code' => 'FAILED',
					'message' => $upload_res['message']
				);
			} else {
				// Delete old uploaded file
				$uploaded_file = $this->UploadedFile->get_by_id($file_id);
				if ($uploaded_file !== FALSE) unlink($uploaded_file->full_path);

				// Update uploaded file data
				$this->update_uploaded_file($file_id, json_decode($upload_res['data'], TRUE));
			}
		}

		$old_data = $this->Assignment->get_by_id($id);
		$data = compact('class_id', 'section_id', 'subject_id', 'description', 'created_date', 'submit_date', 'updated_at');
		$where = compact('id');

		if ($this->Assignment->update($data, $where) == FALSE) {
			$error = $this->db->error();
			return array(
				'code' => 'FAILED',
				'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'There was database failure.'
			);
		}

		$this->log_history('assignment', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		// Assignment selected
		if ($is_selected == 'yes') $this->replace_assignment_selected($id);

		return array(
			'code' => 'SUCCESS',
			'message' => 'Updated successfully'
		);
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'assignment')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {
			$assignment = $this->Assignment->get_by_id($id);
			if ($assignment == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Assignment Failed', sprintf('Assignment id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Assignment->delete($where) == FALSE) {
				$response = $error;
				break;
			}

			$this->delete_uploaded_file($assignment->file_id);

			$this->log_history('assignment', 'delete', '', json_encode($assignment), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!',  sprintf('Assignment <strong>%s</strong> deleted.', $assignment->subject));

		} while (FALSE);

		$this->output_json($response);
	}

	public function file_check()
	{
		$allowed_mime_type_arr = array(
			'application/pdf',
			'application/msword',
			'application/vnd.ms-office',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/zip',
			'application/msword',
			'application/x-zip',
			'application/vnd.ms-excel',
			'application/msexcel',
			'application/x-msexcel',
			'application/x-ms-excel',
			'application/x-excel',
			'application/x-dos_ms_excel',
			'application/xls',
			'application/x-xls',
			'application/excel',
			'application/download',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		);

		return $this->uploaded_file_check($allowed_mime_type_arr, 'Please select only xls, xlsx, doc, docx, or pdf file.');
	}

	public function v($id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();

		$assignment = $this->Assignment->get_by_id($id);
		if ($assignment === FALSE) $this->go_to('assignments', $this->toastr('error', 'Assignment', sprintf('Assignment id %s not found', $id)));

		$subject = $this->Subject->get_by_id($assignment->subject_id);
		if ($subject !== FALSE) $assignment->subject = $subject->name;

		$staff = $this->Staff->get_by_id($assignment->teacher_id);
		if ($staff !== FALSE) $assignment->teacher = $staff->name;

		$uploaded_file = $this->UploadedFile->get_by_id($assignment->file_id);
		if ($uploaded_file !== FALSE) $assignment->file = $uploaded_file->orig_name;

		$assignment_student = $this->AssignmentStudent->get_by_assignment_id($id);
		if ($assignment_student !== FALSE) {
			$assignment->submission = $assignment_student;

			$student_uploaded_file = $this->UploadedFile->get_by_id($assignment_student->file_id);

			if ($student_uploaded_file !== FALSE) {
				$assignment->submission->file = $student_uploaded_file->orig_name;
			}
		}

		$this->output_json($assignment);
	}

	private function get_class_options()
	{
		return $this->SubjectTeacher->get_class($this->sess_user->profile->id);
	}

	public function get_section_options()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));
		$this->output_json($this->SubjectTeacher->get_section($this->sess_user->profile->id));
	}

	public function get_subject_options()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));
		$class_id = $this->input->post('class_id');
		$section_id = $this->input->post('section_id');
		$this->output_json($this->SubjectTeacher->get_subject($class_id, $section_id, $this->sess_user->profile->id));
	}

	public function get_students()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));
		$class_id = $this->input->post('class_id');
		$section_id = $this->input->post('section_id');
		$this->load->model('student_model', 'Student');
		$this->output_json($this->Student->get_by_class_id_section_id($class_id, $section_id));
	}

	public function submission()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		do {
			$assignment_id = $this->input->post('assignment_id');
			$now = '';

			$upload_res = $this->do_upload('file', 'pdf|doc|docx|xls|xlsx');
			$upload_data = json_decode($upload_res['data'], TRUE);

			if ($upload_res['code'] == 'FAILED') {
				$response = $upload_res;
				break;
			}

			$assignment_student = $this->AssignmentStudent->get_by_assignment_id($assignment_id);

			if ($assignment_student === FALSE) {
				// Create uploaded file data
				$file_id = $this->create_uploaded_file($upload_data);

				$now = date('Y-m-d H:i:s');
				$insert = array(
					'assignment_id' => $assignment_id,
					'student_id' => $this->sess_user->profile->id,
					'file_id' => $file_id,
					'created_at' => $now,
					'updated_at' => $now,
				);

				if ($this->AssignmentStudent->insert($insert) === FALSE) {
					$error = $this->db->error();
					$response = array(
						'code' => 'FAILED',
						'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'Upload submission file failed.'
					);
					break;
				}
			} else {
				$file_id = $assignment_student->file_id;

				// Delete old uploaded file
				$uploaded_file = $this->UploadedFile->get_by_id($file_id);
				if ($uploaded_file !== FALSE) unlink($uploaded_file->full_path);

				// Update uploaded file data
				$this->update_uploaded_file($file_id, $upload_data);

				$now = date('Y-m-d H:i:s');
				$update = array(
					'updated_at' => $now
				);
				$where = array('id' => $assignment_student->id);

				if ($this->AssignmentStudent->update($update, $where) == FALSE) {
					$error = $this->db->error();
					$response = array(
						'code' => 'FAILED',
						'message' => (ENVIRONMENT == 'development') ? $error['code'] . ':' . $error['message'] : 'Upload submission file failed.'
					);
					break;
				}
			}

			$response = array(
				'code' => 'SUCCESS',
				'message' => 'Submission file uploaded successfully',
				'data' => array(
					'submissionFile' => $upload_data['orig_name'],
					'submissionDate' => $now,
				)
			);

		} while(FALSE);

		$this->output_json($response);
	}

	public function students_submission($assignment_id = 0)
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('assignments', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsFiltered = $recordsTotal = 0;

		// Set datatables order and search columns
		$this->order_columns = array('student.nis', 'student.name', 'assignment_student.updated_at');
		$this->search_columns = array('student.name');

		$params = $this->datatable_params();
		$params['select'] = 'student.nis, student.name, assignment_student.*, uploaded_file.orig_name AS file';
		$params['join'][] = array('uploaded_file', 'uploaded_file.id = assignment_student.file_id', 'inner');
		$params['join'][] = array('student', 'student.id = assignment_student.student_id', 'inner');
		$params['clauses']['where'] = array('assignment_id' => $assignment_id);
		$assignment_students = $this->AssignmentStudent->get($params);

		if ($assignment_students != FALSE) {
			foreach ($assignment_students as $as) {
				$row = array(
					$as->nis,
					$as->name,
					date('D, M d, Y H:i:s', strtotime($as->updated_at)),
					$this->button_download('/c/d/' . $as->file_id . '/' . $as->file, 'data-item="' . $as->file . '"')
				);
				$data[] = $row;
			}

			$recordsFiltered = $this->AssignmentStudent->count_filtered($params);
			if (isset($params['clauses']['like'])) unset($params['clauses']['like']);
			if (isset($params['clauses']['or_like'])) unset($params['clauses']['or_like']);
			$recordsTotal = $this->AssignmentStudent->count_filtered($params);
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => $data
		);

		$this->output_json($response);
	}
}
