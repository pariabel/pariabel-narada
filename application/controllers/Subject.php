<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->check_session();

		$this->sess_user = $this->get_sess_user();

		$this->module = $this->get_module_by_controller('subject');

		$this->load->model('subject_model', 'Subject');
	}

	public function index()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['read']) OR $this->sess_user->modules[$this->module->id]['read'] == 'no') $this->go_to('dashboard', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to access %s', 'subject')));

		$data = array();
		$data['page_title'] = 'Subjects';
		$data['module'] = $this->module;
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->my_view('subject/index', $data);
	}

	public function datatable()
	{
		if ($this->input->is_ajax_request() === FALSE) $this->go_to('subjects', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$data = array();
		$recordsTotal = $recordsFiltered = 0;

		// Set datatables order and search columns
		$this->order_columns = array('id', 'name', 'created_at', 'updated_at');
		$this->search_columns = array('name');

		$params = $this->datatable_params();

		if ( isset($this->sess_user->modules[$this->module->id]['read']) && $this->sess_user->modules[$this->module->id]['read'] == 'yes') {
			$subjects = $this->Subject->get($params);

			if ($subjects != FALSE) {
				foreach ($subjects as $subject) {
					$buttons = $this->button_edit('/subject/edit/' . $subject->id);
					$buttons .= $this->button_delete('/subject/delete/' . $subject->id, 'data-item="' . $subject->name . '"');

					$row = array(
						$subject->id,
						$subject->name,
						$subject->created_at,
						$subject->updated_at,
						$buttons
					);
					$data[] = $row;
				}

				$recordsFiltered = $this->Subject->count_filtered($params);
				$recordsTotal = $this->Subject->count_all();
			}
		}

		$response = array(
			'draw' => $params['draw'],
			'recordsFiltered' => $recordsFiltered,
			'recordsTotal' => $recordsTotal,
			'data' => $data
		);

		$this->output_json($response);
	}

	public function add()
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['create']) OR $this->sess_user->modules[$this->module->id]['create'] == 'no') $this->go_to('subjects', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to add %s', 'subject')));

		$data = array();
		$data['page_title'] = 'New Subject';
		$data['errors'] = '';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->create() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'New Subject', 'Subject added successfully.'));
				redirect('subjects');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Create Subject Failed', 'Failed create subject. There was database failure.');
			}
		}

		$this->my_view('subject/add', $data);
	}

	private function create()
	{
		$name = $this->input->post('name');
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data = compact('name', 'created_at', 'updated_at');

		$result = $this->Subject->insert($data);

		if ($result === TRUE) $this->log_history('subject', 'create', json_encode($data));

		return $result;
	}

	public function edit($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['update']) OR $this->sess_user->modules[$this->module->id]['update'] == 'no') $this->go_to('subjects', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to edit %s', 'subject')));

		$data = array();
		$data['page_title'] = 'Edit Subject';

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->update() == TRUE) {
				$this->session->set_flashdata('toastr', $this->toastr('success', 'Edit Subject', 'Subject updated successfully.'));
				redirect('subjects');
			} else {
				$data['alert'] = $this->alert('alert-danger', 'Update Subject Failed', 'Failed update subject. There was database failure.');
			}
		}

		if ($this->input->post('id')) $id = $this->input->post('id');
		$subject = $this->Subject->get_by_id($id);
		if ($subject == FALSE) $this->go_to('subjects', $this->toastr('error', 'Subject', sprintf('Subject id %s not found', $id)));
		$data['subject'] = $subject;

		$this->my_view('subject/edit', $data);
	}

	private function update()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$updated_at = date('Y-m-d H:i:s');

		$old_data = $this->Subject->get_by_id($id);

		if ($old_data == FALSE) return FALSE;

		$data = compact('name', 'updated_at');
		$where = compact('id');

		$result = $this->Subject->update($data, $where);

		if ($result == TRUE) $this->log_history('subject', 'update', json_encode($data), json_encode($old_data), json_encode($where));

		return $result;
	}

	public function delete($id = 0)
	{
		if ( ! isset($this->sess_user->modules[$this->module->id]['delete']) OR $this->sess_user->modules[$this->module->id]['delete'] == 'no') $this->go_to('subjects', $this->toastr('error', 'Forbidden', sprintf('You don\'t have permission to delete %s', 'subject')));

		if ($this->input->is_ajax_request() === FALSE) $this->go_to('subjects', $this->toastr('error', 'Forbidden', 'No direct access allowed'));

		$response = array();
		$success = array('code' => 200);
		$error = array('code' => 500);

		do {

			$subject = $this->Subject->get_by_id($id);
			if ($subject == FALSE) {
				$response = $error;
				$response['code'] = 404;
				$response['swal'] = $this->swal('error', 'Delete Subject Failed', sprintf('Subject id %s not found', $id));
				break;
			}

			$where = array('id' => $id);
			if ($this->Subject->delete($where) == FALSE) {
				$response = $error;
				$response['swal'] = $this->swal('error', 'Delete Subject Failed', 'Database failure');
				break;
			}

			$this->log_history('subject', 'delete', '', json_encode($subject), json_encode($where));

			$response = $success;
			$response['swal'] = $this->swal('success', 'Deleted!', sprintf('Subject <strong>%s</strong> deleted.', $subject->name));

		} while (FALSE);

		$this->output_json($response);
	}
}
