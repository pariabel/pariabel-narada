<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($role = '')
	{
		if ($this->session->userdata('sess_user_id')) redirect('dashboard');

		$data = array();
		$data['page_title'] = 'Log In';
		$data['errors'] = '';
		$data['toastr'] = ($this->session->flashdata('toastr')) ? $this->session->flashdata('toastr') : '';

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '<br>');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE) {
			if ($this->login() == TRUE) {
				$data['toastr'] = $this->toastr('success', 'Logged in successfully', '<i class="fas fa-spinner fa-spin"></i> Redirecting...');
			} else {
				$data['errors'] = 'Your username and password do not match.';
			}
		} else {
			$data['errors'] = validation_errors();
		}

		// Login box message
		$msg['staffs'] = 'Staff Log In';
		$msg['teachers'] = 'Teacher Log In';
		$msg['students'] = 'Student Log In';
		$msg['parents'] = 'Parents Log In';
		$data['msg'] = ($role != '' && isset($msg[$role])) ? $msg[$role] : '';

		// Redirect
		$data['redirect'] = ($this->session->flashdata('redirect')) ? site_url($this->session->flashdata('redirect')) : (($this->input->post('redirect')) ? $this->input->post('redirect') : site_url('dashboard'));

		$this->load->view('authentication/login', $data);
	}

	private function login()
	{
		$username = xss_clean($this->input->post('username'));
		$password = xss_clean($this->input->post('password'));

		$response = FALSE;

		$user = $this->User->get_by_username($username);

		do {
			if ($user == FALSE) {
				$response = FALSE;
				break;
			}

			if ( ! password_verify($password, $user->password)) {
				$response = FALSE;
				break;
			}

			$this->session->set_userdata(array(
				'sess_user_id' => $this->encryption->encrypt($user->id),
				'sess_username' => $this->encryption->encrypt($user->username),
				'sess_password' => $this->encryption->encrypt($password)
			));

			$response = TRUE;

		} while (FALSE);

		return $response;
	}

	public function logout()
	{
		// $this->session->sess_destroy();
		$this->session->unset_userdata(array(
			'sess_user_id',
			'sess_username',
			'sess_password',
			'sess_user',
			'sess_navigations',
			'students_class_id',
			'students_section_id',
			'class_id',
			'section_id',
			'questionnaire'
		));
		$this->session->set_flashdata('toastr', $this->toastr('success', 'Logged out successfully', ''));

		redirect('login');
	}
}
