<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_subject_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('teacher_subject');
	}

	public function get_class($teacher_id = 0)
	{
		// $sql = 'SELECT class.* FROM class WHERE id IN(SELECT DISTINCT class_id FROM teacher_subject WHERE teacher_id = ?)';
		$sql = "SELECT class.*
		FROM class
		WHERE id IN(
			SELECT class_id
			FROM class_section
			WHERE id IN(
				SELECT class_section_id FROM teacher_subject WHERE teacher_id = ?
			)
		)";
		$query = $this->db->query($sql, array($teacher_id));

		return $query->result();
	}

	public function get_section($teacher_id = 0)
	{
		$sql = "SELECT section.*
		FROM section
		WHERE id IN(
			SELECT section_id
			FROM class_section
			WHERE id IN(
				SELECT class_section_id FROM teacher_subject WHERE teacher_id = ?
			) AND is_active = 'yes'
		)";

		$query = $this->db->query($sql, array($teacher_id));

		return $query->result();
	}

	public function get_subject($teacher_id = 0)
	{
		$sql = "SELECT `subject`.*
		FROM `subject`
		WHERE id IN(
			SELECT subject_id
			FROM class_subject
			WHERE subject_id IN(
				SELECT subject_id FROM teacher_subject WHERE teacher_id = ?
			) AND is_active = 'yes'
		)";

		$query = $this->db->query($sql, array($teacher_id));

		return $query->result();
	}
}
