<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Class_model extends MY_Model {

    public function __construct()
    {
            parent::__construct();

            $this->from('class');
    }

    public function get_class_id_by_class($class = '')
    {
            $sql = "SELECT id FROM class WHERE class = ?";
            $query = $this->db->query($sql, array($class));

            $class = $query->row();

            if (isset($class)) return $class->id;
    }
}
