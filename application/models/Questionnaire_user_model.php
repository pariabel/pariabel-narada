<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('questionnaire_user');
	}

	public function has_responded($questionnaire_id = 0, $user_id = 0)
	{
		$sql = 'SELECT * FROM questionnaire_user WHERE questionnaire_id = ? AND `user_id` = ?';
		$query = $this->db->query($sql, array($questionnaire_id, $user_id));

		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
