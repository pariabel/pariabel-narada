<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Class_section_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('class_section');
	}

	public function get_section_by_class_id($class_id = 0)
	{
		$sql = "SELECT section.*
		FROM section
		WHERE id IN(
			SELECT section_id
			FROM class_section
			WHERE class_id = ?
			AND is_active = ?
		)";
		$query = $this->db->query($sql, array($class_id, 'yes'));

		return $query->result();
	}
}
