<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('announcement_user');
	}

	public function get_by_user_id($user_id = 0)
	{
		$params = array('clauses' => array('where' => array('user_id' => $user_id)));
		return $this->row($params);
	}
}
