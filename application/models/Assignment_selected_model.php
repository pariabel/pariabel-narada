<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment_selected_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('assignment_selected');
	}
}
