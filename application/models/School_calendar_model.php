<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_calendar_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('school_calendar');
	}
}
