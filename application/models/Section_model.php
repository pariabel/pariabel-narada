<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('section');
	}

	public function get_section_id_by_section($section = '')
	{
		$sql = "SELECT id FROM section WHERE section = ?";
		$query = $this->db->query($sql, array($section));

		$section = $query->row();

		if (isset($section)) return $section->id;
	}
}
