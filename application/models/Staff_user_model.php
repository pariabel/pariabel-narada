<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('staff_user');
	}

	public function get_user_by_staff_id($staff_id = 0)
	{
		$sql = "SELECT `user`.* FROM `user`
		INNER JOIN `staff_user` ON `staff_user`.`user_id` = `user`.id AND staff_user.staff_id = ?";
		$query = $this->db->query($sql, array($staff_id));

		return $query->row();
	}

	public function get_staff_by_user_id($user_id = 0)
	{
		$sql = "SELECT `staff`.* FROM `staff`
		INNER JOIN `staff_user` ON `staff_user`.`staff_id` = `staff`.id AND staff_user.user_id = ?";
		$query = $this->db->query($sql, array($user_id));

		return $query->row();
	}
}
