<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment_student_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('assignment_student');
	}

	public function get_by_assignment_id($assignment_id = 0)
	{
		$params = array('clauses' => array('where' => array('assignment_id' => $assignment_id)));
		return $this->row($params);
	}

	public function count_havenot_submitted_yet($student_id = 0, $class_id = 0, $section_id = 0)
	{
		$sql = "SELECT assignment.id
		FROM assignment
		WHERE assignment.id NOT IN (
			SELECT assignment_student.assignment_id
			FROM assignment_student
			WHERE student_id = ?
		)
		AND assignment.class_id = ?
		AND assignment.section_id = ?";
		$query = $this->db->query($sql, array($student_id, $class_id, $section_id));

		return $query->num_rows();
	}
}
