<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('student_user');
	}

	public function get_user_by_student_id($student_id = 0)
	{
		$sql = "SELECT `user`.* FROM `user`
		INNER JOIN `student_user` ON `student_user`.`user_id` = `user`.id AND student_user.student_id = ?";
		$query = $this->db->query($sql, array($student_id));

		return $query->row();
	}

	public function get_student_by_user_id($user_id = 0)
	{
		$sql = "SELECT `student`.* FROM `student`
		INNER JOIN `student_user` ON `student_user`.`student_id` = `student`.id AND student_user.user_id = ?";
		$query = $this->db->query($sql, array($user_id));

		return $query->row();
	}
}
