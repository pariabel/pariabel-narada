<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('staff');
	}

	public function get_teachers()
	{
		$sql = "SELECT `staff`.*
		FROM `staff`
		WHERE id IN(
		SELECT staff_id
		FROM `staff_role`
		INNER JOIN `role` ON `role`.id = `staff_role`.role_id
		WHERE `role`.`name` = ?)";

		$query = $this->db->query($sql, array('Teacher'));

		return $query->result();
	}
}
