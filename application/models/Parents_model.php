<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('parents');
	}

	public function count_by_name($name = '')
	{
		$params = array(
			'clauses' => array(
				'like' => array('father' => $name),
				'or_like' => array('mother' => $name)
			)
		);
		return $this->count_filtered($params);
	}

	public function count_by_email($email = '')
	{
		$params = array(
			'clauses' => array(
				'like' => array('father_email' => $email),
				'or_like' => array('mother_email' => $email)
			)
		);
		return $this->count_filtered($params);
	}
}
