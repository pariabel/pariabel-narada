<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('module');
	}

	public function get_by_controller($controller = '')
	{
		$params = array(
			'clauses' => array('where' => array('controller' => $controller))
		);
		$module = $this->row($params);

		return $module;
	}
}
