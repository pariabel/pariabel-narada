<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('parents_user');
	}

	public function get_user_by_parents_id($parents_id = 0)
	{
		$sql = "SELECT `user`.* FROM `user`
		INNER JOIN `parents_user` ON `parents_user`.`user_id` = `user`.id AND parents_user.parents_id = ?";
		$query = $this->db->query($sql, array($parents_id));

		return $query->row();
	}

	public function get_parents_by_user_id($user_id = 0)
	{
		$sql = "SELECT `parents`.* FROM `parents`
		INNER JOIN `parents_user` ON `parents_user`.`parents_id` = `parents`.id AND parents_user.user_id = ?";
		$query = $this->db->query($sql, array($user_id));

		return $query->row();
	}
}
