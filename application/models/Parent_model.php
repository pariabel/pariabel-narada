<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('parent');
	}

	public function get_by_user_id($user_id = 0)
	{
		$params = array(
			'select' => 'parent.*',
			'join' => array(0 => array('parent_user', 'parent_user.parent_id = parent.id')),
			'clauses' => array('where' => array('parent_user.user_id' => $user_id))
		);
		$parent = $this->row($params);

		return $parent;
	}
}
