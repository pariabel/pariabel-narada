<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homework_quiz_student_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('homework_quiz_student');
	}

	public function get_by_homework_quiz_id($homework_quiz_id = 0)
	{
		$params = array('clauses' => array('where' => array('homework_quiz_id' => $homework_quiz_id)));
		return $this->row($params);
	}
}
