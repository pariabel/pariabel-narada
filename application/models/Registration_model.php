<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrations_model extends MY_Model {
    private $class;
    public function __construct()
    {
        parent::__construct();

        $this->from('registrations');
        $this->class= $this->load->model('Class_model');
    }
    
}
