<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject_teacher_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('subject_teacher');
	}

	public function get_class($teacher_id = 0)
	{
		// $sql = 'SELECT class.* FROM class WHERE id IN (SELECT DISTINCT class_id FROM subject_teacher WHERE teacher_id = ?)';
		/*$sql = "SELECT class.*
		FROM class
		WHERE id IN (
			SELECT class_id
			FROM subject_teacher WHERE teacher_id = ?
		)";*/
		$sql = "SELECT class.*
		FROM class
		WHERE id IN (
			SELECT a.class_id
			FROM subject_teacher a, class_subject b
			WHERE a.class_id = b.class_id
			AND a.subject_id = b.subject_id
			AND b.is_active = 'yes'
			AND a.teacher_id = ?
		)";
		$query = $this->db->query($sql, array($teacher_id));

		return $query->result();
	}

	public function get_section($teacher_id = 0)
	{
		$sql = "SELECT section.*
		FROM section
		WHERE id IN (
			SELECT section_id
			FROM class_section
			WHERE id IN (
				SELECT section_id FROM subject_teacher WHERE teacher_id = ?
			) AND is_active = 'yes'
		)";

		$query = $this->db->query($sql, array($teacher_id));

		return $query->result();
	}

	public function get_subject($class_id = 0, $section_id = 0, $teacher_id = 0)
	{
		$sql = "SELECT `subject`.*
		FROM `subject`
		WHERE id IN (
			SELECT subject_id
			FROM class_subject
			WHERE subject_id IN (
				SELECT subject_id FROM subject_teacher WHERE class_id = ? AND section_id = ? AND teacher_id = ?
			) AND is_active = 'yes'
		)";

		$query = $this->db->query($sql, array($class_id, $section_id, $teacher_id));

		return $query->result();
	}

	public function get_by_class_section($class_id = 0, $section_id = 0)
	{
		$sql = "SELECT subject_teacher.*
		FROM subject_teacher
		WHERE class_id = ? AND section_id = ?";
		$query = $this->db->query($sql, array($class_id, $section_id));

		return $query->result();
	}
}
