<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('questionnaire_q');
	}
}
