<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_module_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('role_module');
	}

	public function get_by_role_id($role_id = 0)
	{
		$params = array(
			'clauses' => array('where' => array('role_id' => $role_id))
		);
		$role_module = $this->get($params);

		return $role_module;
	}
}
