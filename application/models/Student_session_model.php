<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_session_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('student_session');
	}

	public function get_by_student_id($student_id = 0)
	{
		$params = array('clauses' => array('where' => array('student_id' => $student_id)));
		return $this->row($params);
	}
}
