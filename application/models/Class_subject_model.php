<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Class_subject_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('class_subject');
	}

	public function get_subject_by_class_id($class_id = 0)
	{
		$sql = "SELECT `subject`.*
		FROM `subject`
		WHERE id IN(
			SELECT subject_id
			FROM class_subject
			WHERE class_id = ?
			AND is_active = ?
		)";
		$query = $this->db->query($sql, array($class_id, 'yes'));

		return $query->result();
	}
}
