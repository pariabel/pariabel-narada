<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_role_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('user_role');
	}

	public function get_role_by_user_id($user_id = 0)
	{
		$sql = "SELECT `role`.* FROM `role`
		INNER JOIN `user_role` ON `user_role`.`role_id` = `role`.id AND user_role.user_id = ?";
		$query = $this->db->query($sql, array($user_id));

		return $query->row();
	}
}
