<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploaded_file_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('uploaded_file');
	}
}
