<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('student');
	}

	public function get_by_parents_id($parents_id = 0)
	{
		$params = array(
			'select' => 'student.*',
			'join' => array(array('student_parents', 'student_parents.student_id = student.id AND student_parents.parents_id = ' . $parents_id))
		);
		return $this->get($params);
	}

	public function get_by_class_id_section_id($class_id = 0, $section_id = 0)
	{
		$params = array(
			'select' => 'student.id, student.nis, student.name',
			'join' => array(array('student_session', 'student_session.student_id = student.id AND student_session.class_id = ' . $class_id . ' AND student_session.section_id = ' . $section_id)),
			'clauses' => array('where' => array('student.is_active' => 'yes'))
		);
		return $this->get($params);
	}
}
