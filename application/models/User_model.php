<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('user');
	}

	public function get_by_username($username = '')
	{
		return $this->User->row(array(
			'clauses' => array(
				'where' => array('username' => $username)
			)
		));
	}
}
