<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qqoption_user_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('qq_option_user');
	}

	public function count_by_option_id($option_id = 0)
	{
		$sql = "SELECT COUNT(user_id) cnt FROM qq_option_user WHERE option_id = ?";
		$query = $this->db->query($sql, array($option_id));
		$row = $query->row();

		if ( ! isset($row)) return 0;
		return $row->cnt;
	}
}
