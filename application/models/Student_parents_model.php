<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_parents_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('student_parents');
	}

	public function get_parents_id_by_student_id($student_id = 0)
	{
		$params = array('clauses' => array('where' => array('student_id' => $student_id)));
		return $this->row($params);
	}

	public function get_parents_by_student_id($student_id = 0)
	{
		$sql = "SELECT `parents`.* FROM `parents`
		INNER JOIN `student_parents` ON `student_parents`.`parents_id` = `parents`.id AND student_parents.student_id = ?";
		$query = $this->db->query($sql, array($student_id));

		return $query->row();
	}
}
