<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('session');
	}

	public function get_active_session()
	{
		$params = array('clauses' => array('where' => array('is_active' => 'yes')));
		return $this->row($params);
	}
}
