<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module_navigation_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('module_navigation');
	}

	public function count_childs($parent_id = 0)
	{
		$sql = "SELECT COUNT(*) AS `childs_count` FROM module_navigation WHERE parent_id = ?";
		$query = $this->db->query($sql, array($parent_id));
		$row = $query->row();

		if (isset($row)) return $row->childs_count;
		else return 0;
	}
}
