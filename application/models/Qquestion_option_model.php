<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qquestion_option_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('qq_option');
	}

	public function get_by_question_id($qquestion_id = 0)
	{
		$params = array('clauses' => array('where' => array('qquestion_id' => $qquestion_id)));
		return $this->get($params);
	}
}
