<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_role_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('staff_role');
	}

	public function get_role_by_staff_id($staff_id = 0)
	{
		$sql = "SELECT `role`.* FROM `role`
		INNER JOIN `staff_role` ON `staff_role`.`role_id` = `role`.id AND staff_role.staff_id = ?";
		$query = $this->db->query($sql, array($staff_id));

		return $query->row();
	}
}
