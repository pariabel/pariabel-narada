<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level_class_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();

		$this->from('level_class');
	}

	public function get_class_by_level_id($level_id = 0)
	{
		$sql = "SELECT class.*
		FROM class
		WHERE id IN(
			SELECT class_id
			FROM level_class
			WHERE level_id = ?
		)";
		$query = $this->db->query($sql, array($level_id));

		return $query->result();
	}

	public function get_level_by_class_id($class_id = 0)
	{
		$sql = "SELECT `level`.*
		FROM `level_class`
		INNER JOIN `level` on `level`.id = `level_class`.level_id
		WHERE class_id = ? ";
		$query = $this->db->query($sql, array($class_id));

		return $query->row();
	}
}
