<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $search_columns;
	public $order_columns;

	public function __construct()
	{
		parent::__construct();
	}

	protected function my_view($view_file = '', $data = array())
	{
		if ( ! file_exists(VIEWPATH . $view_file . '.php')) show_404();

		if (file_exists(VIEWPATH . $view_file . '_css.php')) {
			$data['page_styles'] = $this->load->view($view_file . '_css', $data, TRUE);
		}

		if (file_exists(VIEWPATH . $view_file . '_js.php')) {
			$data['page_scripts'] = $this->load->view($view_file . '_js', $data, TRUE);
		}

		// Navbar
		$data['navbar'] = $this->load->view('layouts/navbar', $data, TRUE);

		// Sidebar
		$data['sidebar'] = $this->load->view('layouts/sidebar', $data, TRUE);

		// Content
		$data['content'] = $this->load->view($view_file, $data, TRUE);

		// Footer
		$data['footer'] = $this->load->view('layouts/footer', $data, TRUE);

		// Js scripts
		$data['js_scripts'] = $this->load->view('layouts/js_scripts', $data, TRUE);

		$this->load->view('layouts/master', $data);
	}

	protected function datatable_params()
	{
		$params = array();

		// Datatables requests
		$draw = xss_clean($this->input->post('draw'));
		$search = xss_clean($this->input->post('search'));
		$order = xss_clean($this->input->post('order'));
		$offset = xss_clean($this->input->post('start'));
		$limit = xss_clean($this->input->post('length'));

		$i = 0;
		foreach ((isset($this->search_columns)) ? $this->search_columns : array() as $col) {
			if ( ! empty($search['value'])) {
				if ($i == 0) {
					$params['clauses']['like'] = array($col => $search['value']);
				} else {
					$params['clauses']['or_like'] = array($col => $search['value']);
				}
			}
			$i++;
		}

		if ( ! empty($order) ) $params['order_by'] = sprintf('%s %s', $this->order_columns[$order['0']['column']], $order['0']['dir']);

		$params['limit'] = $limit;
		$params['offset'] = $offset;
		$params['draw'] = $draw;

		return $params;
	}

	protected function response_success($message = '', $data = array())
	{
		$response = array(
			'code' => 200,
			'message' => $message,
		);

		if (count($data) > 0) $response['data'] = $data;

		return $response;
	}

	protected function response_error($message = '', $code = 500)
	{
		$response = array(
			'code' => $code,
			'message' => $message
		);

		return $response;
	}

	protected function dd($output)
	{
		if (is_array($output) || is_object($output)) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($output));
		} else {
			$this->output->set_output($output);
		}

		$this->output->_display();
		exit;
	}

	protected function output_json($output)
	{
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($output))
		->_display();
		exit;
	}
}


Class Backend_Controller extends MY_Controller {

	protected $sess_user;
	protected $module;

	public function __construct()
	{
		parent::__construct();

		$this->config->load('backend');

		$this->load->model('user_model', 'User');
	}

	protected function check_session()
	{
		$this->session->set_flashdata('redirect', uri_string());

		$username = $this->session->userdata('sess_username');
		$password = $this->session->userdata('sess_password');

		$user = $this->User->get_by_username($this->encryption->decrypt($username));

		do {
			if ($user == FALSE) {
				redirect('login');
				break;
			}

			if ( ! password_verify($this->encryption->decrypt($password), $user->password)) {
				redirect('login');
				break;
			}

		} while (FALSE);

		return TRUE;
	}

	protected function my_view($view_file = '', $data = array())
	{
		$this->sess_user = $this->get_sess_user();
		$data['sess_user'] = $this->sess_user;
		// $this->dd($data['sess_user']);

		$data['sidebar_menu'] = ($this->sess_user->role->name != 'Parents') ? $this->get_sidebar_menu($this->sess_user) : '';

		parent::my_view($view_file, $data);
	}

	protected function get_sess_user()
	{
		if ( ! $this->session->userdata('sess_user_id')) redirect('login');

		if ( ! $this->session->userdata('sess_user')) {
			$user = $this->User->get_by_id($this->encryption->decrypt($this->session->userdata('sess_user_id')));

			if ($user == FALSE) redirect('login');

			// User role
			$this->load->model('user_role_model', 'UserRole');
			$user->role = $this->UserRole->get_role_by_user_id($user->id);

			// User modules
			$user->modules = $this->get_user_module($user->role);
			// $this->dd($user);

			// User profile
			$user->profile = $this->get_user_profile($user);

			if ($user->role->name == 'Parents') {
				$user->profile->name = $user->username;
				$user->children = $this->get_parents_children($user);
			} elseif ($user->role->name == 'Student') {
				$user->parents = $this->get_student_parents($user);
				$user->student_session = $this->get_student_session($user->profile->id);
			}

			// $user->notifications = $this->get_user_notification($user);
			// $this->dd($user);

			$this->session->set_userdata('sess_user', $user);
		}

		return $this->session->userdata('sess_user');
	}

	protected function get_user_module($role = stdClass)
	{
		$this->load->model('role_module_model', 'RoleModule');

		$modules = array();

		$role_modules = $this->RoleModule->get_by_role_id($role->id);
		foreach ($role_modules as $rm) {
			$modules[$rm->module_id]['create'] = $rm->create;
			$modules[$rm->module_id]['read'] = $rm->read;
			$modules[$rm->module_id]['update'] = $rm->update;
			$modules[$rm->module_id]['delete'] = $rm->delete;
		}
		// $this->dd($modules);

		return $modules;
	}

	protected function get_sidebar_menu($user = stdClass)
	{
		$menus = array();
		$navigations = array();

		if ( ! $this->session->userdata('sess_navigations')) {
			$this->load->model('Module_navigation_model', 'Navigation');
			$this->load->model('Role_module_model', 'RoleModule');

			$params = array(
				'select' => 'module_navigation.*, module.controller, module.name AS module_name',
				'join' => array(0 => array('module', 'module.id = module_navigation.module_id', 'left')),
				'order_by' => 'parent_id, seq_num'
			);
			$navigations = $this->Navigation->get($params);
			$this->session->set_userdata('sess_navigations', $navigations);
		} else {
			$navigations = $this->session->userdata('sess_navigations');
		}
		// $this->dd($navigations);

		$current_controller = $this->uri->segment(1);

		foreach ($navigations as $nav) {
			if ( ! isset($user->modules[$nav->module_id][$nav->action])) continue;

			if ( $user->modules[$nav->module_id][$nav->action] != 'yes') continue;

			if ($nav->parent_id > 0) {
				$menus[$nav->parent_id]['has_treeview'] = TRUE;
				$menus[$nav->parent_id]['uri'] = 'javascript:;';
				$menus[$nav->parent_id]['childs'][$nav->id] = array(
					'name' => $nav->name,
					'module_id' => $nav->module_id,
					'uri' => '/'. $nav->uri,
					'active' => (stripos(uri_string(), $nav->controller) !== FALSE && preg_match("/^{$nav->controller}/i", $current_controller)) ? ' active' : '',
					'icon'=> 'far fa-circle',
				);

				if (isset($menus[$nav->parent_id]['module_id']) && $menus[$nav->parent_id]['module_id'] == $menus[$nav->parent_id]['childs'][$nav->id]['module_id']) {
					$menus[$nav->parent_id]['childs'][$nav->id]['active'] = (uri_string() == $nav->uri) ? ' active' : '';
				}

				if (stripos(uri_string(), $nav->controller) !== FALSE && preg_match("/^{$nav->controller}/i", $current_controller)) {
					if ($menus[$nav->parent_id]['menu_open'] == FALSE) {
						$menus[$nav->parent_id]['menu_open'] = TRUE;
						$menus[$nav->parent_id]['active'] = ' active';
					}
				}
			} else {
				$uri_str = (uri_string() == 'home') ? 'dashboard' : uri_string();
				$menus[$nav->id] = array(
					'name' => $nav->name,
					'module_id' => $nav->module_id,
					'uri' => '/' . $nav->uri,
					'icon' => (isset($nav->icon) && ! empty($nav->icon)) ? $nav->icon : 'far fa-circle',
					'childs' => FALSE,
					'has_treeview' => FALSE,
					'menu_open' => FALSE,
					'active' => (stripos($uri_str, $nav->controller) !== FALSE) ? ' active' : '',
					// 'notification' => $user->notifications[$nav->module_name]
					'notification' => ''
				);
			}
		}
		// $this->dd($menus);

		return $menus;
	}

	protected function create_uploaded_file($upload_data = array())
	{
		$this->load->model('uploaded_file_model', 'UploadedFile');

		$data = array(
			'file_name' => $upload_data['file_name'],
			'file_type' => $upload_data['file_type'],
			'file_path' => $upload_data['file_path'],
			'full_path' => $upload_data['full_path'],
			'orig_name' => $upload_data['orig_name'],
			'file_ext' => $upload_data['file_ext'],
			'file_size' => $upload_data['file_size'],
			'image_width' => $upload_data['image_width'],
			'image_height' => $upload_data['image_height'],
			'image_size_str' => $upload_data['image_size_str'],
			'uploaded_at' => date('Y-m-d H:i:s'),
		);

		if ($this->UploadedFile->insert($data)) return $this->db->insert_id();
		else return 0;
	}

	protected function update_uploaded_file($id = 0, $upload_data = array())
	{
		$this->load->model('uploaded_file_model', 'UploadedFile');

		$data = array(
			'file_name' => $upload_data['file_name'],
			'file_type' => $upload_data['file_type'],
			'file_path' => $upload_data['file_path'],
			'full_path' => $upload_data['full_path'],
			'orig_name' => $upload_data['orig_name'],
			'file_ext' => $upload_data['file_ext'],
			'file_size' => $upload_data['file_size'],
			'image_width' => $upload_data['image_width'],
			'image_height' => $upload_data['image_height'],
			'image_size_str' => $upload_data['image_size_str'],
			'uploaded_at' => date('Y-m-d H:i:s'),
		);

		return $this->UploadedFile->update($data, compact('id'));
	}

	protected function delete_uploaded_file($file_id = 0)
	{
		$this->load->model('uploaded_file_model', 'UploadedFile');

		$uploaded_file = $this->UploadedFile->get_by_id($file_id);

		if ($uploaded_file == FALSE) show_error(sprintf('Uploaded file id %s not found', $file_id), 404);

		if ( ! unlink($uploaded_file->full_path)) show_error(sprintf('Delete uploaded file id %s failed', $file_id), 500);

		return $this->UploadedFile->delete(array('id' => $file_id));
	}

	protected function do_upload($field = 'userfile', $allowed_types = 'gif|jpg|png', $max_size = 2048)
	{
		$upload_path = $this->config->item('upload_path');

		if ( ! is_dir($upload_path)) mkdir($upload_path);

		$config['upload_path']      = $upload_path;
		$config['allowed_types']    = $allowed_types;
		$config['max_size']         = $max_size;
		$config['encrypt_name']     = TRUE;
		$config['file_ext_tolower'] = TRUE;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field)) {
			$response = array(
				'code' => 'FAILED',
				'message' => $this->upload->display_errors()
			);
		} else {
			$response = array(
				'code' => 'SUCCESS',
				'message' => 'File uploaded successfully',
				'data' => json_encode($this->upload->data())
			);
		}

		return $response;
	}

	public function uploaded_file_check($allowed_mime_type_arr = array(), $msg = '')
	{
		$mime = get_mime_by_extension($_FILES['file']['name']);
		if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
			if (in_array($mime, $allowed_mime_type_arr)) {
				return true;
			} else {
				$this->form_validation->set_message('file_check', $msg);
				return false;
			}
		} else {
			$this->form_validation->set_message('file_check', 'Please choose a file to upload.');
			return false;
		}
	}

	protected function get_module_by_controller($controller = '')
	{
		$this->load->model('module_model', 'Module');
		return $this->Module->get_by_controller($controller);
	}

	protected function log_history($affected_table = '', $operation = '', $new_data = '', $old_data = null, $where_clause = null)
	{
		$this->load->model('Log_history_model', 'LogHistory');

		$user_id = $this->encryption->decrypt($this->session->userdata('sess_user_id'));
		$created_at = date('Y-m-d H:i:s');

		$data = compact('user_id', 'affected_table', 'operation', 'new_data', 'old_data', 'where_clause', 'created_at');
		$this->LogHistory->insert($data);
	}

	protected function swal($type = '', $title = '', $html = '')
	{
		return compact('type', 'title', 'html');
	}

	protected function toastr($type = '', $title = '', $message = '')
	{
		return compact('type', 'title', 'message');
	}

	protected function alert($type = '', $title = '', $message = '')
	{
		$icon = '';
		switch ($type) {
			case 'alert-danger':
				$icon = 'fas fa-ban';
			break;

			case 'alert-info':
				$icon = 'fas fa-info';
			break;

			case 'alert-warning':
				$icon = 'fas fa-exclamation-triangle';
			break;

			case 'alert-success':
				$icon = 'fas fa-check';
			break;
		}
		return compact('type', 'icon', 'title', 'message');
	}

	protected function button_edit($url = '')
	{
		return '<a href="' . $url . '" class="btn btn-sm btn-primary mb-1 mr-1"><i class="fas fa-sm fa-edit"></i> Edit</a>';
	}

	protected function button_delete($url = '', $data = '')
	{
		return '<a href="' . $url . '" class="do-delete btn btn-sm btn-danger mb-1 mr-1" ' . $data . '><i class="fas fa-sm fa-trash"></i> Delete</a>';
	}

	protected function button_download($url = '', $data = '')
	{
		return '<a href="' . $url . '" class="do-download btn btn-sm btn-success mb-1 mr-1" ' . $data . '><i class="fas fa-sm fa-download"></i> Download</a>';
	}

	protected function button_upload($url = '', $data = '')
	{
		return '<a href="' . $url . '" class="do-download btn btn-sm btn-primary mb-1 mr-1" ' . $data . '><i class="fas fa-sm fa-upload"></i> Upload</a>';
	}

	protected function button_view($url = '', $data = '')
	{
		return '<a href="' . $url . '" class="do-view btn btn-sm btn-info mb-1 mr-1" ' . $data . '><i class="fas fa-sm fa-eye"></i> View</a>';
	}

	protected function create_badge($type = '', $text = '')
	{
		return '<span class="badge badge-' . $type . '">' . $text . '</span>';
	}

	protected function go_to($uri = '', $toastr = array())
	{
		if ($uri === '') show_404();

		if (isset($toastr['type']) && isset($toastr['title']) && isset($toastr['message'])) {
			$this->session->set_flashdata('toastr', array(
				'type' => $toastr['type'],
				'title' => $toastr['title'],
				'message' => addslashes($toastr['message'])
			));
		}
		redirect($uri);
	}

	protected function generate_random_password($password_length = 8)
	{
		$str = '1234567890abcdefghijkmnopqrstuvwxyz1234567890ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890';
		$strlen = strlen($str);
		$strsplit = str_split($str);
		$password = '';
		for ($i = 0; $i < $password_length; $i++) {
			// shuffle($strsplit);
			shuffle($strsplit);
			$idx = rand(0, $strlen);
			$password .= $strsplit[$idx];
		}

		return $password;
	}

	// ----------------------------------------------------------

	// Custom functions
	protected function get_user_profile($user = stdClass)
	{
		switch ($user->role->name)
		{
			case 'Administrator':
			case 'Super User':
			case 'Teacher':
			case 'Administrative Staff':
				{
					$this->load->model('staff_user_model', 'StaffUser');
					return $this->StaffUser->get_staff_by_user_id($user->id);
				}
			break;

			case 'Student':
				{
					$this->load->model('student_user_model', 'StudentUser');
					return $this->StudentUser->get_student_by_user_id($user->id);
				}
			break;

			case 'Parents':
				{
					$this->load->model('parents_user_model', 'ParentsUser');
					return $this->ParentsUser->get_parents_by_user_id($user->id);
				}
			break;
		}
	}

	protected function get_student_session($student_id = 0)
	{
		$this->load->model('student_session_model', 'StudentSession');

		return $this->StudentSession->get_by_student_id($student_id);
	}

	protected function get_student_parents($user = stdClass)
	{
		$this->load->model('student_parents_model', 'StudentParents');

		return $this->StudentParents->get_parents_by_student_id($user->profile->id);
	}

	protected function get_parents_children($user = stdClass)
	{
		$this->load->model('student_model', 'Student');

		return $this->Student->get_by_parents_id($user->profile->id);
	}

	protected function get_notification_by_module($user = stdClass, $module = '')
	{
		$notification = '';
		switch ($module) {

			case 'Announcement':
				$this->load->model('announcement_model', 'Announcement');
				$this->load->model('announcement_user_model', 'AnnouncementUser');
				$announcement_user = $this->AnnouncementUser->get_by_user_id($user->id);
				if ($announcement_user != FALSE) {
					switch ($user->role->name) {
						case 'Student':
							$params = array('clauses' => array('where' => array('updated_at >' => $announcement_user->updated_at)));
							$notification = $this->Announcement->count_filtered($params);
						break;

						case 'Parents':
						break;
					}
				}
			break;

			case 'Assignment':
				switch ($user->role->name) {
					case 'Student':

						$this->load->model('assignment_student_model', 'AssignmentStudent');
						$this->load->model('student_session_model', 'StudentSession');

						$student_session = $this->StudentSession->get_by_student_id($user->profile->id);
						$not_submitted = $this->AssignmentStudent->count_havenot_submitted_yet($user->profile->id, $student_session->class_id, $student_session->section_id);

						$notification = ($not_submitted > 0) ? $not_submitted : '';
					break;
				}
			break;

			default:
				$notification = '';
		}

		return $notification;
	}

	protected function get_user_notification($user = stdClass)
	{
		$notification = array();
		foreach ($user->modules as $module_id => $action) {
			$this->load->model('module_model', 'Module');
			$module = $this->Module->get_by_id($module_id);

			$notification[$module->name] = $this->get_notification_by_module($user, $module->name);
		}

		return $notification;
	}

	protected function update_notification_by_module($module = '')
	{
		switch ($module) {

			case 'Announcement':
				$this->AnnouncementUser->replace(array('user_id' => $this->sess_user->id, 'updated_at' => date('Y-m-d H:i:s')));
				$this->sess_user->notifications['Announcement'] = '';
				$this->session->set_userdata('sess_user', $this->sess_user);
			break;
		}
	}
}
