<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	protected $table;

	public function __construct()
	{
		parent::__construct();
	}

	protected function from($table = '')
	{
		if ( empty($table)) return FALSE;

		if ($this->db->simple_query("SHOW TABLES LIKE " . $this->db->escape($table))) {
			$this->table = $table;
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private function _get($params = array())
	{
		if ( empty($this->table)) return FALSE;

		$this->db->from($this->table);

		if (isset($params['join'])) {
			foreach($params['join'] as $join) {
				if (isset($join[2])) {
					$this->db->join($join[0], $join[1], $join[2]);
				} else {
					$this->db->join($join[0], $join[1]);
				}
			}
		}

		foreach ((isset($params['clauses'])) ? $params['clauses'] : array() as $clause => $bind_params)
		{
			switch ($clause)
			{
				case 'where':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->where($key, $val);
					}
				}
				break;

				case 'or_where':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->or_where($key, $val);
					}
				}
				break;

				case 'where_in':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->where_in($key, $val);
					}
				}
				break;

				case 'or_where_in':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->or_where_in($key, $val);
					}
				}
				break;

				case 'where_not_in':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->where_not_in($key, $val);
					}
				}
				break;

				case 'or_where_not_in':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->or_where_not_in($key, $val);
					}
				}
				break;

				case 'like':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->like($key, $val);
					}
				}
				break;

				case 'or_like':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->or_like($key, $val);
					}
				}
				break;

				case 'not_like':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->not_like($key, $val);
					}
				}
				break;

				case 'or_not_like':
				{
					foreach ($bind_params as $key => $val) {
						$this->db->or_not_like($key, $val);
					}
				}
				break;
			}
		}

		if (isset($params['group_by'])) $this->db->group_by($params['group_by']);
		if (isset($params['order_by'])) $this->db->order_by($params['order_by']);
	}

	public function get($params = array())
	{
		if ( empty($this->table)) return FALSE;

		if (isset($params['select'])) $this->db->select($params['select']);

		$this->_get($params);

		if (isset($params['limit']) && $params['limit'] > 0) $this->db->limit($params['limit'], $params['offset']);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function row($params = array(), $n = 0)
	{
		if ( empty($this->table)) return FALSE;

		if (isset($params['select'])) $this->db->select($params['select']);

		$this->_get($params);

		if (isset($params['limit']) && $params['limit'] > 0) $this->db->limit($params['limit'], $params['offset']);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->row($n);
		} else {
			return FALSE;
		}
	}

	public function count_filtered($params = array())
	{
		if ( empty($this->table)) return FALSE;

		$this->_get($params);
		return $this->db->count_all_results();
	}

	public function count_all()
	{
		if ( empty($this->table)) return FALSE;

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function insert($data = array())
	{
		if ( empty($this->table)) return FALSE;

		return $this->db->insert($this->table, $data);
	}

	public function replace($data = array())
	{
		if ( empty($this->table)) return FALSE;

		return $this->db->replace($this->table, $data);
	}

	public function update($data = array(), $where = array())
	{
		if ( empty($this->table)) return FALSE;

		foreach ($data as $k => $v) {
			if (is_array($v)) {
				$this->db->set($k, $v[0], $v[1]);
			} else {
				$this->db->set($k, $v);
			}
		}
		$this->db->where($where);
		return $this->db->update($this->table);
	}

	public function delete($where = array())
	{
		if ( empty($this->table)) return FALSE;

		$this->db->where($where);
		return $this->db->delete($this->table);
	}

	public function get_by_id($id = 0)
	{
		$params = array('clauses' => array('where' => array('id' => $id)));
		return $this->row($params);
	}

	protected function debug_result($result = NULL)
	{
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));

		$this->output->_display();
		exit;
	}
}
