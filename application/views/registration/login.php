<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registartion</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	

	<?php include"css.php"; ?>


</head>
<body>
	

	<div class="bg-body"></div>

	<?php include"header.php"; ?>

	<div class="wrapper">
		
		<div class="box-header">
			<h3>
				LOGIN
			</h3>
			<div class="text-center">
				Silahkan login terlebih dahulu untuk melengkapi detail informasi pendaftaran Anda.
			</div>
		</div>

		<div class="line"></div>

		<div class="box-content">
			<div class="desc-content">
				<div class="box-login">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Email
								</div>
								<input type="text" name="" class="text-field">
							</div>
							<div class="box-field">
								<div class="title-field">
									Password
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="text-center">
						<button class="button-default full-width">
							Login
						</button>
					</div>
					<div class="box-link">
						<a href="<?php echo base_url(); ?>registration/forgot_pass">
							Lupa Kata Sandi?
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>

	<?php include"js.php"; ?>
	
</body>
</html>
