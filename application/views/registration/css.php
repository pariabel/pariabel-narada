<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php echo base_url();?>/favicon.ico">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/registration/lib/bootstrap/css/bootstrap.css">
	<link href="<?php echo base_url();?>/assets/registration/lib/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css" />

	<!-- Fontawesome -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/registration/lib/fontawesome/css/all.css">

	<!-- Master -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/registration/css/master.css">

