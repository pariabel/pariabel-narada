<!DOCTYPE html>
<html style="width: 100%">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invoice Narada School</title>
</head>
<body style="background-color: #e8edef; font-size: 12px; color: #555; padding: 20px; margin: 0; font-family: 'Tahoma'; box-sizing: border-box; width: 100%; overflow: auto; letter-spacing: 0.2px;">
    <table style="width: 100%; max-width: 600px; background-color: #fff; margin: 0 auto; border-radius: 10px;">
        <!--Header-->
        <tr>
            <td style="padding: 10px 20px">
                <table style="width: 100%">
                    <tr>
                        <td style="vertical-align: middle; text-align: center;"> 
                            <img style="width: 100px;" src="https://naradaschool.sch.id/demo/public/assets/registration/images/login-logo.png">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 3px solid #f3f6f7;"></td>
        </tr>
        <!--Header-->

        <!--body-->
        <tr>
            <td style="padding: 20px 20px">
                    <table>
                        <tr>
                            <div style="text-align: center;">
                                <div style="text-align: center; font-size: 14px; margin-top: 10px; display: inline-block; color: #00aad2; font-weight: 600;">
                                        Info Pembayaran
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <td style="line-height: 24px;">
                                <p style="font-size: 16px; font-weight: 600;">
                                    Hai, Mr. / Mrs.<?php echo ucwords($parent_name);?>
                                </p>
                                <p style="font-size: 14px;">
                                    Terima kasih kasih telah mengisi formulir pendaftaran di Narada School.
                                    Berikut ini info pembayaran Anda: 
                                </p>
                            </td>
                        </tr>
                    </table>
                    <table style="font-size: 14px; line-height: 24px;">
                        <tr style="vertical-align: top;">
                            <td>No. Invoice</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td style="font-weight: 600;"><?php echo "INV".date("Y").date("m")."-".$payment_key;?></td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td>Total Pembayaran</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td style="font-weight: 600;">Rp. <?php echo number_format($fee_total);?></td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td>Metode Pembayaran</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td style="font-weight: 600;">Virtual Account (BANK BCA)</td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td>No. Virtual Account</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td style="font-weight: 600;"><?php echo $phone_mobile.$payment_key;?></td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td>Status</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td style="font-weight: 600; color: red;">Belum dibayar</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td style="line-height: 24px;">
                                <p style="font-size: 14px;">
                                    Pembayaran  dapat dilakukan sebelum tanggal <span style="font-weight: 600;">20 Juni 2021, 00:00 WIB</span>
                                </p>
                            </td>
                        </tr>
                    </table>
                    <table style="font-size: 14px; line-height: 24px;">
                        <tr>
                            <td colspan="2">
                                <p style="font-weight: 600;">
                                    Detail Pembayaran 
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="font-weight: 600;">
                                    <a href="#" target="_blank" style="color: #555; text-decoration: none;">
                                        Formulir
                                    </a>
                                </div>
                                <div style="font-size: 14px;">Rp. <?php echo number_format($fee_form) ;?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="font-weight: 600;">
                                    <a href="#" target="_blank" style="color: #555; text-decoration: none;">
                                        Down Payment
                                    </a>
                                </div>
                                <div style="font-size: 14px;">Rp. <?php echo number_format($fee_dp) ;?></div>
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
        <!--body-->

        <!--footer-->
        <tr>
            <td style="padding: 10px 20px; background-color: #f7f9fa;">
                <p style="color: #555; text-align: center; font-size: 12px; line-height: 23px;">
                    Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini. Jika butuh bantuan, silakan hubungi email kami <span style="color:#00aad2;"><b>support@naradschool.com</b></span>
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 20px;">
                <table style="width: 100%">
                    <tr>
                        <td style="color: #555; vertical-align: middle; font-size: 12px; line-height: 22px;">
                            <div style="display: block; text-align: center; margin-bottom: 5px;">
                                <a href="#" target="_blank" style="text-decoration: none; margin-left: 2px; margin-right: 2px;">
                                    <img style="width: 24px;" src="https://naradaschool.sch.id/demo/public/assets/registration/images/sosmed-facebook.png">
                                </a>
                                <a href="#" target="_blank" style="text-decoration: none; margin-left: 2px; margin-right: 2px;">
                                    <img style="width: 24px;" src="https://naradaschool.sch.id/demo/public/assets/registration/images/sosmed-instagram.png">
                                </a>
                            </div>
                            <div style="display: block; text-align: center;">
                                2022 - 2023 Narada School
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!--footer-->
    </table>

</body>
</html>