<!DOCTYPE html>
<html style="width: 100%">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Narada School</title>
	</head>
	<body style="background-color: #e8edef; font-size: 12px; color: #555; padding: 20px; margin: 0; font-family: 'Tahoma'; box-sizing: border-box; width: 100%; overflow: auto; letter-spacing: 0.2px;">
		<table style="width: 100%; max-width: 600px; background-color: #fff; margin: 0 auto; border-radius: 10px;">
			<!--Header-->
			<tr>
				<td style="padding: 10px 20px">
					<table style="width: 100%">
						<tr>
							<td style="vertical-align: middle; text-align: center;"> 
                                                            <img style="width: 100px;" src="https://naradaschool.sch.id/demo/public/assets/registration/images/login-logo.png">
                                                        </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border-bottom: 3px solid #f3f6f7;"></td>
			</tr>
			<!--Header-->

			<!--body-->
			<tr>
				<td style="padding: 20px 20px">
					<table>
						<tr>
							<div style="text-align: center;">
								<div style="text-align: center; font-size: 14px; margin-top: 10px; display: inline-block; color: #00aad2; font-weight: 600;">
									Invitation
								</div>
							</div>
						</tr>
						<tr>
							<td style="line-height: 24px;">
								<p style="font-size: 16px; font-weight: 600;">
									Hai, Mr. / Mrs. <?php echo ucwords($parent_name);?>
								</p>
								<p style="font-size: 14px;">
									Terima kasih kasih telah mengisi formulir pendaftaran di Narada School.
									Untuk tahap selanjutnya kami menngundang Anda untuk mengunjungi sekolah kami. 
								</p>
							</td>
						</tr>
					</table>
					<table style="font-size: 14px; line-height: 24px;">
						<tr style="vertical-align: top;">
							<td>Tanggal</td>
							<td>&nbsp;:&nbsp;</td>
							<td style="font-weight: 600;">10 Juni 2021</td>
						</tr>
						<tr style="vertical-align: top;">
							<td>Waktu</td>
							<td>&nbsp;:&nbsp;</td>
							<td style="font-weight: 600;">10:00 - 14:00</td>
						</tr>
						<tr style="vertical-align: top;">
							<td>Lokasi</td>
							<td>&nbsp;:&nbsp;</td>
							<td style="font-weight: 600;">Jakarta</td>
						</tr>
                                                <tr style="vertical-align: top;">
							<td></td>
							<td>&nbsp;:&nbsp;</td>
                                                        <td style="font-weight: 600;"><button style="background-color:#F5DEB3">Konfirmasi Hadir</button></td>
						</tr>
					</table>
				</td>
			</tr>
			<!--body-->

			<!--footer-->
			<tr>
				<td style="padding: 10px 20px; background-color: #f7f9fa;">
					<p style="color: #555; text-align: center; font-size: 12px; line-height: 23px;">
						Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini. Jika butuh bantuan, silakan hubungi email kami <span style="color:#00aad2;"><b>support@naradschool.com</b></span>
					</p>
				</td>
			</tr>
			<tr>
				<td style="padding: 20px 20px;">
					<table style="width: 100%">
						<tr>
							<td style="color: #555; vertical-align: middle; font-size: 12px; line-height: 22px;">
								<div style="display: block; text-align: center; margin-bottom: 5px;">
                                                                    <a href="#" target="_blank" style="text-decoration: none; margin-left: 2px; margin-right: 2px;">
                                                                        <img style="width: 24px;" src="https://naradaschool.sch.id/demo/public/assets/registration/images/sosmed-facebook.png">
                                                                    </a>
                                                                    <a href="#" target="_blank" style="text-decoration: none; margin-left: 2px; margin-right: 2px;">
                                                                        <img style="width: 24px;" src="https://naradaschool.sch.id/demo/public/assets/registration/images/sosmed-instagram.png">
                                                                    </a>
                                                                </div>
								<div style="display: block; text-align: center;">
									2022 - 2023 Narada School
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--footer-->
		</table>

	</body>
</html>