<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registartion</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	

	<?php include"css.php"; ?>


</head>
<body>
	

	<div class="bg-body"></div>

	<?php include"header.php"; ?>

	<div class="wrapper">
		
		<div class="box-header">
			<h3>
				Lupa Kata Sandi
			</h3>
			<div class="text-center">
				Untuk mendapatkan kata sandi baru silahkan masukan email Anda
			</div>
		</div>

		<div class="line"></div>

		<div class="box-content">
			<div class="desc-content">
				<div class="box-login">
					<form method="POST" action="forgot_pass_success">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-lg-12">
								<div class="box-field">
									<div class="title-field">
										Email
									</div>
									<input type="text" name="" class="text-field">
								</div>
							</div>
						</div>
						<div class="text-center">
							<button class="button-default full-width">
								Process
							</button>
						</div>
					</form>
					<div class="box-link">
						Sudah punya akun? 
						<a href="<?php echo base_url(); ?>login">
							<b>Login</b>
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>

	<?php include"js.php"; ?>
	
</body>
</html>
