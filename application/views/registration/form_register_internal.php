<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registration</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<?php include"css.php"; ?>
</head>
<body>

	<div class="bg-body"></div>

	<?php include"header.php"; ?>
	
	<div class="wrapper">
            <form action="#" id="myForm" method="post" >
		<div class="box-header">
			<h3>
				DATA CALON SISWA
			</h3>
		</div>

		<div class="box-content">

			<!-- Data Siswa -->
			<div class="title-content">
				<h3>
					File Formulir
				</h3>
			</div>
			<div class="desc-content">
				<div class="row">
                                        <div class="col-xs-12 col-sm-12 col-lg-12">
                                            <div class="title-field">
                                                Ketikan NIS <div class="icon-mandatory">*</div>
                                                <span><i>Nomor Induk Siswa</i></span>
                                            </div>
                                            <input type="text" name="nis" id="nis" class="text-field" placeholder="Ketikan Nomor Induk Siswa , lalu tekan Enter..."  required>
                                            <span style='display:none;color:red;' id='spnEmpty'><i>Nomor Induk Siswa tidak ditemukan</i></span>
                                        </div>
                                        
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<div class="box-field">
							<div class="title-field">
								Unit/Jenjang <div class="icon-mandatory">*</div>
								<span><i>Unit/Level</i></span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<span class="select-option-field">
								<select name="i_level" id="i_level" disabled required>
									<option selected="selected">Select</option>
									
								</select>
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<div class="box-field">
							<div class="title-field">
								Kelas <div class="icon-mandatory">*</div>
								<span><i>Class</i></span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<span class="select-option-field">
								<select name="i_class" id="i_class" disabled required>
									<option selected="selected">Select</option>
									
								</select>
							</span>
						</div>
					</div>
				</div>
				<div class="box-highlight" id="boxWaktu" style="display:none;">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Waktu yang diingikan (Khusus Presschool & Kindergarten)
									<span><i>Desired time (Especially for Presschool & Kindergarten)</i></span>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="session" value="pagi">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Pagi</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="session" value="siang">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Siang</div>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
                            
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<div class="title-field">
								No. Formulir 
							</div>
                                
							<b name="bNoForm" id="bNoForm">-</b>
                                                        <input type="hidden" name="student_id" id='student_id' class="text-field"  required>
                                                        <input type="hidden" name="reg_no" id='reg_no' class="text-field"  required>
                                                        <input type="hidden" name="reg_type" id='reg_type' class="text-field" value="internal"  required>
						</div>
					</div>
				</div>
				<div class="box-field">
					<div class="title-field">
						Nama Lengkap <div class="icon-mandatory">*</div>
						<span><i>Full Name</i></span>
					</div>
                                    <input type="text" name="full_name" class="text-field" id='full_name' required>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<div class="title-field">
								Nama Siswa <div class="icon-mandatory">*</div>
								<span><i>Student Name</i></span>
							</div>
							<input type="text" name="student_name" class="text-field" id='student_name' required="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<div class="box-field">
							<div class="title-field">
								Tempat & Tanggal Lahir <div class="icon-mandatory">*</div>
								<span><i>Place & Date of Birth</i></span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<input type="text" name="dob_place" id="dob_place" class="text-field" placeholder="Tempat/Place">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<div class="input-group date datepicker" data-date-format="mm-dd-yyyy">
							    <input type="text" name="dob_date" id="dob_date" class="text-field" placeholder="Tanggal Lahir/Date of Birth" readonly required>
							    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<div class="title-field">
								Nama Orang Tua <div class="icon-mandatory">*</div>
								<span><i>Parents' Name</i></span>
							</div>
							<input type="text" name="parent_name" id='parent_name' class="text-field" value="" >
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<div class="title-field">
								Nama Saudara Kandung Narada <div class="icon-mandatory">*</div>
								<span><i>Name of Siblings in Narada</i></span>
							</div>
							<input type="text" name="brother_name" id="brother_name" class="text-field">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<div class="box-field">
							<div class="title-field">
								No. Telepon <div class="icon-mandatory">*</div>
								<span><i>Phone number</i></span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<input type="text" name="phone_home" id="phone_home" class="text-field" placeholder="Rumah">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<input type="text" name="phone_mobile" id="phone_mobile" class="text-field" placeholder="Handphone_mobile" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-6">
						<div class="box-field">
							<div class="title-field">
								Email Orang Tua <div class="icon-mandatory">*</div>
								<span><i>Parent's Email (active)</i></span>
							</div>
							<input type="text" name="email_parent" class="text-field" id='email_parent'>
						</div>
					</div>
				</div>
			</div>
		</div>

                <div id="konten" style="display:none;"></div>
                <div id="editor"></div>
                
		<div class="box-footer text-center">
			<a href="#">
                            <button class="button-default" id='btnSubmit' disabled>
                                    Selanjutnya
                            </button>
			</a>
		</div>
               </form>
	</div>

	
	<?php include"js.php"; ?>

	<script type="text/javascript">
		$(function () {
		  $(".datepicker").datepicker({ 
		        autoclose: true, 
		        todayHighlight: true,
                        format: 'yyyy-mm-dd',
		  }).datepicker('update', new Date());
		});
	</script>
	
	
</body>
</html>
