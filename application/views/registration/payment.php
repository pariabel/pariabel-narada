<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registartion</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<?php include"css.php"; ?>


</head>
<body>
	

	<div class="bg-body"></div>

	<?php include"header.php"; ?>

	<div class="wrapper">
		
		<div class="box-header">
			<h3>
                            METODE PEMBAYARAN
			</h3>
			<div class="desc-content">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						Nama Bank
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-9">
						<b>Bank BCA</b> - Virtual Account
					</div>
				</div>
                                <div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						Nomor Virtual Account
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-9">
                                            <b><?php echo $payment_code; ?></b>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						Biaya Formulir
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-9">
						Rp.<?php echo number_format($form); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						Biaya DP
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-9">
						Rp. <?php echo number_format($dp);?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						Total Pembayaran
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-9">
						<b>Rp.Rp.<?php echo number_format($total);?></b>
					</div>
				</div>
			</div>
		</div>


		<div class="box-footer text-center">
			<a href="<?php echo base_url(); ?>registration/success">
				<button class="button-default">
					Bayar Sekarang
				</button>
			</a>
		</div>

	</div>

	<?php include"js.php"; ?>
	
</body>
</html>
