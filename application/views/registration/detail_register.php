<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registartion</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	

	<?php include"css.php"; ?>




</head>
<body>

	<div class="bg-body"></div>

	<?php include"header.php"; ?>
	
	<div class="wrapper">
		
		<div class="box-header">
			
			<h3>
				PENDAFTARAN
			</h3>
			<div class="desc-header">
				Silahkan isi formulir pendaftaran data calon siswa. Batas pendaftaran <b>tanggal 16 Juli 2021</b>. Akan dianggap mengundurkan diri apabila pendaftaran dan persyaratan kelengkapan dokumen melewati batas waktu yang disebutkan.
				<span>
					<i>Please fill in the registration form for prospective students. The deadline for registration is 16 July 2021. It will be considered resigned if the registration and required documents have passed the deadline.</i>
				</span>
			</div>
		</div>

		<div class="box-content">

			<!-- Data Siswa -->
			<div class="title-content">
				<h3>
					Data Siswa
				</h3>
			</div>

			<form method="POST" action="register_success">

				<div class="desc-content">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									NISN <div class="icon-mandatory">*</div>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									NIK <div class="icon-mandatory">*</div>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									No. Kartu Keluarga <div class="icon-mandatory">*</div>
									<span><i>Kartu Keluarga Number</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									<b>Nama Peserta Didik</b> 
									<span><i><b>Applicant’s Name</b></i></span>
								</div>
							</div>
							<hr />
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Nama Lengkap <div class="icon-mandatory">*</div>
									<span><i>Full Name</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Nama Panggilan <div class="icon-mandatory">*</div>
									<span><i>Nickname</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Jenis Kelamin <div class="icon-mandatory">*</div>
									<span><i>Gander</i></span>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Pria</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Wanita</div>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Tempat & Tanggal Lahir <div class="icon-mandatory">*</div>
									<span><i>Place & Date of Birth</i></span>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<input type="text" name="" class="text-field" placeholder="Tempat/Place">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="input-group date datepicker" data-date-format="mm-dd-yyyy">
								    <input type="text" name="" class="text-field" placeholder="Tanggal Lahir/Date of Birth" readonly>
								    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-lg-8">
							<div class="box-field">
								<div class="title-field">
									Agama <div class="icon-mandatory">*</div>
									<span><i>Religion</i></span>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="agama">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Buddha</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="agama">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Kristen</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="agama">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Islam</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="agama">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Katolik</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="agama">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Khong Hu Chu</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="agama">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Hindu</div>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Kewarganegaraan <div class="icon-mandatory">*</div>
									<span><i>Nationality</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Anak Ke <div class="icon-mandatory">*</div>
									<span><i>Child number</i></span>
								</div>
							</div>
						</div>
						<div class="col-3 col-md-3 col-lg-4">
							<input type="text" name="" class="text-field">
						</div>
						<div class="col-3 col-sm-3 col-lg-2">
							<div class="box-label">
								Dari:
								<span><i>out of:</i></span>
							</div>
						</div>
						<div class="col-3 col-sm-3 col-lg-4">
							<input type="text" name="" class="text-field">
						</div>
						<div class="col-3 col-sm-3 col-lg-2">
							<div class="box-label">
								Saudara
								<span><i>Brothers</i></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Hobi <div class="icon-mandatory">*</div>
									<span><i>Hobbies</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Status Siswa <div class="icon-mandatory">*</div>
									<span><i>Student’s Status</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Bahasa Sehari – hari <div class="icon-mandatory">*</div>
									<span><i>Language used daily</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
				</div>

				<!-- Keterangan Tempat Tinggal -->
				<div class="title-content">
					<h3>
						Keterangan Tempat Tinggal
					</h3>
				</div>
				<div class="desc-content">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Alamat <div class="icon-mandatory">*</div>
									<span>
										<i>Address</i>
									</span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Kode Pos <div class="icon-mandatory">*</div>
									<span>
										<i>Postal Code</i>
									</span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Jarak Ke Sekolah <div class="icon-mandatory">*</div>
									<span>
										<i>Home to School distance</i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-9 col-sm-6 col-lg-6">
							<input type="text" name="" class="text-field">
						</div>
						<div class="col-3 col-sm-6 col-lg-6">
							KM
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									No. Telepon / Handphone	<div class="icon-mandatory">*</div>
									<span>
										<i>Telephone / Mobile Number</i>
									</span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
				</div>


				<!-- Keterangan Kesehatan -->
				<div class="title-content">
					<h3>
						Keterangan Kesehatan
					</h3>
				</div>
				<div class="desc-content">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Berat Badan <div class="icon-mandatory">*</div>
									<span>
										<i>Weight</i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-9 col-sm-6 col-lg-6">
							<input type="text" name="" class="text-field">
						</div>
						<div class="col-3 col-sm-6 col-lg-6">
							Kg
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Tinggi Badan <div class="icon-mandatory">*</div>
									<span>
										<i>Height</i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-9 col-sm-6 col-lg-6">
							<input type="text" name="" class="text-field">
						</div>
						<div class="col-3 col-sm-6 col-lg-6">
							Cm
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Golongan Darah <div class="icon-mandatory">*</div>
									<span><i>Blood Type</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
				</div>


				<!-- Keterangan Pendidikan -->
				<div class="title-content">
					<h3>
						Keterangan Pendidikan Sebelumnnya
					</h3>
				</div>
				<div class="desc-content">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Asal Sekolah <div class="icon-mandatory">*</div>
									<span><i>Previous School</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									No. Ijasah	<div class="icon-mandatory">*</div>
									<span><i>Graduation Certificate No</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Tanggal Ijasah	<div class="icon-mandatory">*</div>
									<span><i>Issued Date</i></span>
								</div>
								<div class="box-field">
									<div class="input-group date datepicker" data-date-format="mm-dd-yyyy">
									    <input type="text" name="" class="text-field" placeholder="Tanggal Lahir/Date of Birth" readonly>
									    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- Keterangan Orang Tua -->
				<div class="title-content">
					<h3>
						Keterangan Orang Tua
					</h3>
				</div>
				<div class="desc-content">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									<b>DATA AYAH</b>
									<span><i><b>FATHER</b></i></span>
								</div>
							</div>
							<hr />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Nama Ayah <div class="icon-mandatory">*</div>
									<span><i>Father’s Name</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Tempat & Tanggal Lahir <div class="icon-mandatory">*</div>
									<span><i>Place & Date of Birth</i></span>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<input type="text" name="" class="text-field" placeholder="Tempat/Place">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="input-group date datepicker" data-date-format="mm-dd-yyyy">
								    <input type="text" name="" class="text-field" placeholder="Tanggal Lahir/Date of Birth" readonly>
								    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Pendidikan Terakhir <div class="icon-mandatory">*</div>
									<span><i>Last Education</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Pekerjaan <div class="icon-mandatory">*</div>
									<span><i>Occupation</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Penghasilan <div class="icon-mandatory">*</div>
									<span><i>Income</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									No Telepon / Hp <div class="icon-mandatory">*</div>
									<span><i>Telephone / Cell Phone</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Email Ayah
									<span><i>Father’s Email</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									<b>DATA IBU</b>
									<span><i><b>MOTHER</b></i></span>
								</div>
							</div>
							<hr />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Nama Ibu <div class="icon-mandatory">*</div>
									<span><i>Mother’s Name</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Tempat & Tanggal Lahir <div class="icon-mandatory">*</div>
									<span><i>Place & Date of Birth</i></span>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<input type="text" name="" class="text-field" placeholder="Tempat/Place">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="input-group date datepicker" data-date-format="mm-dd-yyyy">
								    <input type="text" name="" class="text-field" placeholder="Tanggal Lahir/Date of Birth" readonly>
								    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Pendidikan Terakhir <div class="icon-mandatory">*</div>
									<span><i>Last Education</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Pekerjaan <div class="icon-mandatory">*</div>
									<span><i>Occupation</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Penghasilan <div class="icon-mandatory">*</div>
									<span><i>Income</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									No Telepon / Hp <div class="icon-mandatory">*</div>
									<span><i>Telephone / Cell Phone</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Email Ibu
									<span><i>Mother’s Email</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
				</div>


				<!-- Data Saudara Kandung -->
				<div class="title-content">
					<h3>
						DATA SAUDARA KANDUNG YANG BERSEKOLAH DI NARADA
					</h3>
				</div>
				<div class="desc-content">

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Nama Siswa	
									<span><i>Student’s Name</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Jenis Kelamin
									<span><i>Gander</i></span>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Pria</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Wanita</div>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Kelas
									<span><i>Class</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Nama Siswa	
									<span><i>Student’s Name</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Jenis Kelamin
									<span><i>Gander</i></span>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Pria</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Wanita</div>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Kelas
									<span><i>Class</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Nama Siswa	
									<span><i>Student’s Name</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Jenis Kelamin
									<span><i>Gander</i></span>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Pria</div>
									</div>
									<div class="clearer"></div>
								</div>
								<div class="list-content-selector">
									<label class="container-radio">
										<input type="radio" name="jenis-kelamin">
										<span class="checkmark-radio"></span>
									</label>
									<div class="content-radio">
										<div class="type text-left">Wanita</div>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6">
							<div class="box-field">
								<div class="title-field">
									Kelas
									<span><i>Class</i></span>
								</div>
								<input type="text" name="" class="text-field">
							</div>
						</div>
					</div>

				</div>

				<!-- Keterangan Kesehatan -->
				<div class="title-content">
					<h3>
						Upload Dokumen Pendukung
					</h3>
				</div>
				<div class="desc-content">
					<div class="row">
						<div class="col-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Akta Lahir <div class="icon-mandatory">*</div>
									<span>
										<i>Certificate of Birth</i>
									</span>
								</div>
								<div class="box-upload-file">
									<input id="uploadFile" class="text-upload" placeholder="Belum ada file yang dipilih" disabled="disabled" />
									<div class="fileUpload button-file">
									    <span>Browse</span>
									    <input id="uploadBtn" type="file" class="upload" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									Kartu Keluarga <div class="icon-mandatory">*</div>
									<span>
										<i>Family Card</i>
									</span>
								</div>
								<div class="box-upload-file">
									<input id="uploadFile" class="text-upload" placeholder="Belum ada file yang dipilih" disabled="disabled" />
									<div class="fileUpload button-file">
									    <span>Browse</span>
									    <input id="uploadBtn" type="file" class="upload" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									KTP Ayah <div class="icon-mandatory">*</div>
									<span>
										<i>Photocopy of Father ID card</i>
									</span>
								</div>
								<div class="box-upload-file">
									<input id="uploadFile" class="text-upload" placeholder="Belum ada file yang dipilih" disabled="disabled" />
									<div class="fileUpload button-file">
									    <span>Browse</span>
									    <input id="uploadBtn" type="file" class="upload" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-lg-12">
							<div class="box-field">
								<div class="title-field">
									KTP Ibu <div class="icon-mandatory">*</div>
									<span>
										<i>Photocopy of Mother ID card</i>
									</span>
								</div>
								<div class="box-upload-file">
									<input id="uploadFile" class="text-upload" placeholder="Belum ada file yang dipilih" disabled="disabled" />
									<div class="fileUpload button-file">
									    <span>Browse</span>
									    <input id="uploadBtn" type="file" class="upload" />
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>


				<p>
					<div class="text-center">
						<button class="button-default">
							Submit
						</button>
					</div>
				</p>

				<p>&nbsp;</p>

			</form>

		</div>

		
	</div>

	
	<?php include"js.php"; ?>

	<script type="text/javascript">
		document.getElementById("uploadBtn").onchange = function () {
		    document.getElementById("uploadFile").value = this.value;
		};
	</script>

	<script type="text/javascript">
		$(function () {
		  $(".datepicker").datepicker({ 
		        autoclose: true, 
		        todayHighlight: true
		  }).datepicker('update', new Date());
		});
	</script>
	
	
</body>
</html>
