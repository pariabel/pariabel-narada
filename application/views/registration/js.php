<!-- jQuery -->
<script src="<?php echo base_url();?>/assets/registration/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>/assets/registration/lib/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>/assets/registration/lib/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>/assets/registration/js/master.js"></script>
<script src="<?php echo base_url();?>/assets/registration/js/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/3.2.1/jquery.serializejson.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<script src="<?php echo base_url();?>/assets/registration/js/App.js"></script>