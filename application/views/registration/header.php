<header>
    <input  type="text" id="baseUrl" value="<?php echo base_url();?>">
	<div class="header">
		<div class="logo-header">
			<div class="nav-mobile actBoxMenu">
				<i class="fas fa-bars"></i>
			</div>
			<a href="<?php echo base_url(); ?>registration">
				<img src="<?php echo base_url(); ?>/assets/img/login-logo.png">
			</a>
		</div>
		<div class="menu-header">
			<ul class="ul-menu-header">
				<!-- Sesudah Login -->
				<li><a class="active" href="<?php echo base_url(); ?>registration">Home</a></li>
				<li><a href="<?php echo base_url(); ?>registration/profile">Edit Profil</a></li>
				<li><a href="<?php echo base_url(); ?>registration/detail_register">Pendaftaran</a></li>
				<!-- End -->
				<li><a href="<?php echo base_url(); ?>registration/login">Login</a></li>
			</ul>
		</div>
		<div class="clearer"></div>
	</div>
</header>

<div class="box-menu-overlay">
	<div class="modal-overlay-menu actCloseMenu"></div>
	<div class="box-menu bg-white">
		<div class="close-menu actCloseMenu">
			<i class="fas fa-times"></i>
		</div>
		<div class="logo-mobile">
			<img src="<?php echo base_url(); ?>/assets/img/login-logo.png">
		</div>
		<div class="mobile-menu">
			<!-- Sesudah Login -->
			<div class="mobile-list-menu">
				<a href="#">
					Home
				</a>
			</div>
			<div class="mobile-list-menu">
				<a href="#">
					Edit Profil
				</a>
			</div>
			<div class="mobile-list-menu">
				<a href="#">
					Pendaftaran
				</a>
			</div>
			<!-- End -->
			<div class="mobile-list-menu">
				<a href="#">
					Login
				</a>
			</div>
		</div>
	</div>
</div>