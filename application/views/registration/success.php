<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registration</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	

	<?php include"css.php"; ?>



</head>
<body>
	
	<div class="bg-body"></div>
	
	<?php include"header.php"; ?>

	<div class="wrapper">
		<div class="box-header">
			<h3>
				Penerimaan Siswa Baru (PSB) <br /> Tahun Akademik <br /> 2022/2023
			</h3>
			<div class="desc-header">
				<p>
					Terima kasih.
				</p>

				<p>
					Formulir Pendaftaran telah berhasil Anda isi. Untuk mengetahui informasi tahap selanjutnya silahkan cek email Anda. Kami sudah mengirimkan detail informasi ke email Anda.
				</p>

				<p>
					Salam, <br />
					Narada School
				</p>

			</div>
		</div>
	</div>
		
	

	<?php include"js.php"; ?>
	
</body>
</html>