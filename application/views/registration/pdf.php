<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Narada School | Registartion</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	

	<?php include"css.php"; ?>


</head>
<body>
    <div class="bg-body"></div>
    <div class="wrapper">
        <div class="box-header">
            <center>
                <h3>
                    Informasi Pendahuluan <br >
                    Penerimaan Siswa Baru (PSB) 
                    Tahun Akademik 2022/2023
                </h3>
            </center>
            <div class="desc-header">
                <ol>
                    <li>
                        Sekolah Narada menerapkan pembelajaran secara Agama Buddha.
                        (Motto Narada: Bahusacca, Sippa, Sila – Ilmu Pengetahuan, Keterampilan, Moralitas)
                    </li>
                    <li>
                        Calon siswa baru diwajibkan untuk membeli formulir.
                    </li>
                    <li>
                        Formulir pendaftaran yang sudah dibeli tidak dapat dikembalikan atau dialihkan.
                    </li>
                    <li>
                        Data calon siswa wajib diisi dengan lengkap dan benar sesuai dengan dokumen.
                    </li>
                    <li>
                        Data siswa yang dibutuhkan untuk di-upload adalah:
                        <ul>
                            <li>
                                Akte Lahir
                            </li>
                            <li>
                                Kartu Keluarga
                            </li>
                            <li>
                                Fotokopi KTP Ayah 
                            </li>
                            <li>
                                Fotokopi KTP Ibu
                            </li>
                        </ul>
                    </li>
                    <li>
                        Satu formulir pendaftaran hanya berlaku untuk 1 siswa.
                    </li>
                    <li>
                        Pendaftaran lebih dari 1 nama siswa (yang sibling/mempunyai saudara) boleh menggunakan 1 alamat email saat login.
                    </li>
                    <li>
                        Siswa ekternal wajib mengikuti test masuk, hasil test akan menentukan siswa diterima atau tidaknya.
                    </li>
                    <li>
                        Siswa yang mutasi dari sekolah lain wajib memenuhi persyaratan dokumen yang ditentukan untuk siswa mutasi.
                    </li>
                    <li>
                        <p>
                            Pilihan Kelas dan Cut off date (batas umur) untuk siswa TK & SD, sebagai berikut:
                        </p>
                        <p><b>Preschool :</b></p>
                        <ul>
                            <li>
                                Nursery: tanggal 16 Oktober 2019 - 15 Oktober 2020 atau diawal tahun akademik sudah berumur 2 tahun
                            </li>
                            <li>
                                PreKG: tanggal 16 Oktober 2018 - 15 Oktober 2019 atau diawal tahun akademik sudah berumur 3 tahun
                            </li>
                            <li>
                                K1: tanggal 16 Oktober 2017 - 15 Oktober 2018 atau diawal tahun akademik sudah berumur 4 tahun
                            </li>
                            <li>
                                K2: tanggal 16 Oktober 2016 - 15 Oktober 2017 atau diawal tahun akademik sudah berumur 5 tahun
                            </li>
                        </ul>
                        <br />
                        <p><b>Elementary : Grade 1 - 5</b></p>
                        <ul>
                            <li>
                                Grade 1: tanggal 16 Oktober 2015 - 15 Oktober 2016 atau diawal tahun akademik sudah berumur 6 tahun
                            </li>
                        </ul>
                        <br />
                        <p><b>Junior High : 7 – 8</b></p>
                        <p><b>Senior high : 10 - 11</b></p>
                    </li>
                    <li>
                        Raport siswa kelas 6 dan kelas 9 yang menyatakan lulus/naik kelas akan menjadi acuan diterimanya siswa (selain hasil test penerimaan).
                    </li>
                </ol>

                <div class="list-check-item">
                    <div class="title-check">
                        <center>
                        Dengan ini saya menyetujui dengan syarat dan ketentuan yang ada di formulir ini.
                        </center>
                    </div>
                </div>
                <div style="float:right;">
                    <p>Jakarta, <?php echo $date;?></p>
                    <p>
                    <p>
                    <p>
                    Bpk./Ibu <?php echo ucwords($parent_name);?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
