<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/questionnaire">Questionnaire</a></li>
						<li class="breadcrumb-item"><a href="/questionnaire/question/<?php echo $questionnaire->id; ?>">Question</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>

				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $questionnaire->title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $question->id; ?>">
					<input type="hidden" name="questionnaire_id" value="<?php echo $questionnaire->id; ?>">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Question</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('question') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="question" placeholder="Enter question" value="<?php echo set_value('question', $question->question); ?>">
							<?php echo form_error('question', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<div><label>Option Type</label></div>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="optionRadio" name="option_type" value="radio" <?php echo set_radio('option_type', 'radio', ($question->option_type == 'radio') ? TRUE : FALSE); ?>>
								<label for="optionRadio" class="custom-control-label font-weight-normal">Radio</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="optionCheckbox" name="option_type" value="checkbox" <?php echo set_radio('option_type', 'checkbox', ($question->option_type == 'checkbox') ? TRUE : FALSE); ?>>
								<label for="optionCheckbox" class="custom-control-label font-weight-normal">Checkbox</label>
							</div>
						</div>

						<div class="form-group">
							<button type="button" class="btn btn-sm btn-primary" id="btnNewOption"><i class="fas fa-plus fa-sm"></i> New Option</button>
						</div>

						<?php if ($this->input->post()): ?>
						<div id="optionList" class="form-group">
							<label>Options</label>
							<?php $i = 0; ?>
							<?php foreach ($this->input->post('option') as $k => $v): ?>
							<?php if ($i == 0 OR $i == 1): ?>
							<input type="text" class="form-control mb-2<?php echo (form_error('option[' . $k . ']') != '') ? ' is-invalid' : ' is-valid'; ?>" name="option[<?php echo $k; ?>]" value="<?php echo set_value('option[' . $k. ']'); ?>">
							<?php echo form_error('option[' . $k . ']', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
							<?php else: ?>
							<div class="input-group mb-2">
								<input type="text" class="form-control<?php echo (form_error('option[' . $k . ']') != '') ? ' is-invalid' : ' is-valid'; ?>" name="option[<?php echo $k; ?>]" value="<?php echo set_value('option[' . $k . ']'); ?>">
								<div class="input-group-append option-delete" style="cursor: pointer;">
								<button type="button" class="btn btn-danger"><i class="fas fa-times"></i></button>
								</div>
							</div>
							<?php echo form_error('option[' . $k . ']', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
							<?php endif; ?>
							<?php $i++; ?>
							<?php endforeach; ?>

							<?php foreach (($this->input->post('new_option')) ? $this->input->post('new_option') : array() as $k => $v): ?>
							<div class="input-group mb-2">
								<input type="text" class="form-control<?php echo (form_error('new_option[' . $k . ']') != '') ? ' is-invalid' : ' is-valid'; ?>" name="new_option[<?php echo $k; ?>]" value="<?php echo set_value('new_option[' . $k . ']'); ?>">
								<div class="input-group-append option-delete" style="cursor: pointer;">
									<button type="button" class="btn btn-danger"><i class="fas fa-times"></i></button>
								</div>
							</div>
							<?php echo form_error('new_option[' . $k . ']', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
							<?php endforeach; ?>
						</div>
						<?php else: ?>
						<div id="optionList" class="form-group">
							<label>Options</label>
							<?php foreach ($options as $k => $v): ?>
							<?php if ($k == 0 OR $k == 1): ?>
							<input type="text" class="form-control mb-2" name="option[<?php echo $v->id; ?>]" value="<?php echo set_value('option['. $v->id .']', $v->option); ?>">
							<?php else: ?>
							<div class="input-group mb-2">
								<input type="text" class="form-control" name="option[<?php echo $v->id; ?>]" value="<?php echo set_value('option['. $v->id .']', $v->option); ?>">
								<div class="input-group-append option-delete" style="cursor: pointer;">
									<button type="button" class="btn btn-danger"><i class="fas fa-times"></i></button>
								</div>
							</div>
							<?php endif; ?>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>

					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Question</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>

<div id="optionBody" style="display: none;">
	<div class="input-group mb-2">
		<input type="text" class="form-control" name="new_option[]">
		<div class="input-group-append option-delete">
			<button type="button" class="btn btn-danger"><i class="fas fa-times"></i></button>
		</div>
	</div>
</div>
