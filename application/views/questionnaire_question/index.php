
<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item"><a href="/questionnaire">Questionnaire</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<input type="hidden" name="questionnaire_id" value="<?php echo $questionnaire->id; ?>">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $questionnaire->title; ?></h3>

							<?php if ($sess_user->modules[$module->id]['create'] == 'yes'): ?>
							<div class="card-tools my-n1">
								<a class="btn btn-sm btn-primary" href="/questionnaire_question/add"><i class="fas fa-sm fa-plus-circle"></i> New Question</a>
							</div>
							<?php endif; ?>

						</div>
						<div class="card-body">
							<table id="dt-question" class="table table-bordered table-hover" style="width:100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Question</th>
										<th>Option Type</th>
										<th>Created At</th>
										<th>Updated At</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>ID</th>
										<th>Question</th>
										<th>Option Type</th>
										<th>Created At</th>
										<th>Updated At</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
