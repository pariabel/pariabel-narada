<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/teachers">Teachers</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<h4>Profile</h4>

						<div class="form-group">
							<label>NIP</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('nip') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="nip" placeholder="Enter NIP" value="<?php echo set_value('nip'); ?>">
							<?php echo form_error('nip', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Teacher name</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('name') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="name" placeholder="Enter teacher name" value="<?php echo set_value('name'); ?>">
							<?php echo form_error('name', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<hr>
						<h4>User Account</h4>

						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="username" placeholder="Enter username" value="<?php echo set_value('username'); ?>">
							<?php echo form_error('username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Password</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<button id="btnGenerateRandomPassword" type="button" class="btn btn-primary">Generate Random Password</button>
								</div>
								<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="password" placeholder="Enter password" value="<?php echo set_value('password'); ?>">
								<div class="input-group-append">
									<button id="toggleEye" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
								</div>
							</div>
							<?php echo form_error('password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create Teacher</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
