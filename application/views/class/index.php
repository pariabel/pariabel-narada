<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>

					<div class="card-tools my-n1">
						<a class="btn btn-sm btn-primary" href="/classes/add"><i class="fas fa-sm fa-plus-circle"></i> New Class</a>
					</div>
				</div>
				<div class="card-body">
					<div class="text-center">
						<div id="dropdownLevel" class="btn-group mb-2">
							<button type="button" class="btn btn-info px-5"><?php echo (isset($level)) ? $level->level : 'Choose School Level'; ?></button>
							<button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
								<span class="sr-only">Toggle Dropdown</span>
								<div class="dropdown-menu" role="menu">
									<?php foreach ($levels as $level): ?>
									<a class="dropdown-item" href="/classes/<?php echo url_title(strtolower($level->level)); ?>"><?php echo $level->level; ?></a>
									<?php endforeach; ?>
								</div>
							</button>
						</div>
					</div>

					<table id="dt-classes" class="table table-bordered table-hover nowrap" style="width:100%">
						<thead>
							<tr>
								<th>Class</th>
								<th>Sections</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($classes as $class): ?>
							<tr>
								<td><?php echo $class->class; ?></td>
								<?php if ($class->sections): ?>
								<td>
									<?php foreach ($class->sections as $section): ?>
									<?php echo $section->section; ?><br>
									<?php endforeach; ?>
								</td>
								<?php else: ?>
								<td></td>
								<?php endif; ?>
								<td><?php echo $class->buttons; ?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Class</th>
								<th>Sections</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
