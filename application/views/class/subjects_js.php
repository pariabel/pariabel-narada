<!-- Toastr -->
	<script src="/assets/plugins/toastr/toastr.min.js"></script>
	<?php if ($toastr !== ''): ?>
	<script>
	<?php echo sprintf("toastr.%s('%s', '%s');", $toastr['type'], $toastr['message'], $toastr['title']); ?>
	</script>
	<?php endif; ?>
