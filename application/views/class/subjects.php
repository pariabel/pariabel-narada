<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/classes">Class</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="class_id" value="<?php echo $class->id; ?>">
					<div class="card-body">
						<div class="row">
							<?php foreach ($subjects as $subject): ?>
							<div class="col-md-6 p-1">
								<div class="icheck-primary d-inline">
									<input type="checkbox" id="cbSubject<?php echo $subject->id; ?>" name="subject[<?php echo $subject->id; ?>]" value="1" <?php echo set_checkbox('subject[' . $subject->id . ']', '1', (isset($class->subjects[$subject->id]) && $class->subjects[$subject->id]['is_active'] == 'yes') ? TRUE : FALSE); ?>>
									<label for="cbSubject<?php echo $subject->id; ?>" class="font-weight-normal"><?php echo $subject->name; ?></label>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Subjects</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
