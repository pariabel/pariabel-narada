<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/classes">Class</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Level</label>
							<select name="level_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('level_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Level">
								<option value="">Select Level</option>
								<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>" <?php echo set_select('level_id', $level->id, ($level->id == $this->input->post('level_id')) ? TRUE : FALSE); ?>><?php echo $level->level; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('level_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Class</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('class') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="class" placeholder="Enter class" value="<?php echo set_value('class'); ?>">
							<?php echo form_error('class', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<div><label>Section</label></div>
							<?php foreach ($sections as $section): ?>
							<div class="icheck-primary d-inline pr-3">
								<input type="checkbox" id="cbSection<?php echo $section->section; ?>" name="section[<?php echo $section->id; ?>]" value="1">
								<label for="cbSection<?php echo $section->section; ?>"><?php echo $section->section; ?></label>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create Class</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
