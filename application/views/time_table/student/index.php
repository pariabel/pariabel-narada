<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $page_title; ?></h3>

							<?php if ($sess_user->modules[$module->id]['create'] == 'yes'): ?>
							<div class="card-tools my-n1">
								<a class="btn btn-sm btn-primary" href="/time_table/add"><i class="fas fa-sm fa-plus-circle"></i> New Time Table</a>
							</div>
							<?php endif; ?>

						</div>
						<div class="card-body">
							<table id="dt-timetable" class="table table-bordered table-hover" style="width:100%" data-order="[[1, &quot;desc&quot;]]">
								<thead>
									<tr>
										<th>Title</th>
										<th>Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Title</th>
										<th>Date</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
