<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/role">Role</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="role_id" value="<?php echo $role->id; ?>">
					<div class="card-body table-responsive">
						<table id="dt-permission" class="table table-bordered table-hover" style="width: 100%;">
							<thead>
								<tr>
									<th>Module</th>
									<th>Create</th>
									<th>Read</th>
									<th>Update</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($modules as $module): ?>
								<tr>
									<td><?php echo $module->name; ?></td>
									<td>
										<div class="icheck-primary d-inline">
											<input type="checkbox" id="cbCreate<?php echo $module->id; ?>" name="create[<?php echo $module->id; ?>]" value="1" <?php echo set_checkbox('create[' . $module->id . ']', '1', (isset($role_modules[$module->id]['create']) && $role_modules[$module->id]['create'] == 'yes') ? TRUE : FALSE); ?>>
											<label for="cbCreate<?php echo $module->id; ?>" class="font-weight-normal">Create</label>
										</div>
									</td>
									<td>
										<div class="icheck-primary d-inline">
											<input type="checkbox" id="cbRead<?php echo $module->id; ?>" name="read[<?php echo $module->id; ?>]" value="1" <?php echo set_checkbox('read[' . $module->id . ']', '1', (isset($role_modules[$module->id]['read']) && $role_modules[$module->id]['read'] == 'yes') ? TRUE : FALSE); ?>>
											<label for="cbRead<?php echo $module->id; ?>" class="font-weight-normal">Read</label>
										</div>
									</td>
									<td>
										<div class="icheck-primary d-inline">
											<input type="checkbox" id="cbUpdate<?php echo $module->id; ?>" name="update[<?php echo $module->id; ?>]" value="1" <?php echo set_checkbox('update[' . $module->id . ']', '1', (isset($role_modules[$module->id]['update']) && $role_modules[$module->id]['update'] == 'yes') ? TRUE : FALSE); ?>>
											<label for="cbUpdate<?php echo $module->id; ?>" class="font-weight-normal">Update</label>
										</div>
									</td>
									<td>
										<div class="icheck-primary d-inline">
											<input type="checkbox" id="cbDelete<?php echo $module->id; ?>" name="delete[<?php echo $module->id; ?>]" value="1" <?php echo set_checkbox('delete[' . $module->id . ']', '1', (isset($role_modules[$module->id]['delete']) && $role_modules[$module->id]['delete'] == 'yes') ? TRUE : FALSE); ?>>
											<label for="cbDelete<?php echo $module->id; ?>" class="font-weight-normal">Delete</label>
										</div>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Module</th>
									<th>Create</th>
									<th>Read</th>
									<th>Update</th>
									<th>Delete</th>
								</tr>
							</tfoot>
						</table>
					</div>

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Role Permission</button>
					</div>
				</form>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
