<!-- DataTables -->
	<script src="/assets/plugins/datatables/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
	<!-- Toastr -->
	<script src="/assets/plugins/toastr/toastr.min.js"></script>
	<!-- Page Script -->
	<script src="/assets/js/pages/role.permission.js"></script>
	<?php if ($toastr !== ''): ?>
	<script>
	<?php echo sprintf("toastr.%s('%s', '%s');", $toastr['type'], $toastr['message'], $toastr['title']); ?>
	</script>
	<?php endif; ?>
