<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $student->name; ?></h3>
						</div>
						<div class="card-body">
							<dl class="row">
								<dt class="col-sm-2">NIS.</dt>
								<dd class="col-sm-10"><?php echo $student->nis; ?></dd>
								<dt class="col-sm-2">Name</dt>
								<dd class="col-sm-10"><?php echo $student->name; ?></dd>
								<dt class="col-sm-2">Class</dt>
								<dd class="col-sm-10"><?php echo $student->class->class; ?><?php echo $student->section->section; ?></dd>
							</dl>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
