<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $page_title; ?></h3>
						</div>
						<div class="card-body">
							<table id="dt-assignment" class="table table-bordered table-hover" style="width:100%">
								<thead>
									<tr>
										<th>Subject</th>
										<th>Teacher</th>
										<th>Created Date</th>
										<th>Deadline Date</th>
										<th>Submission File</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Subject</th>
										<th>Teacher</th>
										<th>Created Date</th>
										<th>Deadline Date</th>
										<th>Submission File</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
