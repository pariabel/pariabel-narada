<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/assignments">Assignments</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post" enctype="multipart/form-data">
					<input type="hidden" name="selected_class_id" value="<?php echo set_value('selected_class_id'); ?>">
					<input type="hidden" name="selected_section_id" value="<?php echo set_value('selected_section_id'); ?>">
					<input type="hidden" name="selected_subject_id" value="<?php echo set_value('selected_subject_id'); ?>">
					<input type="hidden" name="checked_is_selected" value="<?php echo set_value('checked_is_selected'); ?>">
					<?php if ($this->input->post() && $this->input->post('is_selected') == 'yes'): ?>
					<?php foreach ( ($this->input->post('student_id')) ? $this->input->post('student_id') : array() as $k => $student_id): ?>
					<input type="hidden" name="selected_student_id[]" value="<?php echo $student_id; ?>">
					<?php endforeach; ?>
					<?php else: ?>
					<input type="hidden" name="selected_student_id[]">
					<?php endif; ?>
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h5><i class="<?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['body']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Class</label>
							<select name="class_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('class_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Class">
								<option value="">Select Class</option>
								<?php foreach($classes as $class): ?>
								<option value="<?php echo $class->id; ?>" <?php echo set_select('class_id', $class->id, ($class->id == $this->input->post('class_id')) ? TRUE : FALSE); ?>><?php echo $class->class; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('class_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Section</label>
							<select name="section_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('section_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Section" disabled>
								<option value="">Select Section</option>
							</select>
							<?php echo form_error('section_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Subject</label>
							<select name="subject_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('subject_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Subject" disabled>
								<option value="">Select Subject</option>
							</select>
							<?php echo form_error('subject_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div id="radioIsSelected" class="form-group" style="display:none">
							<label>Select Students</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="isSelectedNo" name="is_selected" value="no" <?php echo set_radio('is_selected', 'no', (($this->input->post() && $this->input->post('is_selected') == 'no') OR ! $this->input->post()) ? TRUE : FALSE); ?>>
								<label for="isSelectedNo" class="custom-control-label font-weight-normal">No</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="isSelectedYes" name="is_selected" value="yes" <?php echo set_radio('is_selected', 'yes', ($this->input->post() && $this->input->post('is_selected') == 'yes') ? TRUE : FALSE); ?>>
								<label for="isSelectedYes" class="custom-control-label font-weight-normal">Yes</label>
							</div>
						</div>

						<div id="duallistboxStudent" class="form-group" style="display:none">
							<select name="student_id[]" id="selectStudent" multiple="multiple">
							</select>
							<?php echo form_error('student_id[]', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Description</label>
							<textarea name="description" class="form-control<?php echo ($this->input->post()) ? ((form_error('description') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3"><?php echo set_value('description'); ?></textarea>
						</div>

						<div class="form-group">
							<label>Created Date</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="far fa-calendar-alt"></i>
									</span>
								</div>
								<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('created_date') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="created_date" placeholder="Enter created date" value="<?php echo set_value('created_date'); ?>">
								<?php echo form_error('created_date', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
							</div>
						</div>

						<div class="form-group">
							<label>Deadline Date</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="far fa-calendar-alt"></i>
									</span>
								</div>
								<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('submit_date') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="submit_date" placeholder="Enter submit date" value="<?php echo set_value('submit_date'); ?>">
								<?php echo form_error('submit_date', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
							</div>
						</div>

						<div class="form-group">
							<label>File</label>
							<div class="input-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input<?php echo ($this->input->post()) ? ((form_error('file') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="file">
									<label class="custom-file-label">Choose file</label>
								</div>
							</div>
							<small class="form-text text-muted">[.xls, .xlsx, .doc, .docx, .pdf] - Maximum upload file size: 2MB</small>
							<?php echo form_error('file', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create Assignment</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
