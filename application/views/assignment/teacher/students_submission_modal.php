<div class="modal fade" id="modalStudentsSubmission">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Students Submission</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">

				<div class="my-3">
					<table id="dtStudentsSubmission" class="table table-sm table-bordered table-hover" style="width: 100%" data-order="[[2, &quot;desc&quot;]]">
						<thead>
							<tr>
								<th>NIS.</th>
								<th>Name</th>
								<th>Submit Date</th>
								<th data-orderable="false">Submission File</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>NIS.</th>
								<th>Name</th>
								<th>Submit Date</th>
								<th data-orderable="false">Submission File</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>

			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
