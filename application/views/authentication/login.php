<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $page_title; ?> - <?php echo $this->config->item('site_name'); ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/toastr/toastr.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page" style="height: 80vh">
	<div class="login-box">
		<div class="login-logo">
			<!--
			<a href="javascript:;"><?php //echo $this->config->item('login_logo'); ?></a>
			-->
			<a href="javascript:;"><img src="<?php echo base_url(); ?>/assets/img/login-logo.png"></a>
			
		</div>
		<!-- /.login-logo -->

		<?php if (isset($errors) && $errors != ''): ?>
		<div class="alert alert-danger alert-dismissible" style="width: 100%;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h5><i class="icon fas fa-ban"></i> Log In Failed</h5>
			<small class=""><?php echo $errors; ?></small>
		</div>
		<?php endif; ?>

		<div class="card">
			<div class="card-body login-card-body">

				<p class="login-box-msg text-uppercase"><?php echo $msg; ?></p>

				<form action="" method="post">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
					<div class="input-group mb-3">
						<input type="text" name="username" class="form-control<?php echo ($this->input->post()) ? ((form_error('username') != '') ? ' is-invalid' : '') : ''; ?>" placeholder="Username">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-user"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="password" name="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('password') != '') ? ' is-invalid' : '') : ''; ?>" placeholder="Password">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-8">
							<!-- <div class="icheck-primary">
								<input type="checkbox" id="remember">
								<label for="remember">
									Remember Me
								</label>
							</div> -->
						</div>
						<!-- /.col -->
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block">LOG IN</button>
						</div>
						<!-- /.col -->
					</div>
				</form>
			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="<?php echo base_url();?>/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url();?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Toastr -->
	<script src="<?php echo base_url();?>/assets/plugins/toastr/toastr.min.js"></script>

	<script>
		$(document).ready(function(){
			$('input[name="username"]').focus();
			<?php if ($toastr != '' && $toastr['type'] == 'success'): ?>
			toastr.options.timeOut = 1000;
			toastr.options.positionClass = 'toast-top-full-width';
			<?php if ($toastr['title'] == 'Logged in successfully'): ?>
			var redirect = $('input[name="redirect"]').val();
			$('input[name="username"]').blur();
			$('.card').css('opacity', 0.5);
			$('button[type="submit"]').attr('disabled', true);
			toastr.options.onHidden = function() {
				location.href = redirect;
			};
			<?php endif; ?>
			toastr.success('<?php echo $toastr['message']; ?>', '<?php echo $toastr['title']; ?>');
			<?php endif; ?>
		});
	</script>

</body>
</html>
