<footer class="main-footer">
			<div class="float-right d-none d-sm-block">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2020 <a href="<?php echo $this->config->item('frontend_url'); ?>" target="new"><?php echo $this->config->item('site_name'); ?></a>.</strong> All rights
			reserved.
		</footer>
