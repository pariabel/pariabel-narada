<li class="nav-item">
	<a href="/dashboard" class="nav-link<?php echo (uri_string() == 'dashboard' || uri_string() == 'home') ? ' active' : ''; ?>">
		<i class="nav-icon fas fa-tachometer-alt"></i>
		<p>Dashboard</p>
	</a>
</li>
<li class="nav-item has-treeview<?php echo (preg_match("/^my_children/", uri_string())) ? ' menu-open' : ''; ?>" style="width: 100%;">
	<a href="javascript:;" class="nav-link<?php echo (preg_match("/^my_children/", uri_string())) ? ' active' : ''; ?>">
		<i class="nav-icon fas fa-users"></i>
		<p>
			My Children
			<i class="fas fa-angle-left right"></i>
		</p>
	</a>
	<ul class="nav nav-treeview">
		<?php foreach ($sess_user->children as $child): ?>
		<li class="nav-item" style="width: 100%;">
			<a href="/my_children/student/<?php echo $child->id; ?>" class="text-truncate nav-link<?php echo (uri_string() == 'my_children/student/' . $child->id) ? ' active': ''; ?>">
				<i class="far fa-circle nav-icon"></i>
				<?php echo $child->name; ?>
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</li>
<li class="nav-item">
	<a href="/announcements" class="nav-link<?php echo (uri_string() == 'announcements') ? ' active' : ''; ?>">
		<i class="nav-icon fas fa-bullhorn"></i>
		<p>
			Announcement
			<?php if (isset($sess_user->notifications['Announcement']) && $sess_user->notifications['Announcement'] != ''): ?>
			<span class="badge badge-info right"><?php echo $sess_user->notifications['Announcement']; ?></span>
			<?php endif; ?>
		</p>
	</a>
</li>
<li class="nav-item">
	<a href="/questionnaire" class="nav-link<?php echo (uri_string() == 'questionnaire') ? ' active' : ''; ?>">
		<i class="nav-icon far fa-list-alt"></i>
		<p>Questionnaire</p>
	</a>
</li>
<li class="nav-item">
	<a href="/school_calendar" class="nav-link<?php echo (uri_string() == 'school_calendar') ? ' active' : ''; ?>">
		<i class="nav-icon far fa-calendar-alt"></i>
		<p>School Calendar</p>
	</a>
</li>
<li class="nav-item">
	<a href="/time_table" class="nav-link<?php echo (uri_string() == 'time_table') ? ' active' : ''; ?>">
		<i class="nav-icon far fa-calendar-check"></i>
		<p>Time Table</p>
	</a>
</li>
<li class="nav-item has-treeview<?php echo (preg_match("/^assignment/", uri_string())) ? ' menu-open' : ''; ?>" style="width: 100%">
	<a href="javascript:;" class="nav-link<?php echo (preg_match("/^assignment/", uri_string())) ? ' active' : ''; ?>">
		<i class="nav-icon fas fa-book"></i>
		<p>
			Assignments
			<i class="fas fa-angle-left right"></i>
		</p>
	</a>
	<ul class="nav nav-treeview">
		<?php foreach ($sess_user->children as $child): ?>
		<li class="nav-item" style="width: 100%;">
			<a href="/assignment/student/<?php echo $child->id; ?>" class="text-truncate nav-link<?php echo (uri_string() == 'assignment/student/' . $child->id) ? ' active': ''; ?>">
				<i class="far fa-circle nav-icon"></i>
				<?php echo $child->name; ?>
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</li>
