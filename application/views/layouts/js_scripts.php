<!-- jQuery -->
	<script src="<?php echo base_url();?>/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url();?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<?php if (isset($page_scripts)): ?>
	<?php echo $page_scripts; ?>
	<?php endif; ?>
	<!-- pace-progress -->
	<script src="<?php echo base_url();?>/assets/plugins/pace-progress/pace.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="<?php echo base_url();?>/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url();?>/assets/js/adminlte.min.js"></script>
	<!-- Custom script -->
	<script src="<?php echo base_url();?>/assets/js/custom.js"></script>
