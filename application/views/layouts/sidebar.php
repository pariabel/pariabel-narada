<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Brand Logo -->
			<a href="/dashboard" class="brand-link">
				<img src="<?php echo base_url();?>/assets/img/login-logo.png"
				alt="AdminLTE Logo"
				class="brand-image img-circle elevation-3"
				style="opacity: .8">
				<span class="brand-text font-weight-light"><?php echo $this->config->item('site_name'); ?></span>
			</a>

			<!-- Sidebar -->
			<div class="sidebar">

				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

						<?php if ($sess_user->role->name == 'Parents'): ?>
						<?php echo $this->load->view('layouts/sidebar_parents', array('sess_user' => $sess_user), TRUE); ?>
						<?php else: ?>
						<?php foreach ($sidebar_menu as $nav): ?>
						<li class="nav-item<?php echo ($nav['has_treeview']) ? ' has-treeview' : ''; ?><?php echo ($nav['menu_open']) ? ' menu-open' : ''; ?>">
							<a href="<?php echo base_url().$nav['uri']; ?>" class="nav-link<?php echo $nav['active']; ?>">
								<i class="<?php echo $nav['icon']; ?> nav-icon"></i>
								<p>
									<?php echo $nav['name'] ?>
									<?php if ($nav['has_treeview']): ?>
									<i class="fas fa-angle-left right"></i>
									<?php endif; ?>
								</p>
							</a>
							<?php if ($nav['has_treeview']): ?>
							<ul class="nav nav-treeview">
								<?php foreach ($nav['childs'] as $navc): ?>
								<li class="nav-item">
                                                                    <a href="<?php echo base_url().$navc['uri']; ?>" class="nav-link<?php echo $navc['active']; ?>">
										<i class="<?php echo $navc['icon']; ?> nav-icon"></i>
										<p><?php echo htmlspecialchars($navc['name']); ?></p>
									</a>
								</li>
								<?php endforeach; ?>
							</ul>
							<?php endif; ?>
						</li>
						<?php endforeach; ?>
						<?php endif; ?>

					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>
