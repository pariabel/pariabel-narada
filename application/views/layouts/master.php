<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $page_title; ?> - <?php echo $this->config->item('site_name'); ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<?php if (isset($page_styles)): ?>
	<?php echo $page_styles; ?>
	<?php endif; ?>
	<!-- pace-progress -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/pace-progress/themes/blue/pace-theme-flat-top.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/custom.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
	<!-- Site wrapper -->
	<div class="wrapper">

		<!-- Navbar -->
		<?php echo $navbar; ?>
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		<?php echo $sidebar; ?>

		<!-- Content Wrapper. Contains page content -->
		<?php echo $content; ?>
		<!-- /.content-wrapper -->

		<?php echo $footer; ?>

		<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
			<i class="fas fa-chevron-up"></i>
		</a>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Control sidebar content goes here -->
		</aside>
		<!-- /.control-sidebar -->
	</div>
	<!-- ./wrapper -->

	<?php echo $js_scripts; ?>
</body>
</html>
