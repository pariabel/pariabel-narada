<div class="modal fade" id="modalUser">
			<div class="modal-dialog">
				<div class="modal-content">

					<div class="modal-header">
						<h4 class="modal-title">User</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">

						<div class="alert alert-dismissible" style="display: none;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5></h5>
							<span></span>
						</div>

						<div id="userInfo" style="display: none;">
							<dl class="row">
								<dt class="col-sm-4">Role</dt>
								<dd class="col-sm-8" id="userRole"></dd>
								<dt class="col-sm-4">Name</dt>
								<dd class="col-sm-8" id="userName"></dd>
								<dt class="col-sm-4">Username</dt>
								<dd class="col-sm-8" id="userUsername"></dd>
								<dt class="col-sm-4">Register At</dt>
								<dd class="col-sm-8" id="userRegisterAt"></dd>
							</dl>
						</div>

					</div>

					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
