<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $page_title; ?></h3>
							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
							</div>
						</div>
						<div class="card-body" style="overflow: auto;">
							<table id="dt-loghistory" class="table table-bordered table-hover" style="width:100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Created At</th>
										<th>Affected Table</th>
										<th>Operation</th>
										<th data-orderable="false">User</th>
										<th data-orderable="false">New Data</th>
										<th data-orderable="false">Old Data</th>
										<th data-orderable="false">Where Clause</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>ID</th>
										<th>Created At</th>
										<th>Affected Table</th>
										<th>Operation</th>
										<th data-orderable="false">User</th>
										<th data-orderable="false">New Data</th>
										<th data-orderable="false">Old Data</th>
										<th data-orderable="false">Where Clause</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>

		<?php echo $this->load->view('log_history/user_modal', '', TRUE); ?>
