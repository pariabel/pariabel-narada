<!-- DataTables -->
	<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<!-- Page Script -->
	<script src="/assets/js/pages/log-history.index.js"></script>
