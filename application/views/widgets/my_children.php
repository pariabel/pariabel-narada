<h5>My Children</h5>
<?php foreach ($children as $student): ?>
<!-- Default box -->
<div class="card">
	<div class="card-header">
		<h3 class="card-title"><?php echo $student->name; ?></h3>
	</div>
	<div class="card-body">
		<dl class="row">
			<dt class="col-sm-2">NIS.</dt>
			<dd class="col-sm-10"><?php echo $student->nis; ?></dd>
			<dt class="col-sm-2">Name</dt>
			<dd class="col-sm-10"><?php echo $student->name; ?></dd>
			<dt class="col-sm-2">Class</dt>
			<dd class="col-sm-10"><?php echo $student->class->class; ?><?php echo $student->section->section; ?></dd>
		</dl>
	</div>
	<!-- /.card-body -->
</div>
<!-- /.card -->
<?php endforeach; ?>
