<div class="card card-primary card-outline card-outline-tabs">
	<div class="card-header p-0 border-bottom-0">
		<ul class="nav nav-tabs" role="tablist">
			<?php foreach ($tabs as $tab): ?>
			<li class="nav-item">
				<a class="nav-link<?php echo $tab['is_active']; ?>" id="<?php echo $tab['id']; ?>" data-toggle="pill" href="<?php echo $tab['href']; ?>" role="tab"><?php echo $tab['text']; ?></a>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<div class="card-body">
		<div class="tab-content">
			<?php foreach ($tabs as $tab): ?>
			<div class="tab-pane fade show<?php echo $tab['is_active']; ?>" id="<?php echo $tab['content_id']; ?>" role="tabpanel">
			<?php echo $tab['content_body']; ?>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<!-- /.card -->
</div>
