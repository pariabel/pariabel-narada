<!-- Small boxes (Stat box) -->
<div class="row">
	<?php foreach ($stat_boxs as $stat_box): ?>
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box <?php echo $stat_box['bg']; ?>">
			<div class="inner">
				<h3><?php echo $stat_box['count']; ?></h3>
				<p><?php echo $stat_box['title']; ?></p>
			</div>
			<div class="icon">
				<i class="<?php echo $stat_box['icon']; ?>"></i>
			</div>
			<a href="<?php echo $stat_box['url']; ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<?php endforeach; ?>
</div>
<!-- /.row -->
