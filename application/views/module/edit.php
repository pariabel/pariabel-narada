<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/modules">Modules</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $module->id; ?>">

					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('name') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="name" value="<?php echo set_value('name', $module->name); ?>">
							<?php echo form_error('name', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Controller</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('controller') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="controller" placeholder="Enter controller" value="<?php echo set_value('controller', $module->controller); ?>">
							<?php echo form_error('controller', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Is Active</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="activeYes" name="is_active" value="yes" <?php echo set_radio('is_active', 'yes', ($module->is_active == 'yes') ? TRUE : FALSE); ?>>
								<label for="activeYes" class="custom-control-label font-weight-normal">Yes</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="activeNo" name="is_active" value="no" <?php echo set_radio('is_active', 'no', ($module->is_active == 'no') ? TRUE : FALSE); ?>>
								<label for="activeNo" class="custom-control-label font-weight-normal">No</label>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Module</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
