<!-- Page Script -->
<script src="<?php echo base_url();?>/assets/js/pages/dashboard.index.js"></script>
<?php if ($toastr !== ''): ?>
<!-- Toastr -->
<script src="<?php echo base_url();?>/assets/plugins/toastr/toastr.min.js"></script>
<script>
toastr.options.positionClass = 'toast-top-full-width';
<?php if ($toastr['title'] == 'Change Password'): ?>
	<?php if ($toastr['type'] == 'success'): ?>
	$('#changePasswordSuccess').show();
	$('form').css('opacity', 0.3);
	$('button[type="submit"]').attr('disabled', true);
	setTimeout(function(){
		location.href = '/logout';
	}, 5000);
	<?php endif; ?>
<?php endif; ?>
<?php echo sprintf("toastr.%s('%s', '%s');", $toastr['type'], $toastr['message'], $toastr['title']); ?>
</script>
<?php endif; ?>
