<div class="modal fade" id="modalParents">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<div class="modal-header">
						<h4 class="modal-title">Parents</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">

						<form id="formSearchParents" role="form" action="" method="post">
							<div class="form-row justify-content-center">
								<div class="col-sm-9 my-1">
									<label class="sr-only">Query</label>
									<input type="text" name="q" class="form-control" placeholder="Father or Mother">
								</div>
								<div class="col-sm-3 my-1">
									<button type="submit" class="btn btn-block btn-primary" id="btnSearch"><i class="fas fa-search"></i> Search</button>
								</div>
							</div>
						</form>

						<div class="my-3">
							<table id="dtParents" class="table table-sm table-bordered table-hover d-none" style="width: 100%">
								<thead>
									<tr>
										<th>Father</th>
										<th>Mother</th>
										<th>Address</th>
										<th>Choose</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Father</th>
										<th>Mother</th>
										<th>Address</th>
										<th>Choose</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>

					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
