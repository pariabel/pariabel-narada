<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/student/list">Students</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<h5>Student</h5>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>NIS.</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('nis') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="nis" placeholder="Enter NIS" value="<?php echo set_value('nis'); ?>">
									<?php echo form_error('nis', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Student name</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('name') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="name" placeholder="Enter student name" value="<?php echo set_value('name'); ?>">
									<?php echo form_error('name', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Class</label>
									<select name="class_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('class_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Class">
										<option value="">Select Class</option>
										<?php foreach($classes as $class): ?>
										<option value="<?php echo $class->id; ?>" <?php echo set_select('class_id', $class->id, ($class->id == $this->input->post('class_id')) ? TRUE : FALSE); ?>><?php echo $class->class; ?></option>
										<?php endforeach; ?>
									</select>
									<?php echo form_error('class_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Section</label>
									<select name="section_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('section_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Section"<?php echo ( ! isset($sections)) ? ' disabled' : ''; ?>>
										<option value="">Select Section</option>
										<?php foreach($sections as $section): ?>
										<option value="<?php echo $section->id; ?>" <?php echo set_select('section_id', $section->id, ($section->id == $this->input->post('section_id')) ? TRUE : FALSE); ?>><?php echo $section->section; ?></option>
										<?php endforeach; ?>
									</select>
									<?php echo form_error('section_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('student_username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="student_username" placeholder="Enter username" value="<?php echo set_value('student_username'); ?>">
									<?php echo form_error('student_username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<button id="btnStudentRandomPassword" type="button" class="btn btn-primary">Random Password</button>
										</div>
										<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('student_password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="student_password" placeholder="Enter password" value="<?php echo set_value('student_password'); ?>">
										<div class="input-group-append">
											<button id="toggleEyeStudent" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
										</div>
									</div>
									<?php echo form_error('student_password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<hr>
						<h5>Parents</h5>

						<div class="form-group">
							<label>Parents Information</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="parentsInfoNew" name="parents_info" value="new" <?php echo set_radio('parents_info', 'new', TRUE); ?>>
								<label for="parentsInfoNew" class="custom-control-label font-weight-normal">New</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="parentsInfoExisting" name="parents_info" value="existing" <?php echo set_radio('parents_info', 'existing'); ?>>
								<label for="parentsInfoExisting" class="custom-control-label font-weight-normal">Existing</label>
							</div>
						</div>

						<input type="hidden" name="parents_id" value="0">

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Father</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('father') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="father" placeholder="Enter father" value="<?php echo set_value('father'); ?>"<?php echo ($this->input->post('parents_info') == 'existing') ? ' readonly' : ''; ?>>
									<?php echo form_error('father', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Father Email</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('father_email') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="father_email" placeholder="Enter father email" value="<?php echo set_value('father_email'); ?>"<?php echo ($this->input->post('parents_info') == 'existing') ? ' readonly' : ''; ?>>
									<?php echo form_error('father_email', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Mother</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('mother') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="mother" placeholder="Enter mother" value="<?php echo set_value('mother'); ?>"<?php echo ($this->input->post('parents_info') == 'existing') ? ' readonly' : ''; ?>>
									<?php echo form_error('mother', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Mother Email</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('mother_email') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="mother_email" placeholder="Enter mother email" value="<?php echo set_value('mother_email'); ?>"<?php echo ($this->input->post('parents_info') == 'existing') ? ' readonly' : ''; ?>>
									<?php echo form_error('mother_email', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Address</label>
							<textarea class="form-control<?php echo ($this->input->post()) ? ((form_error('address') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3" name="address" placeholder="Enter address"<?php echo ($this->input->post('parents_info') == 'existing') ? ' readonly' : ''; ?>><?php echo set_value('address'); ?></textarea>
							<?php echo form_error('address', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div id="dParentsUser" class="row"<?php echo ($this->input->post('parents_info') == 'existing') ? ' style="display: none"' : ''; ?>>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('parents_username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="parents_username" placeholder="Enter username" value="<?php echo set_value('parents_username'); ?>">
									<?php echo form_error('parents_username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<button id="btnParentsRandomPassword" type="button" class="btn btn-primary">Random Password</button>
										</div>
										<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('parents_password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="parents_password" placeholder="Enter password" value="<?php echo set_value('parents_password'); ?>">
										<div class="input-group-append">
											<button id="toggleEyeParents" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
										</div>
									</div>
									<?php echo form_error('parents_password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create Student</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>

<?php echo $this->load->view('student/parents_existing_modal', '', TRUE); ?>
