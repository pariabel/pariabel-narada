<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">

					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Select Criteria</h3>
							<?php if ($sess_user->modules[$module->id]['create'] == 'yes'): ?>
							<div class="card-tools my-n1">
								<a class="btn btn-sm btn-primary" href="/student/add"><i class="fas fa-sm fa-plus-circle"></i> New Student</a>
							</div>
							<?php endif; ?>
						</div>

						<!-- form start -->
						<form role="form" action="" method="post">
							<div class="card-body">
								<div class="form-row align-items-center">
									<div class="col-sm-6 my-1">
										<label>Class</label>
										<select class="custom-select" name="class_id">
											<option value="">Select Class</option>
											<?php foreach ($classes as $class): ?>
											<option value="<?php echo $class->id; ?>" <?php echo set_select('class_id', $class->id, ($this->session->userdata('students_class_id') && $this->session->userdata('students_class_id') == $class->id) ? TRUE : FALSE); ?>><?php echo $class->class; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="col-sm-6 my-1">
										<label>Section</label>
										<select class="custom-select" name="section_id" disabled>
											<option value="">Select Section</option>
											<?php foreach ( (isset($sections)) ? $sections : array() as $section): ?>
											<option value="<?php echo $section->id; ?>" <?php echo set_select('section_id', $section->id, ($this->session->userdata('students_section_id') && $this->session->userdata('students_section_id') == $section->id) ? TRUE : FALSE); ?>><?php echo $section->section; ?></option>
											<?php endforeach; ?>
										</select>
									</div>

								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
								<button type="submit" class="btn btn-primary" name="search" value="yes" disabled><i class="fas fa-search"></i> Search</button>
							</div>
						</form>
					</div>
					<!-- /.card -->

					<!-- Default box -->
					<div id="cardStudents" class="card d-none">
						<div class="card-header">
							<h3 class="card-title"><?php echo $page_title; ?></h3>
						</div>
						<div class="card-body">
							<table id="dt-student" class="table table-bordered table-hover" style="width:100%">
								<thead>
									<tr>
										<th>NIS.</th>
										<th>Name</th>
										<th>Is Active</th>
										<th>Created At</th>
										<th>Updated At</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>NIS.</th>
										<th>Name</th>
										<th>Is Active</th>
										<th>Created At</th>
										<th>Updated At</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
