<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/student/list">Students</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $student->id; ?>">
					<input type="hidden" name="student_user_id" value="<?php echo $student_user->id; ?>">
					<input type="hidden" name="parents_id" value="<?php echo $parents->id; ?>">
					<input type="hidden" name="saved_parents_id" value="<?php echo $parents->id; ?>">
					<input type="hidden" name="parents_user_id" value="<?php echo $parents_user->id; ?>">

					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<h5>Student</h5>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>NIS.</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('nis') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="nis" placeholder="Enter NIS" value="<?php echo set_value('nis', $student->nis); ?>">
									<?php echo form_error('nis', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Student name</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('name') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="name" placeholder="Enter student name" value="<?php echo set_value('name', $student->name); ?>">
									<?php echo form_error('name', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Class</label>
									<select name="class_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('class_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Class">
										<option value="">Select Class</option>
										<?php foreach($classes as $class): ?>
										<option value="<?php echo $class->id; ?>" <?php echo set_select('class_id', $class->id, ($class->id == $student_session->class_id) ? TRUE : FALSE); ?>><?php echo $class->class; ?></option>
										<?php endforeach; ?>
									</select>
									<?php echo form_error('class_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Section</label>
									<select name="section_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('section_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Section">
										<option value="">Select Section</option>
										<?php foreach($sections as $section): ?>
										<option value="<?php echo $section->id; ?>" <?php echo set_select('section_id', $section->id, ($section->id == $student_session->section_id) ? TRUE : FALSE); ?>><?php echo $section->section; ?></option>
										<?php endforeach; ?>
									</select>
									<?php echo form_error('section_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('student_username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="student_username" placeholder="Enter username" value="<?php echo set_value('student_username', $student_user->username); ?>">
									<?php echo form_error('student_username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<button id="btnStudentRandomPassword" type="button" class="btn btn-primary">Random Password</button>
										</div>
										<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('student_password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="student_password" placeholder="Enter password" value="<?php echo set_value('student_password'); ?>">
										<div class="input-group-append">
											<button id="toggleEyeStudent" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
										</div>
									</div>
									<?php echo form_error('student_password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Is Active</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="activeYes" name="is_active" value="yes" <?php echo set_radio('is_active', 'yes', ($parents->is_active == 'yes') ? TRUE : FALSE); ?>>
								<label for="activeYes" class="custom-control-label font-weight-normal">Yes</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="activeNo" name="is_active" value="no" <?php echo set_radio('is_active', 'no', ($parents->is_active == 'no') ? TRUE : FALSE); ?>>
								<label for="activeNo" class="custom-control-label font-weight-normal">No</label>
							</div>
						</div>

						<hr>
						<h5>Parents</h5>

						<div class="form-group">
							<label>No Changes</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="noChangesYes" name="no_changes" value="yes" <?php echo set_radio('no_changes', 'yes', TRUE); ?>>
								<label for="noChangesYes" class="custom-control-label font-weight-normal">Yes</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="noChangesNo" name="no_changes" value="no" <?php echo set_radio('no_changes', 'no'); ?>>
								<label for="noChangesNo" class="custom-control-label font-weight-normal">No</label>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Father</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('father') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="father" placeholder="Enter father" value="<?php echo set_value('father', $parents->father); ?>">
									<input type="hidden" name="saved_father" value="<?php echo $parents->father; ?>">
									<?php echo form_error('father', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Father Email</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('father_email') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="father_email" placeholder="Enter father email" value="<?php echo set_value('father_email', $parents->father_email); ?>">
									<input type="hidden" name="saved_father_email" value="<?php echo $parents->father_email; ?>">
									<?php echo form_error('father_email', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Mother</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('mother') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="mother" placeholder="Enter mother" value="<?php echo set_value('mother', $parents->mother); ?>">
									<input type="hidden" name="saved_mother" value="<?php echo $parents->mother; ?>">
									<?php echo form_error('mother', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Mother Email</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('mother_email') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="mother_email" placeholder="Enter mother email" value="<?php echo set_value('mother_email', $parents->mother_email); ?>">
									<input type="hidden" name="saved_mother_email" value="<?php echo $parents->mother_email; ?>">
									<?php echo form_error('mother_email', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Address</label>
							<textarea class="form-control<?php echo ($this->input->post()) ? ((form_error('address') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3" name="address" placeholder="Enter address"><?php echo set_value('address', $parents->address); ?></textarea>
							<input type="hidden" name="saved_address" value="<?php echo $parents->address; ?>">
							<?php echo form_error('address', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div id="dParentsUser" class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('parents_username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="parents_username" placeholder="Enter username" value="<?php echo set_value('parents_username', $parents_user->username); ?>">
									<input type="hidden" name="saved_parents_username" value="<?php echo $parents_user->username; ?>">
									<?php echo form_error('parents_username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<button id="btnParentsRandomPassword" type="button" class="btn btn-primary">Random Password</button>
										</div>
										<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('parents_password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="parents_password" placeholder="Enter password" value="<?php echo set_value('parents_password'); ?>">
										<div class="input-group-append">
											<button id="toggleEyeParents" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
										</div>
									</div>
									<?php echo form_error('parents_password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Student</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>

<?php echo $this->load->view('student/parents_existing_modal', '', TRUE); ?>
