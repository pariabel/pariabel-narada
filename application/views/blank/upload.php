<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/school_calendar">School Calendar</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card">
			<div class="card-header">
				<h3 class="card-title"><?php echo $page_title; ?></h3>
			</div>
			<!-- form start -->
			<form role="form" action="" method="post" enctype="multipart/form-data">
				<div class="card-body">
					<?php if (isset($alert)): ?>
					<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="<?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
						<?php echo $alert['body']; ?>
					</div>
					<?php endif; ?>

					<div class="form-group">
						<label>File</label>
						<div class="input-group">
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="file">
								<label class="custom-file-label">Choose file</label>
							</div>
						</div>
						<?php echo form_error('file', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
					</div>
				</div>
				<!-- /.card-body -->

				<div class="card-footer">
					<button type="submit" class="btn btn-primary">Upload</button>
				</div>
			</form>
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
