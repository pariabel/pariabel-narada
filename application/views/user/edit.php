<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/users">Users</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $user->id; ?>">

					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="username" placeholder="Enter username" value="<?php echo set_value('username', $user->username); ?>">
							<?php echo form_error('username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
						<div class="form-group">
						<label>Password</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<button id="btnGenerateRandomPassword" type="button" class="btn btn-primary">Generate Random Password</button>
								</div>
								<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="password" placeholder="Enter password" value="<?php echo set_value('password'); ?>">
								<div class="input-group-append">
									<button id="toggleEye" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
								</div>
							</div>
							<?php echo form_error('password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
						<div class="form-group">
							<label>Role</label>
							<select name="role_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('role_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Role">
								<option value="">Select Role</option>
								<?php foreach ($roles as $role): ?>
								<option value="<?php echo $role->id; ?>" <?php echo set_select('role_id', $role->id, ($role->id == $user->role->role_id) ? TRUE : FALSE); ?>><?php echo $role->name; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('role_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
						<div class="form-group">
							<label>Active</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="activeYes" name="is_active" value="yes" <?php echo set_radio('is_active', 'yes', ($user->is_active == 'yes') ? TRUE : FALSE); ?>>
								<label for="activeYes" class="custom-control-label font-weight-normal">Yes</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="activeNo" name="is_active" value="no" <?php echo set_radio('is_active', 'no', ($user->is_active == 'no') ? TRUE : FALSE); ?>>
								<label for="activeNo" class="custom-control-label font-weight-normal">No</label>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update User</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
