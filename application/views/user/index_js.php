<!-- DataTables -->
	<script src="<?php echo base_url();?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url();?>/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url();?>/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<!-- SweetAlert2 -->
	<script src="<?php echo base_url();?>/assets/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="<?php echo base_url();?>/assets/plugins/toastr/toastr.min.js"></script>
	<!-- Page Script -->
	<script src="<?php echo base_url();?>/assets/js/pages/user.index.js"></script>
	<?php if ($toastr !== ''): ?>
	<script>
	<?php echo sprintf("toastr.%s('%s', '%s');", $toastr['type'], $toastr['message'], $toastr['title']); ?>
	</script>
	<?php endif; ?>
