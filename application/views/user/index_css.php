<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/datatables-responsive/css/responsive.bootstrap4.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Toastr -->
<link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/toastr/toastr.min.css">
