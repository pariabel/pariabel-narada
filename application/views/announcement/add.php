<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/announcements">Announcements</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Level</label>
							<select name="level_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('level_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Level">
								<option value="">Select Level</option>
								<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>" <?php echo set_select('level_id', $level->id, ($level->id == $this->input->post('level_id')) ? TRUE : FALSE); ?>><?php echo $level->level; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('level_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('title') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="title" placeholder="Enter title" value="<?php echo set_value('title'); ?>">
							<?php echo form_error('title', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Body</label>
							<textarea class="form-control<?php echo ($this->input->post()) ? ((form_error('body') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3" id="bodyTextarea" name="body" placeholder="Enter body"><?php echo set_value('body'); ?></textarea>
							<?php echo form_error('body', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create Announcement</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
