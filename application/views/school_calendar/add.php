<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/school_calendar">School Calendar</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post" enctype="multipart/form-data">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h5><i class="<?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['body']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Level</label>
							<select name="level_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('level_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Level">
								<option value="">Select Level</option>
								<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>" <?php echo set_select('level_id', $level->id, ($level->id == $this->input->post('level_id')) ? TRUE : FALSE); ?>><?php echo $level->level; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('level_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('title') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="title" placeholder="Enter title" value="<?php echo set_value('title'); ?>">
							<?php echo form_error('title', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>File</label>
							<div class="input-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input <?php echo ($this->input->post()) ? ((form_error('file') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="file">
									<label class="custom-file-label">Choose file</label>
								</div>
							</div>
							<small class="form-text text-muted">[.xls, .xlsx, .doc, .docx, .pdf] - Maximum upload file size: 2MB</small>
							<?php echo form_error('file', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create School Calendar</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
