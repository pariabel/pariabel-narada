<!-- DataTables -->
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<!-- Toastr -->
	<script src="/assets/plugins/toastr/toastr.min.js"></script>
	<!-- Select2 -->
	<script src="/assets/plugins/select2/js/select2.full.min.js"></script>
	<!-- Page Script -->
	<script src="/assets/js/pages/assign-subjects.index.js"></script>
	<?php if ($toastr !== ''): ?>
	<script>
	<?php echo sprintf("toastr.%s('%s', '%s');", $toastr['type'], $toastr['message'], $toastr['title']); ?>
	</script>
	<?php endif; ?>
