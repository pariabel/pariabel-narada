<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/classes">Class</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Select Criteria</h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<div class="card-body">
						<div class="form-row align-items-center">
							<div class="col-sm-6 my-1">
								<label>Class</label>
								<select class="custom-select" name="class_id">
									<option value="">Select Class</option>
									<?php foreach ($classes as $class): ?>
									<option value="<?php echo $class->id; ?>" <?php echo set_select('class_id', $class->id, (isset($selected_class_id) && $selected_class_id == $class->id) ? TRUE : FALSE); ?>><?php echo $class->class; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-6 my-1">
								<label>Section</label>
								<select class="custom-select" name="section_id" disabled>
									<option value="">Select Section</option>
									<?php foreach ( (isset($sections)) ? $sections : array() as $section): ?>
									<option value="<?php echo $section->id; ?>" <?php echo set_select('section_id', $section->id, (isset($selected_section_id) && $selected_section_id == $section->id) ? TRUE : FALSE); ?>><?php echo $section->section; ?></option>
									<?php endforeach; ?>
								</select>
							</div>

						</div>
					</div>
					<!-- /.card-body -->
					<div class="card-footer">
						<button type="submit" class="btn btn-primary" name="search" value="yes" disabled><i class="fas fa-search"></i> Search</button>
					</div>
				</form>
			</div>
			<!-- /.card -->

			<!-- Default box -->
			<div id="cardSubjects" class="card" style="display: none;">
				<div class="card-header">
					<h3 class="card-title">Subjects</h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="selected_class_id" value="<?php echo set_value('selected_class_id', (isset($selected_class_id)) ? $selected_class_id : ''); ?>">
					<input type="hidden" name="selected_section_id" value="<?php echo set_value('selected_section_id', (isset($selected_section_id)) ? $selected_section_id : ''); ?>">

					<div class="card-body">
						<table id="dt-subjects" class="table table-bordered table-hover text-nowrap" style="width:100%">
							<thead>
								<tr>
									<th>Subject</th>
									<th>Teacher</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ( (isset($subjects)) ? $subjects : array() as $subject): ?>
							<tr>
								<td><?php echo $subject->name; ?></td>
								<td>
									<select class="form-control select2bs4" style="width: 100%;" name="teacher[<?php echo $subject->id; ?>]">
										<option value="0">Select Teacher</option>
										<?php foreach ( (isset($teachers)) ? $teachers : array() as $teacher): ?>
										<option value="<?php echo $teacher->id; ?>" <?php echo set_select('teacher[' . $subject->id . ']', $teacher->id, (isset($subject_teacher[$subject->id]) && $subject_teacher[$subject->id] == $teacher->id) ? TRUE : FALSE); ?>><?php echo $teacher->name; ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Subject</th>
									<th>Teacher</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary" name="update" value="yes">Assign Subjects</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
