<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/homework_quiz">Homework Quiz</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $homework_quiz->id; ?>">
					<input type="hidden" name="file_id" value="<?php echo $homework_quiz->file_id; ?>">
					<input type="hidden" name="selected_section_id" value="<?php echo $homework_quiz->section_id; ?>">
					<input type="hidden" name="selected_subject_id" value="<?php echo $homework_quiz->subject_id; ?>">

					<div class="card-body">
						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h5><i class="<?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['body']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Class</label>
							<select name="class_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('class_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Class">
								<option value="">Select Class</option>
								<?php foreach($classes as $class): ?>
								<option value="<?php echo $class->id; ?>" <?php echo set_select('class_id', $class->id, ($class->id == $homework_quiz->class_id) ? TRUE : FALSE); ?>><?php echo $class->class; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('class_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Section</label>
							<select name="section_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('section_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Section">
								<option value="">Select Section</option>
							</select>
							<?php echo form_error('section_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Subject</label>
							<select name="subject_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('subject_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Subject">
								<option value="">Select Subject</option>
							</select>
							<?php echo form_error('subject_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Description</label>
							<textarea name="description" class="form-control<?php echo ($this->input->post()) ? ((form_error('description') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3"><?php echo set_value('description', $homework_quiz->description); ?></textarea>
						</div>

						<div class="form-group">
							<label>Created Date</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('created_date') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="created_date" placeholder="Enter created date" value="<?php echo set_value('created_date', $homework_quiz->created_date); ?>">
							<?php echo form_error('created_date', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Submit Date</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('submit_date') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="submit_date" placeholder="Enter submit date" value="<?php echo set_value('submit_date', $homework_quiz->submit_date); ?>">
							<?php echo form_error('submit_date', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Uploaded File</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="<?php echo $this->config->item('file_extension_fa_icon')[$uploaded_file->file_ext]; ?>"></i></span>
								</div>
								<input type="text" class="form-control" name="file_name" value="<?php echo set_value('file_name', $uploaded_file->orig_name); ?>" readonly>
							</div>
						</div>

						<div class="form-group">
							<label>File</label>
							<div class="input-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input<?php echo ($this->input->post()) ? ((form_error('file') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="file">
									<label class="custom-file-label">Choose file</label>
								</div>
							</div>
							<?php echo form_error('file', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Homework Quiz</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
