<div class="modal fade" id="modalDetail">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form role="form" id="formSubmission" action="" method="post" enctype="multipart/form-data">
						<input type="hidden" name="homework_quiz_id">
						<div class="modal-header">
							<h4 class="modal-title">Homework Quiz</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<dl class="row">
								<dt class="col-sm-4">Subject</dt>
								<dd class="col-sm-8" id="hqSubject"></dd>
								<dt class="col-sm-4">Description</dt>
								<dd class="col-sm-8" id="hqDescription"></dd>
								<dt class="col-sm-4">Teacher</dt>
								<dd class="col-sm-8" id="hqTeacher"></dd>
								<dt class="col-sm-4">Created Date</dt>
								<dd class="col-sm-8" id="hqCreatedDate"></dd>
								<dt class="col-sm-4">Submit Date</dt>
								<dd class="col-sm-8" id="hqSubmitDate"></dd>
								<dt class="col-sm-4">Submission File</dt>
								<dd class="col-sm-8" id="hqSubmissionFile">-</dd>
								<dt class="col-sm-4">Submission Date</dt>
								<dd class="col-sm-8" id="hqSubmissionDate">-</dd>
							</dl>

							<div id="inputUploadSubmissionFile" class="form-group">
								<label>Upload File</label>
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="file">
										<label class="custom-file-label">Choose file</label>
									</div>
								</div>
								<small class="form-text text-muted">Maximum upload file size: 2MB</small>
								<?php echo form_error('file', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
							</div>

						</div>
						<div class="modal-footer justify-content-between">
							<button type="submit" class="btn btn-primary" id="btnSubmission">Submission</button>
							<button type="reset" class="btn btn-default" id="btnReset">Reset</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
