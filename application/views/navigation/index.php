<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $page_title; ?></h3>

							<?php if ($sess_user->modules[$module->id]['create'] == 'yes'): ?>
							<div class="card-tools my-sm-n1">
								<button class="btn btn-sm btn-primary mr-1" id="btnUpdateSequence"><i class="fas fa-sm fa-list-alt"></i> Update Sequence</button>
								<a class="btn btn-sm btn-primary" href="/navigation/add"><i class="fas fa-sm fa-plus-circle"></i> New Navigation</a>
							</div>
							<?php endif; ?>

						</div>
						<div class="card-body">
							<form role="form" action="" method="post">
							<table id="dt-navigation" class="table table-bordered table-hover" style="width:100%">
								<thead>
									<tr>
										<th>Sequence #</th>
										<th>Name</th>
										<th>Icon</th>
										<th>URI</th>
										<th>Childs</th>
										<th>Updated At</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Sequence #</th>
										<th>Name</th>
										<th>Icon</th>
										<th>URI</th>
										<th>Childs</th>
										<th>Updated At</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
							</form>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
