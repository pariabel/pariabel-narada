<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/navigation">Navigation</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $navigation->id; ?>">

					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Parent</label>
							<select class="custom-select" name="parent_id">
								<option value="0">None</option>
								<?php foreach ($parent_navs as $pnav): ?>
								<option value="<?php echo $pnav->id; ?>" <?php echo set_select('parent_id', $navigation->parent_id, ($navigation->parent_id == $pnav->id) ? TRUE : FALSE); ?>><?php echo $pnav->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('name') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="name" placeholder="Enter navigation name" value="<?php echo set_value('name', $navigation->name); ?>">
							<?php echo form_error('name', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Icon</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('icon') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="icon" placeholder="Enter navigation icon" value="<?php echo set_value('icon', $navigation->icon); ?>">
							<?php echo form_error('icon', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>URI</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('uri') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="uri" placeholder="Enter navigation uri" value="<?php echo set_value('uri', $navigation->uri); ?>">
							<?php echo form_error('uri', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Module</label>
							<select name="module_id" class="form-control select2bs4<?php echo ($this->input->post()) ? ((form_error('module_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" style="width: 100%;" placeholder="Select Module">
								<option value="">Select Module</option>
								<?php foreach ($modules as $module): ?>
								<option value="<?php echo $module->id; ?>" <?php echo set_select('module_id', $navigation->module_id, ($navigation->module_id == $module->id) ? TRUE : FALSE); ?>><?php echo $module->name; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('module_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Action</label>
							<div class="custom-control custom-radio">
								<input class="custom-control-input" type="radio" id="actionCreate" name="action" value="create" <?php echo set_radio('action', 'create', ($navigation->action == 'create') ? TRUE : FALSE); ?>>
								<label for="actionCreate" class="custom-control-label font-weight-normal">Create</label>
							</div>
							<div class="custom-control custom-radio">
								<input class="custom-control-input" type="radio" id="actionRead" name="action" value="read" <?php echo set_radio('action', 'read', ($navigation->action == 'read') ? TRUE : FALSE); ?>>
								<label for="actionRead" class="custom-control-label font-weight-normal">Read</label>
							</div>
							<div class="custom-control custom-radio">
								<input class="custom-control-input" type="radio" id="actionUpdate" name="action" value="update" <?php echo set_radio('action', 'update', ($navigation->action == 'update') ? TRUE : FALSE); ?>>
								<label for="actionUpdate" class="custom-control-label font-weight-normal">Update</label>
							</div>
							<div class="custom-control custom-radio">
								<input class="custom-control-input" type="radio" id="actionDelete" name="action" value="delete" <?php echo set_radio('action', 'delete', ($navigation->action == 'delete') ? TRUE : FALSE); ?>>
								<label for="actionDelete" class="custom-control-label font-weight-normal">Delete</label>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Navigation</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
