<!-- Toastr -->
	<script src="/assets/plugins/toastr/toastr.min.js"></script>
	<!-- Page Script -->
	<script src="/assets/js/pages/profile.index.js"></script>
	<?php if ($toastr !== ''): ?>
	<script>
		toastr.options.positionClass = 'toast-top-full-width';
	<?php echo sprintf("toastr.%s('%s', '%s');", $toastr['type'], $toastr['message'], $toastr['title']); ?>
	<?php if ($toastr['type'] == 'success'): ?>
		$('#changePasswordSuccess').show();
		$('form').css('opacity', 0.3);
		$('button[type="submit"]').attr('disabled', true);
		setTimeout(function(){
			location.href = '/logout';
		}, 5000);
	<?php endif; ?>
	</script>
	<?php endif; ?>
