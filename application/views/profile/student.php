<dl class="row">
	<dt class="col-sm-2">NIS.</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->nis; ?></dd>
	<dt class="col-sm-2">Name</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->name; ?></dd>
	<dt class="col-sm-2">Username</dt>
	<dd class="col-sm-10"><?php echo $sess_user->username; ?></dd>
	<dt class="col-sm-2">Class</dt>
	<dd class="col-sm-10"><?php echo $class->class; ?><?php echo $section->section; ?></dd>
</dl>
<hr>
<h4>Parents</h4>
<dl class="row">
	<dt class="col-sm-2">Father</dt>
	<dd class="col-sm-10"><?php echo $sess_user->parents->father; ?></dd>
	<dt class="col-sm-2">Father Email</dt>
	<dd class="col-sm-10"><?php echo $sess_user->parents->father_email; ?></dd>
	<dt class="col-sm-2">Mother</dt>
	<dd class="col-sm-10"><?php echo $sess_user->parents->mother; ?></dd>
	<dt class="col-sm-2">Mother Email</dt>
	<dd class="col-sm-10"><?php echo $sess_user->parents->mother_email; ?></dd>
	<dt class="col-sm-2">Address</dt>
	<dd class="col-sm-10"><?php echo $sess_user->parents->address; ?></dd>
</dl>

