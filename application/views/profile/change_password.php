<div id="changePasswordSuccess" class="mb-5" style="display: none;">
	<h4><i class="fas fa-lg fa-spinner fa-spin"></i> Your session will automatically ends in seconds to refresh the changes.</h4>
</div>

<form role="form" action="" method="post">

	<div class="form-group">
		<label>Current Password</label>
		<div class="input-group">
			<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('current_password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="current_password" value="<?php echo set_value('current_password'); ?>">
			<div class="input-group-append" style="cursor: pointer;" id="toggleEyeCurrentPassword">
				<span class="input-group-text"><i class="fas fa-eye-slash"></i></span>
			</div>
		</div>
		<?php echo form_error('current_password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
	</div>

	<div class="form-group">
		<label>New Password</label>
		<div class="input-group">
			<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('new_password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="new_password" value="<?php echo set_value('new_password'); ?>">
			<div class="input-group-append" style="cursor: pointer;" id="toggleEyeNewPassword">
				<span class="input-group-text"><i class="fas fa-eye-slash"></i></span>
			</div>
		</div>
		<?php echo form_error('new_password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
	</div>

	<button type="submit" class="btn btn-primary">Change Password</button>
	<button type="reset" class="btn btn-default d-none">Reset</button>

</form>
