<dl class="row">
	<dt class="col-sm-2">Name</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->name; ?></dd>
	<dt class="col-sm-2">Username</dt>
	<dd class="col-sm-10"><?php echo $sess_user->username; ?></dd>
	<dt class="col-sm-2">Register At</dt>
	<dd class="col-sm-10"><?php echo date('F d, Y H:i:s', strtotime($sess_user->created_at)); ?></dd>
</dl>
