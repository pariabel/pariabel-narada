<dl class="row">
	<dt class="col-sm-2">Father</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->father; ?></dd>
	<dt class="col-sm-2">Father Email</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->father_email; ?></dd>
	<dt class="col-sm-2">Mother</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->mother; ?></dd>
	<dt class="col-sm-2">Mother Email</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->mother_email; ?></dd>
	<dt class="col-sm-2">Address</dt>
	<dd class="col-sm-10"><?php echo $sess_user->profile->address; ?></dd>
	<dt class="col-sm-2">Username</dt>
	<dd class="col-sm-10"><?php echo $sess_user->username; ?></dd>
</dl>
