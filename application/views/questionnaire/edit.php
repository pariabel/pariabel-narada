<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/questionnaire">Questionnaire</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>
				<!-- form start -->
				<form role="form" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $questionnaire->id; ?>">

					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<div class="form-group">
							<label>Level</label>
							<select name="level_id" class="custom-select<?php echo ($this->input->post()) ? ((form_error('level_id') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" placeholder="Select Level">
								<option value="">Select Level</option>
								<?php foreach ($levels as $level): ?>
								<option value="<?php echo $level->id; ?>" <?php echo set_select('level_id', $level->id, ($level->id == $questionnaire->level_id) ? TRUE : FALSE); ?>><?php echo $level->level; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('level_id', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<div><label>Respondent</label></div>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="respondentStudent" name="respondent" value="Student" <?php echo set_radio('respondent', 'Student', ($questionnaire->respondent == 'Student') ? TRUE : FALSE); ?>>
								<label for="respondentStudent" class="custom-control-label font-weight-normal">Student</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="respondentParents" name="respondent" value="Parents" <?php echo set_radio('respondent', 'Parents', ($questionnaire->respondent == 'Parents') ? TRUE : FALSE); ?>>
								<label for="respondentParents" class="custom-control-label font-weight-normal">Parents</label>
							</div>
						</div>

						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('title') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="title" placeholder="Enter title" value="<?php echo set_value('title', $questionnaire->title); ?>">
							<?php echo form_error('title', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Description</label>
							<textarea class="form-control<?php echo ($this->input->post()) ? ((form_error('description') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3" name="description" placeholder="Enter description"><?php echo set_value('description', $questionnaire->description); ?></textarea>
							<?php echo form_error('description', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<div class="form-group">
							<label>Active</label><br>
							<div class="custom-control custom-radio d-inline-block mr-5">
								<input class="custom-control-input" type="radio" id="activeYes" name="is_active" value="yes" <?php echo set_radio('is_active', 'yes', ($questionnaire->is_active == 'yes') ? TRUE : FALSE); ?>>
								<label for="activeYes" class="custom-control-label font-weight-normal">Yes</label>
							</div>
							<div class="custom-control custom-radio d-inline-block">
								<input class="custom-control-input" type="radio" id="activeNo" name="is_active" value="no" <?php echo set_radio('is_active', 'no', ($questionnaire->is_active == 'no') ? TRUE : FALSE); ?>>
								<label for="activeNo" class="custom-control-label font-weight-normal">No</label>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Update Questionnaire</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
