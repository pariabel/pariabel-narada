<!-- DataTables -->
	<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<!-- SweetAlert2 -->
	<script src="/assets/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Page Script -->
	<script src="/assets/js/pages/questionnaire.student.index.js"></script>
	<?php if ($swal !== ''): ?>
	<script>
	<?php echo sprintf("Swal.fire('%s', '%s', '%s')", $swal['title'], $swal['text'], $swal['icon']); ?>
	</script>
	<?php endif; ?>
