<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item"><a href="/questionnaire">Questionnaire</a></li>
								<li class="breadcrumb-item active">Respond</li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $questionnaire->title; ?></h3>
						</div>

						<!-- form start -->
						<form role="form" action="" method="post">
							<input type="hidden" name="questionnaire_id" value="<?php echo $questionnaire->id; ?>">

							<div class="card-body">

								<?php foreach ( ($questions != FALSE) ? $questions : array() as $k => $question): ?>
								<div>
									<?php echo $k+1; ?>.&nbsp;&nbsp;&nbsp;<?php echo $question->question; ?>
								</div>
								<div class="ml-4 mb-2">
									<?php foreach ( ($question->options != FALSE) ? $question->options : array() as $i => $option): ?>
									<div class="icheck-primary">
										<?php if ($question->option_type == 'radio'): ?>
										<input type="radio" name="option_question[<?php echo $question->id; ?>]" id="radioOption<?php echo $option->id; ?>" value="<?php echo $option->id; ?>" <?php echo set_radio('option_question[' . $question->id . ']', $option->id); ?>>
										<label for="radioOption<?php echo $option->id; ?>"><?php echo $option->option; ?></label>
										<?php elseif ($question->option_type == 'checkbox'): ?>
										<input type="checkbox" name="option_question[<?php echo $question->id; ?>][]" id="cbOption<?php echo $option->id; ?>" value="<?php echo $option->id; ?>" <?php echo set_checkbox('option_question[' . $question->id . '][]', $option->id); ?>>
										<label for="cbOption<?php echo $option->id; ?>"><?php echo $option->option; ?></label>
										<?php endif; ?>
									</div>
									<?php endforeach; ?>
									<?php echo form_error('option_question[' . $question->id . ']', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
								<?php endforeach; ?>

							</div>
							<!-- /.card-body -->

							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Submit Questionnaire</button>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
