<!-- ChartJS -->
<script src="/assets/plugins/chart.js/Chart.min.js"></script>
<script>
$('.progress-bar').each(function() {
	$(this).animate({
		'width': $(this).attr('data-width')
	}, { duration: 500, easing: 'linear'})
});

//-------------
//- PIE CHART -
//-------------
var pieOptions     = {
	maintainAspectRatio : false,
	responsive : true,
}
<?php foreach ( ($questions != FALSE) ? $questions : array() as $k => $question): ?>
var pieData<?php echo $k+1; ?> = {
	labels: [
		<?php foreach ( ($question->options != FALSE) ? $question->options : array() as $i => $option): ?>
		'<?php echo $option->option; ?>',
		<?php endforeach; ?>
	],
	datasets: [
		{
			data: [
				<?php foreach ( ($question->options != FALSE) ? $question->options : array() as $i => $option): ?>
				'<?php echo $option->user_count; ?>',
				<?php endforeach; ?>
			],
			backgroundColor : [
				<?php foreach ( ($question->options != FALSE) ? $question->options : array() as $i => $option): ?>
				'<?php echo $option->bg_color; ?>',
				<?php endforeach; ?>
			],
		}
	]
}
// Get context with jQuery - using jQuery's .get() method.
var pieChartCanvas<?php echo $k+1; ?> = $('#pieChart<?php echo $k+1; ?>').get(0).getContext('2d')

// Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas<?php echo $k+1; ?>, {
	type: 'pie',
	data: pieData<?php echo $k+1; ?>,
	options: pieOptions
})
<?php endforeach; ?>
</script>
