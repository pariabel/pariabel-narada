<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1><?php echo $page_title; ?></h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/home">Home</a></li>
								<li class="breadcrumb-item"><a href="/questionnaire">Questionnaire</a></li>
								<li class="breadcrumb-item active">Preview</li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Default box -->
					<div class="card">
						<div class="card-header">
							<h3 class="card-title"><?php echo $questionnaire->title; ?></h3>
						</div>
						<div class="card-body">

							<?php foreach ( ($questions != FALSE) ? $questions : array() as $k => $question): ?>
							<div>
								<?php echo $k+1; ?>.&nbsp;&nbsp;&nbsp;<?php echo $question->question; ?>
							</div>
							<div class="ml-4">
								<?php foreach ( ($question->options != FALSE) ? $question->options : array() as $i => $option): ?>
								<div class="icheck-primary">
									<?php if ($question->option_type == 'radio'): ?>
									<input type="radio" name="option_checked_question<?php echo $question->id; ?>" id="radioOption<?php echo $option->id; ?>" value="<?php echo $option->id; ?>">
									<label for="radioOption<?php echo $option->id; ?>"><?php echo $option->option; ?></label>
									<?php elseif ($question->option_type == 'checkbox'): ?>
									<input type="checkbox" name="option<?php echo $option->id; ?>" id="cbOption<?php echo $option->id; ?>" value="<?php echo $option->id; ?>">
									<label for="cbOption<?php echo $option->id; ?>"><?php echo $option->option; ?></label>
									<?php endif; ?>
								</div>
								<?php endforeach; ?>
							</div>
							<?php endforeach; ?>

						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<a href="javascript:history.go(-1);" class="btn btn-default"><i class="fas fa-arrow-left"></i> Back</a>
						</div>
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>
