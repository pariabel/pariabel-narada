<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $page_title; ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/home">Home</a></li>
						<li class="breadcrumb-item"><a href="/parents">Parents</a></li>
						<li class="breadcrumb-item active"><?php echo $page_title; ?></li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Default box -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_title; ?></h3>
				</div>

				<!-- form start -->
				<form role="form" action="" method="post">
					<div class="card-body">

						<?php if (isset($alert)): ?>
						<div class="alert <?php echo $alert['type']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><i class="icon <?php echo $alert['icon']; ?>"></i> <?php echo $alert['title']; ?></h5>
							<?php echo $alert['message']; ?>
						</div>
						<?php endif; ?>

						<h5>Profile</h5>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Father</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('father') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="father" placeholder="Enter father" value="<?php echo set_value('father'); ?>">
									<?php echo form_error('father', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Father Email</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('father_email') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="father_email" placeholder="Enter father email" value="<?php echo set_value('father_email'); ?>">
									<?php echo form_error('father_email', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Mother</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('mother') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="mother" placeholder="Enter mother" value="<?php echo set_value('mother'); ?>">
									<?php echo form_error('mother', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Mother Email</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('mother_email') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="mother_email" placeholder="Enter mother email" value="<?php echo set_value('mother_email'); ?>">
									<?php echo form_error('mother_email', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Address</label>
							<textarea class="form-control<?php echo ($this->input->post()) ? ((form_error('address') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" rows="3" name="address" placeholder="Enter address"><?php echo set_value('address'); ?></textarea>
							<?php echo form_error('address', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
						</div>

						<hr>
						<h5>User Account</h5>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" class="form-control<?php echo ($this->input->post()) ? ((form_error('username') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="username" placeholder="Enter username" value="<?php echo set_value('username'); ?>">
									<?php echo form_error('username', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<button id="btnGenerateRandomPassword" type="button" class="btn btn-primary">Random Password</button>
										</div>
										<input type="password" class="form-control<?php echo ($this->input->post()) ? ((form_error('password') != '') ? ' is-invalid' : ' is-valid') : ''; ?>" name="password" placeholder="Enter password" value="<?php echo set_value('password'); ?>">
										<div class="input-group-append">
											<button id="toggleEye" type="button" class="btn btn-success"><i class="fas fa-eye-slash"></i></button>
										</div>
									</div>
									<?php echo form_error('password', '<div class="invalid-feedback" style="display: block">', '</div>'); ?>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->

					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Create Parents</button>
					</div>
				</form>
			</div>
			<!-- /.card -->
		</div>
	</section>
	<!-- /.content -->
</div>
