<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['site_name'] = 'Narada School';
$config['frontend_url'] = 'https://naradaschool.sch.id';
//$config['login_logo'] = '<b>Narada</b> School';
$config['login_logo'] = '<img src="/assets/img/login-logo.png" class="img-fluid">';

$config['badge_types'] = [
	'yes' => 'success',
	'no' => 'danger'
];

$config['upload_path'] = '/home/h81878/apps.naradaschool.sch.id/upload/' . date('Y');

$config['file_extension_fa_icon'] = array(
	'.jpg' => 'far fa-file-image',
	'.jpeg' => 'far fa-file-image',
	'.bmp' => 'far fa-file-image',
	'.gif' => 'far fa-file-image',
	'.png' => 'far fa-file-image',
	'.pdf' => 'far fa-file-pdf',
	'.doc' => 'far fa-file-word',
	'.docx' => 'far fa-file-word',
	'.xls' => 'far fa-file-excel',
	'.xlsx' => 'far fa-file-excel',
);

$config['bg_class'] = array('bg-primary', 'bg-danger', 'bg-success', 'bg-warning', 'bg-info', 'bg-pink', 'bg-lime', 'bg-orange', 'bg-fuchsia', 'bg-gray');
$config['bg_color'] = array('#007bff', '#dc3545', '#28a745', '#ffc107', '#17a2b8', '#e83e8c', '#01ff70', '#ff851b', '#f012be', '#adb5bd');
