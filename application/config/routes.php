<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'authentication';
$route['logout'] = 'authentication/logout';
$route['staffs'] = 'authentication/index/staffs';
$route['teachers'] = 'authentication/index/teachers';
$route['parents'] = 'authentication/index/parents';
$route['students'] = 'authentication/index/students';

$route['home'] = 'dashboard';
$route['users'] = 'user';
$route['roles'] = 'role';
$route['modules'] = 'module';

$route['c/v/(:any)/(:any)'] = 'uploaded_file/read/$1/$2';
$route['c/d/(:any)/(:any)'] = 'uploaded_file/download/$1/$2';
$route['announcements'] = 'announcement';
$route['assignments'] = 'assignment';
$route['subjects'] = 'subject';
$route['sections'] = 'section';
$route['classes/pre-school'] = 'classes/index/1';
$route['classes/elementary'] = 'classes/index/2';
$route['classes/junior-high-school'] = 'classes/index/3';
$route['classes/senior-high-school'] = 'classes/index/4';
$route['staff/list'] = 'staff/index';
$route['teacher/list'] = 'teacher/index';
$route['student/list'] = 'student/index';
$route['parents/list'] = 'parents/index';
$route['questionnaire/question/(:num)'] = 'questionnaire_question/index/$1';
$route['questionnaire/v/(:num)'] = 'questionnaire/preview/$1';
