<?php

function header_protection() {
	$CI =& get_instance();

	$CI->output->set_header('X-XSS-Protection: 1; mode=block');
	$CI->output->set_header('X-Frame-Options: SAMEORIGIN');
}
