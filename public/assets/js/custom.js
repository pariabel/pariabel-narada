var $backToTop = $('#back-to-top');
$backToTop.hide().css({bottom: '2.2rem', right: '1rem', opacity: '0.9'});
$(window).on('scroll', function() {
	if ($(this).scrollTop() > 100) {
		$backToTop.fadeIn();
	} else {
		$backToTop.fadeOut();
	}
});
