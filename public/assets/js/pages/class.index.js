"use strict";
var classIndex = function() {

	var initTable = function() {
		var table = $('#dt-classes').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ajax: {
				url: '/classes/datatable',
				type: 'POST'
			},
			order: [[0, 'desc']],
			columnDefs: [
				{ targets: -1, className: 'dt-body-nowrap', orderable: false}
			]
		});

		table.on('draw', function() {
			$('#dt-classes').on('click', 'tbody td .do-delete, tbody span.dtr-data .do_delete', function() {
				var name = $(this).attr('data-item');
				var deleteUrl = $(this).attr('href');

				Swal.fire({
					title: 'Are you sure?',
					html: 'Class <strong>' + name + '</strong> will be deleted!',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				})
				.then((result) => {
					if (result.value) {
						$.get(deleteUrl, function(response){
							Swal.fire(
								response.swal['title'],
								response.swal['html'],
								response.swal['type']
							).then((result) => {
								table.ajax.reload();
							});
						});
					}
				});

				return false;
			});
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable();
		}

	};

}();

$(function() {
	classIndex.init();
});
