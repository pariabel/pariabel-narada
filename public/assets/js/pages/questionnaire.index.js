"use strict";
var questionnaireIndex = function() {

	var initModal = function() {
		$('#modalDetail').on('show.bs.modal', function (event) {
			var btn = $(event.relatedTarget);

			$.ajax({
				url: btn.attr('href'),
				type: 'get',
				success: function(data){
					$('.modal-title').html(data.title);
					$('.modal-body').html(data.body);
				}
			});
		});
	};

	var initTable = function() {
		var table = $('#dt-questionnaire').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ajax: {
				url: '/questionnaire/datatable',
				type: 'POST'
			},
			order: [[0, 'desc']],
			columnDefs: [
				{ targets: -1, className: 'dt-body-nowrap', orderable: false}
			]
		});

		table.on('draw', function() {
			$('#dt-questionnaire').on('click', 'tbody td .do-delete, tbody span.dtr-data .do_delete', function() {
				var name = $(this).attr('data-item');
				var deleteUrl = $(this).attr('href');

				Swal.fire({
					title: 'Are you sure?',
					html: 'Questionnaire <strong>' + name + '</strong> will be deleted!',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				})
				.then((result) => {
					if (result.value) {
						$.get(deleteUrl, function(response){
							Swal.fire(
								response.swal['title'],
								response.swal['html'],
								response.swal['type']
							).then((result) => {
								table.ajax.reload();
							});
						});
					}
				});

				return false;
			});
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable();
			initModal();
		},

	};

}();

questionnaireIndex.init();
