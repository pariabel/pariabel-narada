"use strict";
var classAdd = function() {

	var initForm = function() {
		$('input, select, textarea').on('focus', function() {
			$(this).removeClass('is-invalid');
			$(this).parent().find('.invalid-feedback').hide();
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initForm();
		}

	};

}();

classAdd.init();
