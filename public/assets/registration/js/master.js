$(function(){
    var App = {

        initial : function(){
            App.megaMenu();
        },

        megaMenu : function(){  

          // Menu responsive
          $('.actBoxMenu').click(function(){
            $(".box-menu").css({"left" : "0px"});
            $("#main-content").css({"marginLeft" : "280px"});
            $(".box-menu-overlay").css({
              "backgroundColor": "rgba(2, 2, 62, 0.8)",
              "position" : "fixed",
              "width" : "100%",
              "height" : "100%",
              "top" : "0",
              "left" : "0",
              "z-index" : "99999"
            }); 
            $(".modal-overlay-menu").css({"display" : "block"});
          });
          $('.actCloseMenu').click(function(){
              $(".box-menu").css({"left": "-280px"});
              $("#main-content").css({"marginLeft" : "0px"});
              $(".box-menu-overlay").css({
                "width" : "0"
              });
              $(".modal-overlay-menu").css({"display" : "none"});
          });

        }

       
    };

    App.initial();  

});