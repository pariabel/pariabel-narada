$(function(){
    var BASE_URL=$('#baseUrl').val();
    
    $('#chkAggree').change(function () {
        if ($(this).prop("checked")) {
            $('#btnNext').removeAttr('disabled');
            return;
        }else{
            $('#btnNext').attr('disabled','disabled');
        }
    });
    
    //for internal only
    $('#nis').on('keypress',function(e) {
        if(e.which == 13) {
            var nis=$(this).val();
            if(nis !=""){
                $.post(BASE_URL+'registration/search/',{'nis':nis},function(r){
                    
                    if(r.length > 0){
                        enabledSubmit(r);
                        
                    }else{
                        disabledSubmit()
                    }
                })
            }
        }
    });
    
    
    $.getJSON(BASE_URL+'registration/getLevel',function(r){
        console.log(r)
        renderOption(r,'i_level','a',function(){
            $('#i_level').on('change',function(){
                
                var a=$(this).val();
                var b=$(this).find(":selected").attr('code');
                
                $('#i_class').removeAttr('disabled')
                
                $.getJSON(BASE_URL+'registration/getClass/'+a+'/'+b,function(r){
                    
                    renderOption(r,'i_class','e',function(){
                        $('#bNoForm').text(r.formulir)
                        $('#reg_no').val(r.formulir)
                        
                        $('#i_class').on('change',function(){
                            var t=$(this).val();
                            if(t=="14"){
                                $('#boxWaktu').show();
                            }else{
                                $('#boxWaktu').hide();
                            }
                        })
                    });

                })
            })
        });  
    })
    
    
    
    function renderOption(r,o,m,f){
        var el="";
        
        var datas=o=='i_level'?r:r.kelas;
        
        if(datas.length > 0){
            var a=['20221','20222','20223','20224'];
            
            
            
            $.each(datas,function(i,e){
                if(o=='i_level'){
                    
                    el+="<option value='"+e.id+"' code='"+a[i]+"'>"+e.level+"</option>"
                }else{
                    el+="<option value='"+e.id+"' >"+e.class+"</option>"
                }
            })
        }
        if(m=='a'){
            $('#'+o).append(el);
        }else{
            $('#'+o).empty().html("<option value=''>Select</option>"+el);
        }
        
        f()
    }
    
    function enabledSubmit(r){
        $('#student_id').val(r[0].id)
        $('#i_level').removeAttr('disabled')
        $('#full_name').val(r[0].name);
        $('#student_name').val(r[0].name);
        $('#parent_name').val(r[0].father);
        $('#email_parent').val(r[0].father_email);
        $('#btnSubmit').removeAttr('disabled');
        $('#spnEmpty').hide();
    }
    function disabledSubmit(){
        $('#i_level').attr('disabled','disabled')
        $('#full_name').val('');
        $('#student_name').val('');
        $('#parent_name').val('');
        $('#email_parent').val('');
        $('#btnSubmit').attr('disabled','disabled');
        $('#spnEmpty').show();
        
    }
    
    $("#myForm").validate({
        rules: {
            nis: "required",
            i_level: "required",
            i_class: "required",
            dob_date: "required",
            parent_name:"required",
            full_name:"required",
            student_name:"required",
            dob_place:"required",
            email_parent: {
                required: true,
                email: true
            },
            agree: "required",
            phone_mobile:{
                required: true,
                number: true
            }
        },
        messages: {
            nis: "NIS Wajib diisi",
            i_level: "Tingkat Sekolah wajib dipilih",
            i_class: "Kelas wajib dipilih",
            email_parent: "Format email salah",
            agree: "Please accept our policy",
            dob_date: "Tanggal Lahir Wajib diisi",
            dob_place: "Tempat Lahir Wajib diisi",
            parent_name: "Nama Orang Tua Wajib diisi",
            full_name: "Nama Lengkap Calon Murid Wajib diisi",
            student_name: "Nama Murid Wajib diisi",
            topic: "Please select at least 2 topics",
            phone_mobile:"Nomor Telepon Wajib diisi dan harus numeric"
        }
    });
        
    $('#btnSubmit').off().on('click',function(){
        var session=$('input[name="session"]:checked').val();
        var reg_type = $('#reg_type').val();
        
        if($("#myForm").valid()){
            
            $('#btnSubmit').attr('disabled','disabled').text("Processing ...").css({"background-color":"#DC143C"})
            
            var datas = $("#myForm").serializeArray();
            
            $.post(BASE_URL+'registration/doRegister/',datas,function(r){
                var strWindowFeatures = "location=yes,height=50,width=520,scrollbars=yes,status=yes";
                var win=window.open(BASE_URL+"registration/pdf","_blank", strWindowFeatures);

                setTimeout(function(){
                    
                    if(reg_type=="internal"){
                        window.location.href=BASE_URL+"registration/payment";
                    }else{
                        window.location.href=BASE_URL+"registration/register_success";
                    }
                    
                },'1000')
            })
        }
       return false;
    });
    
    
    $('#konvert').click(function () {   
        
    });
    
})